package org.xtext.example.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.QuestDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQuestDslParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'PollSystem'", "'{'", "'}'", "'Poll'", "'Question'", "'options'", "'=>'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;

    // delegates
    // delegators


        public InternalQuestDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQuestDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQuestDslParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g"; }


     
     	private QuestDslGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(QuestDslGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRulePollSystem"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:60:1: entryRulePollSystem : rulePollSystem EOF ;
    public final void entryRulePollSystem() throws RecognitionException {
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:61:1: ( rulePollSystem EOF )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:62:1: rulePollSystem EOF
            {
             before(grammarAccess.getPollSystemRule()); 
            pushFollow(FOLLOW_rulePollSystem_in_entryRulePollSystem61);
            rulePollSystem();

            state._fsp--;

             after(grammarAccess.getPollSystemRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePollSystem68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePollSystem"


    // $ANTLR start "rulePollSystem"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:69:1: rulePollSystem : ( ( rule__PollSystem__Group__0 ) ) ;
    public final void rulePollSystem() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:73:2: ( ( ( rule__PollSystem__Group__0 ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:74:1: ( ( rule__PollSystem__Group__0 ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:74:1: ( ( rule__PollSystem__Group__0 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:75:1: ( rule__PollSystem__Group__0 )
            {
             before(grammarAccess.getPollSystemAccess().getGroup()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:76:1: ( rule__PollSystem__Group__0 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:76:2: rule__PollSystem__Group__0
            {
            pushFollow(FOLLOW_rule__PollSystem__Group__0_in_rulePollSystem94);
            rule__PollSystem__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPollSystemAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePollSystem"


    // $ANTLR start "entryRulePoll"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:88:1: entryRulePoll : rulePoll EOF ;
    public final void entryRulePoll() throws RecognitionException {
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:89:1: ( rulePoll EOF )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:90:1: rulePoll EOF
            {
             before(grammarAccess.getPollRule()); 
            pushFollow(FOLLOW_rulePoll_in_entryRulePoll121);
            rulePoll();

            state._fsp--;

             after(grammarAccess.getPollRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRulePoll128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRulePoll"


    // $ANTLR start "rulePoll"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:97:1: rulePoll : ( ( rule__Poll__Group__0 ) ) ;
    public final void rulePoll() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:101:2: ( ( ( rule__Poll__Group__0 ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:102:1: ( ( rule__Poll__Group__0 ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:102:1: ( ( rule__Poll__Group__0 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:103:1: ( rule__Poll__Group__0 )
            {
             before(grammarAccess.getPollAccess().getGroup()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:104:1: ( rule__Poll__Group__0 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:104:2: rule__Poll__Group__0
            {
            pushFollow(FOLLOW_rule__Poll__Group__0_in_rulePoll154);
            rule__Poll__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getPollAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rulePoll"


    // $ANTLR start "entryRuleQuestion"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:116:1: entryRuleQuestion : ruleQuestion EOF ;
    public final void entryRuleQuestion() throws RecognitionException {
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:117:1: ( ruleQuestion EOF )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:118:1: ruleQuestion EOF
            {
             before(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_ruleQuestion_in_entryRuleQuestion181);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getQuestionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuestion188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:125:1: ruleQuestion : ( ( rule__Question__Group__0 ) ) ;
    public final void ruleQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:129:2: ( ( ( rule__Question__Group__0 ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:130:1: ( ( rule__Question__Group__0 ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:130:1: ( ( rule__Question__Group__0 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:131:1: ( rule__Question__Group__0 )
            {
             before(grammarAccess.getQuestionAccess().getGroup()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:132:1: ( rule__Question__Group__0 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:132:2: rule__Question__Group__0
            {
            pushFollow(FOLLOW_rule__Question__Group__0_in_ruleQuestion214);
            rule__Question__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleOption"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:144:1: entryRuleOption : ruleOption EOF ;
    public final void entryRuleOption() throws RecognitionException {
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:145:1: ( ruleOption EOF )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:146:1: ruleOption EOF
            {
             before(grammarAccess.getOptionRule()); 
            pushFollow(FOLLOW_ruleOption_in_entryRuleOption241);
            ruleOption();

            state._fsp--;

             after(grammarAccess.getOptionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOption248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleOption"


    // $ANTLR start "ruleOption"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:153:1: ruleOption : ( ( rule__Option__Group__0 ) ) ;
    public final void ruleOption() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:157:2: ( ( ( rule__Option__Group__0 ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:158:1: ( ( rule__Option__Group__0 ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:158:1: ( ( rule__Option__Group__0 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:159:1: ( rule__Option__Group__0 )
            {
             before(grammarAccess.getOptionAccess().getGroup()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:160:1: ( rule__Option__Group__0 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:160:2: rule__Option__Group__0
            {
            pushFollow(FOLLOW_rule__Option__Group__0_in_ruleOption274);
            rule__Option__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getOptionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleOption"


    // $ANTLR start "rule__PollSystem__Group__0"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:174:1: rule__PollSystem__Group__0 : rule__PollSystem__Group__0__Impl rule__PollSystem__Group__1 ;
    public final void rule__PollSystem__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:178:1: ( rule__PollSystem__Group__0__Impl rule__PollSystem__Group__1 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:179:2: rule__PollSystem__Group__0__Impl rule__PollSystem__Group__1
            {
            pushFollow(FOLLOW_rule__PollSystem__Group__0__Impl_in_rule__PollSystem__Group__0308);
            rule__PollSystem__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PollSystem__Group__1_in_rule__PollSystem__Group__0311);
            rule__PollSystem__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__Group__0"


    // $ANTLR start "rule__PollSystem__Group__0__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:186:1: rule__PollSystem__Group__0__Impl : ( 'PollSystem' ) ;
    public final void rule__PollSystem__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:190:1: ( ( 'PollSystem' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:191:1: ( 'PollSystem' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:191:1: ( 'PollSystem' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:192:1: 'PollSystem'
            {
             before(grammarAccess.getPollSystemAccess().getPollSystemKeyword_0()); 
            match(input,11,FOLLOW_11_in_rule__PollSystem__Group__0__Impl339); 
             after(grammarAccess.getPollSystemAccess().getPollSystemKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__Group__0__Impl"


    // $ANTLR start "rule__PollSystem__Group__1"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:205:1: rule__PollSystem__Group__1 : rule__PollSystem__Group__1__Impl rule__PollSystem__Group__2 ;
    public final void rule__PollSystem__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:209:1: ( rule__PollSystem__Group__1__Impl rule__PollSystem__Group__2 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:210:2: rule__PollSystem__Group__1__Impl rule__PollSystem__Group__2
            {
            pushFollow(FOLLOW_rule__PollSystem__Group__1__Impl_in_rule__PollSystem__Group__1370);
            rule__PollSystem__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PollSystem__Group__2_in_rule__PollSystem__Group__1373);
            rule__PollSystem__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__Group__1"


    // $ANTLR start "rule__PollSystem__Group__1__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:217:1: rule__PollSystem__Group__1__Impl : ( '{' ) ;
    public final void rule__PollSystem__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:221:1: ( ( '{' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:222:1: ( '{' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:222:1: ( '{' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:223:1: '{'
            {
             before(grammarAccess.getPollSystemAccess().getLeftCurlyBracketKeyword_1()); 
            match(input,12,FOLLOW_12_in_rule__PollSystem__Group__1__Impl401); 
             after(grammarAccess.getPollSystemAccess().getLeftCurlyBracketKeyword_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__Group__1__Impl"


    // $ANTLR start "rule__PollSystem__Group__2"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:236:1: rule__PollSystem__Group__2 : rule__PollSystem__Group__2__Impl rule__PollSystem__Group__3 ;
    public final void rule__PollSystem__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:240:1: ( rule__PollSystem__Group__2__Impl rule__PollSystem__Group__3 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:241:2: rule__PollSystem__Group__2__Impl rule__PollSystem__Group__3
            {
            pushFollow(FOLLOW_rule__PollSystem__Group__2__Impl_in_rule__PollSystem__Group__2432);
            rule__PollSystem__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__PollSystem__Group__3_in_rule__PollSystem__Group__2435);
            rule__PollSystem__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__Group__2"


    // $ANTLR start "rule__PollSystem__Group__2__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:248:1: rule__PollSystem__Group__2__Impl : ( ( rule__PollSystem__PollsAssignment_2 )* ) ;
    public final void rule__PollSystem__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:252:1: ( ( ( rule__PollSystem__PollsAssignment_2 )* ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:253:1: ( ( rule__PollSystem__PollsAssignment_2 )* )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:253:1: ( ( rule__PollSystem__PollsAssignment_2 )* )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:254:1: ( rule__PollSystem__PollsAssignment_2 )*
            {
             before(grammarAccess.getPollSystemAccess().getPollsAssignment_2()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:255:1: ( rule__PollSystem__PollsAssignment_2 )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:255:2: rule__PollSystem__PollsAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__PollSystem__PollsAssignment_2_in_rule__PollSystem__Group__2__Impl462);
            	    rule__PollSystem__PollsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

             after(grammarAccess.getPollSystemAccess().getPollsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__Group__2__Impl"


    // $ANTLR start "rule__PollSystem__Group__3"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:265:1: rule__PollSystem__Group__3 : rule__PollSystem__Group__3__Impl ;
    public final void rule__PollSystem__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:269:1: ( rule__PollSystem__Group__3__Impl )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:270:2: rule__PollSystem__Group__3__Impl
            {
            pushFollow(FOLLOW_rule__PollSystem__Group__3__Impl_in_rule__PollSystem__Group__3493);
            rule__PollSystem__Group__3__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__Group__3"


    // $ANTLR start "rule__PollSystem__Group__3__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:276:1: rule__PollSystem__Group__3__Impl : ( '}' ) ;
    public final void rule__PollSystem__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:280:1: ( ( '}' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:281:1: ( '}' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:281:1: ( '}' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:282:1: '}'
            {
             before(grammarAccess.getPollSystemAccess().getRightCurlyBracketKeyword_3()); 
            match(input,13,FOLLOW_13_in_rule__PollSystem__Group__3__Impl521); 
             after(grammarAccess.getPollSystemAccess().getRightCurlyBracketKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__Group__3__Impl"


    // $ANTLR start "rule__Poll__Group__0"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:303:1: rule__Poll__Group__0 : rule__Poll__Group__0__Impl rule__Poll__Group__1 ;
    public final void rule__Poll__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:307:1: ( rule__Poll__Group__0__Impl rule__Poll__Group__1 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:308:2: rule__Poll__Group__0__Impl rule__Poll__Group__1
            {
            pushFollow(FOLLOW_rule__Poll__Group__0__Impl_in_rule__Poll__Group__0560);
            rule__Poll__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Poll__Group__1_in_rule__Poll__Group__0563);
            rule__Poll__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__0"


    // $ANTLR start "rule__Poll__Group__0__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:315:1: rule__Poll__Group__0__Impl : ( 'Poll' ) ;
    public final void rule__Poll__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:319:1: ( ( 'Poll' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:320:1: ( 'Poll' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:320:1: ( 'Poll' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:321:1: 'Poll'
            {
             before(grammarAccess.getPollAccess().getPollKeyword_0()); 
            match(input,14,FOLLOW_14_in_rule__Poll__Group__0__Impl591); 
             after(grammarAccess.getPollAccess().getPollKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__0__Impl"


    // $ANTLR start "rule__Poll__Group__1"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:334:1: rule__Poll__Group__1 : rule__Poll__Group__1__Impl rule__Poll__Group__2 ;
    public final void rule__Poll__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:338:1: ( rule__Poll__Group__1__Impl rule__Poll__Group__2 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:339:2: rule__Poll__Group__1__Impl rule__Poll__Group__2
            {
            pushFollow(FOLLOW_rule__Poll__Group__1__Impl_in_rule__Poll__Group__1622);
            rule__Poll__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Poll__Group__2_in_rule__Poll__Group__1625);
            rule__Poll__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__1"


    // $ANTLR start "rule__Poll__Group__1__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:346:1: rule__Poll__Group__1__Impl : ( ( rule__Poll__IdAssignment_1 )? ) ;
    public final void rule__Poll__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:350:1: ( ( ( rule__Poll__IdAssignment_1 )? ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:351:1: ( ( rule__Poll__IdAssignment_1 )? )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:351:1: ( ( rule__Poll__IdAssignment_1 )? )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:352:1: ( rule__Poll__IdAssignment_1 )?
            {
             before(grammarAccess.getPollAccess().getIdAssignment_1()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:353:1: ( rule__Poll__IdAssignment_1 )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:353:2: rule__Poll__IdAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Poll__IdAssignment_1_in_rule__Poll__Group__1__Impl652);
                    rule__Poll__IdAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getPollAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__1__Impl"


    // $ANTLR start "rule__Poll__Group__2"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:363:1: rule__Poll__Group__2 : rule__Poll__Group__2__Impl rule__Poll__Group__3 ;
    public final void rule__Poll__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:367:1: ( rule__Poll__Group__2__Impl rule__Poll__Group__3 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:368:2: rule__Poll__Group__2__Impl rule__Poll__Group__3
            {
            pushFollow(FOLLOW_rule__Poll__Group__2__Impl_in_rule__Poll__Group__2683);
            rule__Poll__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Poll__Group__3_in_rule__Poll__Group__2686);
            rule__Poll__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__2"


    // $ANTLR start "rule__Poll__Group__2__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:375:1: rule__Poll__Group__2__Impl : ( '{' ) ;
    public final void rule__Poll__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:379:1: ( ( '{' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:380:1: ( '{' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:380:1: ( '{' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:381:1: '{'
            {
             before(grammarAccess.getPollAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__Poll__Group__2__Impl714); 
             after(grammarAccess.getPollAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__2__Impl"


    // $ANTLR start "rule__Poll__Group__3"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:394:1: rule__Poll__Group__3 : rule__Poll__Group__3__Impl rule__Poll__Group__4 ;
    public final void rule__Poll__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:398:1: ( rule__Poll__Group__3__Impl rule__Poll__Group__4 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:399:2: rule__Poll__Group__3__Impl rule__Poll__Group__4
            {
            pushFollow(FOLLOW_rule__Poll__Group__3__Impl_in_rule__Poll__Group__3745);
            rule__Poll__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Poll__Group__4_in_rule__Poll__Group__3748);
            rule__Poll__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__3"


    // $ANTLR start "rule__Poll__Group__3__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:406:1: rule__Poll__Group__3__Impl : ( ( rule__Poll__QuestionAssignment_3 ) ) ;
    public final void rule__Poll__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:410:1: ( ( ( rule__Poll__QuestionAssignment_3 ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:411:1: ( ( rule__Poll__QuestionAssignment_3 ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:411:1: ( ( rule__Poll__QuestionAssignment_3 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:412:1: ( rule__Poll__QuestionAssignment_3 )
            {
             before(grammarAccess.getPollAccess().getQuestionAssignment_3()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:413:1: ( rule__Poll__QuestionAssignment_3 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:413:2: rule__Poll__QuestionAssignment_3
            {
            pushFollow(FOLLOW_rule__Poll__QuestionAssignment_3_in_rule__Poll__Group__3__Impl775);
            rule__Poll__QuestionAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getPollAccess().getQuestionAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__3__Impl"


    // $ANTLR start "rule__Poll__Group__4"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:423:1: rule__Poll__Group__4 : rule__Poll__Group__4__Impl ;
    public final void rule__Poll__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:427:1: ( rule__Poll__Group__4__Impl )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:428:2: rule__Poll__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Poll__Group__4__Impl_in_rule__Poll__Group__4805);
            rule__Poll__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__4"


    // $ANTLR start "rule__Poll__Group__4__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:434:1: rule__Poll__Group__4__Impl : ( '}' ) ;
    public final void rule__Poll__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:438:1: ( ( '}' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:439:1: ( '}' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:439:1: ( '}' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:440:1: '}'
            {
             before(grammarAccess.getPollAccess().getRightCurlyBracketKeyword_4()); 
            match(input,13,FOLLOW_13_in_rule__Poll__Group__4__Impl833); 
             after(grammarAccess.getPollAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__Group__4__Impl"


    // $ANTLR start "rule__Question__Group__0"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:463:1: rule__Question__Group__0 : rule__Question__Group__0__Impl rule__Question__Group__1 ;
    public final void rule__Question__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:467:1: ( rule__Question__Group__0__Impl rule__Question__Group__1 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:468:2: rule__Question__Group__0__Impl rule__Question__Group__1
            {
            pushFollow(FOLLOW_rule__Question__Group__0__Impl_in_rule__Question__Group__0874);
            rule__Question__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__1_in_rule__Question__Group__0877);
            rule__Question__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__0"


    // $ANTLR start "rule__Question__Group__0__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:475:1: rule__Question__Group__0__Impl : ( 'Question' ) ;
    public final void rule__Question__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:479:1: ( ( 'Question' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:480:1: ( 'Question' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:480:1: ( 'Question' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:481:1: 'Question'
            {
             before(grammarAccess.getQuestionAccess().getQuestionKeyword_0()); 
            match(input,15,FOLLOW_15_in_rule__Question__Group__0__Impl905); 
             after(grammarAccess.getQuestionAccess().getQuestionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__0__Impl"


    // $ANTLR start "rule__Question__Group__1"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:494:1: rule__Question__Group__1 : rule__Question__Group__1__Impl rule__Question__Group__2 ;
    public final void rule__Question__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:498:1: ( rule__Question__Group__1__Impl rule__Question__Group__2 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:499:2: rule__Question__Group__1__Impl rule__Question__Group__2
            {
            pushFollow(FOLLOW_rule__Question__Group__1__Impl_in_rule__Question__Group__1936);
            rule__Question__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__2_in_rule__Question__Group__1939);
            rule__Question__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__1"


    // $ANTLR start "rule__Question__Group__1__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:506:1: rule__Question__Group__1__Impl : ( ( rule__Question__IdAssignment_1 )? ) ;
    public final void rule__Question__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:510:1: ( ( ( rule__Question__IdAssignment_1 )? ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:511:1: ( ( rule__Question__IdAssignment_1 )? )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:511:1: ( ( rule__Question__IdAssignment_1 )? )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:512:1: ( rule__Question__IdAssignment_1 )?
            {
             before(grammarAccess.getQuestionAccess().getIdAssignment_1()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:513:1: ( rule__Question__IdAssignment_1 )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:513:2: rule__Question__IdAssignment_1
                    {
                    pushFollow(FOLLOW_rule__Question__IdAssignment_1_in_rule__Question__Group__1__Impl966);
                    rule__Question__IdAssignment_1();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getQuestionAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__1__Impl"


    // $ANTLR start "rule__Question__Group__2"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:523:1: rule__Question__Group__2 : rule__Question__Group__2__Impl rule__Question__Group__3 ;
    public final void rule__Question__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:527:1: ( rule__Question__Group__2__Impl rule__Question__Group__3 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:528:2: rule__Question__Group__2__Impl rule__Question__Group__3
            {
            pushFollow(FOLLOW_rule__Question__Group__2__Impl_in_rule__Question__Group__2997);
            rule__Question__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__3_in_rule__Question__Group__21000);
            rule__Question__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__2"


    // $ANTLR start "rule__Question__Group__2__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:535:1: rule__Question__Group__2__Impl : ( '{' ) ;
    public final void rule__Question__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:539:1: ( ( '{' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:540:1: ( '{' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:540:1: ( '{' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:541:1: '{'
            {
             before(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,12,FOLLOW_12_in_rule__Question__Group__2__Impl1028); 
             after(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__2__Impl"


    // $ANTLR start "rule__Question__Group__3"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:554:1: rule__Question__Group__3 : rule__Question__Group__3__Impl rule__Question__Group__4 ;
    public final void rule__Question__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:558:1: ( rule__Question__Group__3__Impl rule__Question__Group__4 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:559:2: rule__Question__Group__3__Impl rule__Question__Group__4
            {
            pushFollow(FOLLOW_rule__Question__Group__3__Impl_in_rule__Question__Group__31059);
            rule__Question__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__4_in_rule__Question__Group__31062);
            rule__Question__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__3"


    // $ANTLR start "rule__Question__Group__3__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:566:1: rule__Question__Group__3__Impl : ( ( rule__Question__TextAssignment_3 ) ) ;
    public final void rule__Question__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:570:1: ( ( ( rule__Question__TextAssignment_3 ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:571:1: ( ( rule__Question__TextAssignment_3 ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:571:1: ( ( rule__Question__TextAssignment_3 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:572:1: ( rule__Question__TextAssignment_3 )
            {
             before(grammarAccess.getQuestionAccess().getTextAssignment_3()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:573:1: ( rule__Question__TextAssignment_3 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:573:2: rule__Question__TextAssignment_3
            {
            pushFollow(FOLLOW_rule__Question__TextAssignment_3_in_rule__Question__Group__3__Impl1089);
            rule__Question__TextAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getTextAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__3__Impl"


    // $ANTLR start "rule__Question__Group__4"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:583:1: rule__Question__Group__4 : rule__Question__Group__4__Impl rule__Question__Group__5 ;
    public final void rule__Question__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:587:1: ( rule__Question__Group__4__Impl rule__Question__Group__5 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:588:2: rule__Question__Group__4__Impl rule__Question__Group__5
            {
            pushFollow(FOLLOW_rule__Question__Group__4__Impl_in_rule__Question__Group__41119);
            rule__Question__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__5_in_rule__Question__Group__41122);
            rule__Question__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__4"


    // $ANTLR start "rule__Question__Group__4__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:595:1: rule__Question__Group__4__Impl : ( 'options' ) ;
    public final void rule__Question__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:599:1: ( ( 'options' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:600:1: ( 'options' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:600:1: ( 'options' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:601:1: 'options'
            {
             before(grammarAccess.getQuestionAccess().getOptionsKeyword_4()); 
            match(input,16,FOLLOW_16_in_rule__Question__Group__4__Impl1150); 
             after(grammarAccess.getQuestionAccess().getOptionsKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__4__Impl"


    // $ANTLR start "rule__Question__Group__5"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:614:1: rule__Question__Group__5 : rule__Question__Group__5__Impl rule__Question__Group__6 ;
    public final void rule__Question__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:618:1: ( rule__Question__Group__5__Impl rule__Question__Group__6 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:619:2: rule__Question__Group__5__Impl rule__Question__Group__6
            {
            pushFollow(FOLLOW_rule__Question__Group__5__Impl_in_rule__Question__Group__51181);
            rule__Question__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__6_in_rule__Question__Group__51184);
            rule__Question__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__5"


    // $ANTLR start "rule__Question__Group__5__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:626:1: rule__Question__Group__5__Impl : ( '{' ) ;
    public final void rule__Question__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:630:1: ( ( '{' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:631:1: ( '{' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:631:1: ( '{' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:632:1: '{'
            {
             before(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_5()); 
            match(input,12,FOLLOW_12_in_rule__Question__Group__5__Impl1212); 
             after(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__5__Impl"


    // $ANTLR start "rule__Question__Group__6"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:645:1: rule__Question__Group__6 : rule__Question__Group__6__Impl rule__Question__Group__7 ;
    public final void rule__Question__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:649:1: ( rule__Question__Group__6__Impl rule__Question__Group__7 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:650:2: rule__Question__Group__6__Impl rule__Question__Group__7
            {
            pushFollow(FOLLOW_rule__Question__Group__6__Impl_in_rule__Question__Group__61243);
            rule__Question__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__7_in_rule__Question__Group__61246);
            rule__Question__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__6"


    // $ANTLR start "rule__Question__Group__6__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:657:1: rule__Question__Group__6__Impl : ( ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* ) ) ;
    public final void rule__Question__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:661:1: ( ( ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:662:1: ( ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:662:1: ( ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:663:1: ( ( rule__Question__OptionsAssignment_6 ) ) ( ( rule__Question__OptionsAssignment_6 )* )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:663:1: ( ( rule__Question__OptionsAssignment_6 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:664:1: ( rule__Question__OptionsAssignment_6 )
            {
             before(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:665:1: ( rule__Question__OptionsAssignment_6 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:665:2: rule__Question__OptionsAssignment_6
            {
            pushFollow(FOLLOW_rule__Question__OptionsAssignment_6_in_rule__Question__Group__6__Impl1275);
            rule__Question__OptionsAssignment_6();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); 

            }

            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:668:1: ( ( rule__Question__OptionsAssignment_6 )* )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:669:1: ( rule__Question__OptionsAssignment_6 )*
            {
             before(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:670:1: ( rule__Question__OptionsAssignment_6 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=RULE_ID && LA4_0<=RULE_STRING)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:670:2: rule__Question__OptionsAssignment_6
            	    {
            	    pushFollow(FOLLOW_rule__Question__OptionsAssignment_6_in_rule__Question__Group__6__Impl1287);
            	    rule__Question__OptionsAssignment_6();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getQuestionAccess().getOptionsAssignment_6()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__6__Impl"


    // $ANTLR start "rule__Question__Group__7"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:681:1: rule__Question__Group__7 : rule__Question__Group__7__Impl rule__Question__Group__8 ;
    public final void rule__Question__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:685:1: ( rule__Question__Group__7__Impl rule__Question__Group__8 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:686:2: rule__Question__Group__7__Impl rule__Question__Group__8
            {
            pushFollow(FOLLOW_rule__Question__Group__7__Impl_in_rule__Question__Group__71320);
            rule__Question__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__8_in_rule__Question__Group__71323);
            rule__Question__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__7"


    // $ANTLR start "rule__Question__Group__7__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:693:1: rule__Question__Group__7__Impl : ( '}' ) ;
    public final void rule__Question__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:697:1: ( ( '}' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:698:1: ( '}' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:698:1: ( '}' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:699:1: '}'
            {
             before(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_7()); 
            match(input,13,FOLLOW_13_in_rule__Question__Group__7__Impl1351); 
             after(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__7__Impl"


    // $ANTLR start "rule__Question__Group__8"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:712:1: rule__Question__Group__8 : rule__Question__Group__8__Impl ;
    public final void rule__Question__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:716:1: ( rule__Question__Group__8__Impl )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:717:2: rule__Question__Group__8__Impl
            {
            pushFollow(FOLLOW_rule__Question__Group__8__Impl_in_rule__Question__Group__81382);
            rule__Question__Group__8__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__8"


    // $ANTLR start "rule__Question__Group__8__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:723:1: rule__Question__Group__8__Impl : ( '}' ) ;
    public final void rule__Question__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:727:1: ( ( '}' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:728:1: ( '}' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:728:1: ( '}' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:729:1: '}'
            {
             before(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_8()); 
            match(input,13,FOLLOW_13_in_rule__Question__Group__8__Impl1410); 
             after(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__8__Impl"


    // $ANTLR start "rule__Option__Group__0"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:760:1: rule__Option__Group__0 : rule__Option__Group__0__Impl rule__Option__Group__1 ;
    public final void rule__Option__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:764:1: ( rule__Option__Group__0__Impl rule__Option__Group__1 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:765:2: rule__Option__Group__0__Impl rule__Option__Group__1
            {
            pushFollow(FOLLOW_rule__Option__Group__0__Impl_in_rule__Option__Group__01459);
            rule__Option__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Option__Group__1_in_rule__Option__Group__01462);
            rule__Option__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__0"


    // $ANTLR start "rule__Option__Group__0__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:772:1: rule__Option__Group__0__Impl : ( ( rule__Option__Group_0__0 )? ) ;
    public final void rule__Option__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:776:1: ( ( ( rule__Option__Group_0__0 )? ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:777:1: ( ( rule__Option__Group_0__0 )? )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:777:1: ( ( rule__Option__Group_0__0 )? )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:778:1: ( rule__Option__Group_0__0 )?
            {
             before(grammarAccess.getOptionAccess().getGroup_0()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:779:1: ( rule__Option__Group_0__0 )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:779:2: rule__Option__Group_0__0
                    {
                    pushFollow(FOLLOW_rule__Option__Group_0__0_in_rule__Option__Group__0__Impl1489);
                    rule__Option__Group_0__0();

                    state._fsp--;


                    }
                    break;

            }

             after(grammarAccess.getOptionAccess().getGroup_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__0__Impl"


    // $ANTLR start "rule__Option__Group__1"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:789:1: rule__Option__Group__1 : rule__Option__Group__1__Impl ;
    public final void rule__Option__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:793:1: ( rule__Option__Group__1__Impl )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:794:2: rule__Option__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Option__Group__1__Impl_in_rule__Option__Group__11520);
            rule__Option__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__1"


    // $ANTLR start "rule__Option__Group__1__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:800:1: rule__Option__Group__1__Impl : ( ( rule__Option__TextAssignment_1 ) ) ;
    public final void rule__Option__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:804:1: ( ( ( rule__Option__TextAssignment_1 ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:805:1: ( ( rule__Option__TextAssignment_1 ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:805:1: ( ( rule__Option__TextAssignment_1 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:806:1: ( rule__Option__TextAssignment_1 )
            {
             before(grammarAccess.getOptionAccess().getTextAssignment_1()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:807:1: ( rule__Option__TextAssignment_1 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:807:2: rule__Option__TextAssignment_1
            {
            pushFollow(FOLLOW_rule__Option__TextAssignment_1_in_rule__Option__Group__1__Impl1547);
            rule__Option__TextAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getOptionAccess().getTextAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group__1__Impl"


    // $ANTLR start "rule__Option__Group_0__0"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:821:1: rule__Option__Group_0__0 : rule__Option__Group_0__0__Impl rule__Option__Group_0__1 ;
    public final void rule__Option__Group_0__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:825:1: ( rule__Option__Group_0__0__Impl rule__Option__Group_0__1 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:826:2: rule__Option__Group_0__0__Impl rule__Option__Group_0__1
            {
            pushFollow(FOLLOW_rule__Option__Group_0__0__Impl_in_rule__Option__Group_0__01581);
            rule__Option__Group_0__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Option__Group_0__1_in_rule__Option__Group_0__01584);
            rule__Option__Group_0__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_0__0"


    // $ANTLR start "rule__Option__Group_0__0__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:833:1: rule__Option__Group_0__0__Impl : ( ( rule__Option__IdAssignment_0_0 ) ) ;
    public final void rule__Option__Group_0__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:837:1: ( ( ( rule__Option__IdAssignment_0_0 ) ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:838:1: ( ( rule__Option__IdAssignment_0_0 ) )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:838:1: ( ( rule__Option__IdAssignment_0_0 ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:839:1: ( rule__Option__IdAssignment_0_0 )
            {
             before(grammarAccess.getOptionAccess().getIdAssignment_0_0()); 
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:840:1: ( rule__Option__IdAssignment_0_0 )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:840:2: rule__Option__IdAssignment_0_0
            {
            pushFollow(FOLLOW_rule__Option__IdAssignment_0_0_in_rule__Option__Group_0__0__Impl1611);
            rule__Option__IdAssignment_0_0();

            state._fsp--;


            }

             after(grammarAccess.getOptionAccess().getIdAssignment_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_0__0__Impl"


    // $ANTLR start "rule__Option__Group_0__1"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:850:1: rule__Option__Group_0__1 : rule__Option__Group_0__1__Impl ;
    public final void rule__Option__Group_0__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:854:1: ( rule__Option__Group_0__1__Impl )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:855:2: rule__Option__Group_0__1__Impl
            {
            pushFollow(FOLLOW_rule__Option__Group_0__1__Impl_in_rule__Option__Group_0__11641);
            rule__Option__Group_0__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_0__1"


    // $ANTLR start "rule__Option__Group_0__1__Impl"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:861:1: rule__Option__Group_0__1__Impl : ( '=>' ) ;
    public final void rule__Option__Group_0__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:865:1: ( ( '=>' ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:866:1: ( '=>' )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:866:1: ( '=>' )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:867:1: '=>'
            {
             before(grammarAccess.getOptionAccess().getEqualsSignGreaterThanSignKeyword_0_1()); 
            match(input,17,FOLLOW_17_in_rule__Option__Group_0__1__Impl1669); 
             after(grammarAccess.getOptionAccess().getEqualsSignGreaterThanSignKeyword_0_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__Group_0__1__Impl"


    // $ANTLR start "rule__PollSystem__PollsAssignment_2"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:885:1: rule__PollSystem__PollsAssignment_2 : ( rulePoll ) ;
    public final void rule__PollSystem__PollsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:889:1: ( ( rulePoll ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:890:1: ( rulePoll )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:890:1: ( rulePoll )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:891:1: rulePoll
            {
             before(grammarAccess.getPollSystemAccess().getPollsPollParserRuleCall_2_0()); 
            pushFollow(FOLLOW_rulePoll_in_rule__PollSystem__PollsAssignment_21709);
            rulePoll();

            state._fsp--;

             after(grammarAccess.getPollSystemAccess().getPollsPollParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__PollSystem__PollsAssignment_2"


    // $ANTLR start "rule__Poll__IdAssignment_1"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:900:1: rule__Poll__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Poll__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:904:1: ( ( RULE_ID ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:905:1: ( RULE_ID )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:905:1: ( RULE_ID )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:906:1: RULE_ID
            {
             before(grammarAccess.getPollAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Poll__IdAssignment_11740); 
             after(grammarAccess.getPollAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__IdAssignment_1"


    // $ANTLR start "rule__Poll__QuestionAssignment_3"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:915:1: rule__Poll__QuestionAssignment_3 : ( ruleQuestion ) ;
    public final void rule__Poll__QuestionAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:919:1: ( ( ruleQuestion ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:920:1: ( ruleQuestion )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:920:1: ( ruleQuestion )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:921:1: ruleQuestion
            {
             before(grammarAccess.getPollAccess().getQuestionQuestionParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleQuestion_in_rule__Poll__QuestionAssignment_31771);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getPollAccess().getQuestionQuestionParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Poll__QuestionAssignment_3"


    // $ANTLR start "rule__Question__IdAssignment_1"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:930:1: rule__Question__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Question__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:934:1: ( ( RULE_ID ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:935:1: ( RULE_ID )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:935:1: ( RULE_ID )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:936:1: RULE_ID
            {
             before(grammarAccess.getQuestionAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Question__IdAssignment_11802); 
             after(grammarAccess.getQuestionAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__IdAssignment_1"


    // $ANTLR start "rule__Question__TextAssignment_3"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:945:1: rule__Question__TextAssignment_3 : ( RULE_STRING ) ;
    public final void rule__Question__TextAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:949:1: ( ( RULE_STRING ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:950:1: ( RULE_STRING )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:950:1: ( RULE_STRING )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:951:1: RULE_STRING
            {
             before(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_3_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Question__TextAssignment_31833); 
             after(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__TextAssignment_3"


    // $ANTLR start "rule__Question__OptionsAssignment_6"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:960:1: rule__Question__OptionsAssignment_6 : ( ruleOption ) ;
    public final void rule__Question__OptionsAssignment_6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:964:1: ( ( ruleOption ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:965:1: ( ruleOption )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:965:1: ( ruleOption )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:966:1: ruleOption
            {
             before(grammarAccess.getQuestionAccess().getOptionsOptionParserRuleCall_6_0()); 
            pushFollow(FOLLOW_ruleOption_in_rule__Question__OptionsAssignment_61864);
            ruleOption();

            state._fsp--;

             after(grammarAccess.getQuestionAccess().getOptionsOptionParserRuleCall_6_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__OptionsAssignment_6"


    // $ANTLR start "rule__Option__IdAssignment_0_0"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:975:1: rule__Option__IdAssignment_0_0 : ( RULE_ID ) ;
    public final void rule__Option__IdAssignment_0_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:979:1: ( ( RULE_ID ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:980:1: ( RULE_ID )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:980:1: ( RULE_ID )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:981:1: RULE_ID
            {
             before(grammarAccess.getOptionAccess().getIdIDTerminalRuleCall_0_0_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Option__IdAssignment_0_01895); 
             after(grammarAccess.getOptionAccess().getIdIDTerminalRuleCall_0_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__IdAssignment_0_0"


    // $ANTLR start "rule__Option__TextAssignment_1"
    // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:990:1: rule__Option__TextAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Option__TextAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:994:1: ( ( RULE_STRING ) )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:995:1: ( RULE_STRING )
            {
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:995:1: ( RULE_STRING )
            // ../org.xtext.example.quest.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalQuestDsl.g:996:1: RULE_STRING
            {
             before(grammarAccess.getOptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Option__TextAssignment_11926); 
             after(grammarAccess.getOptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Option__TextAssignment_1"

    // Delegated rules


 

    public static final BitSet FOLLOW_rulePollSystem_in_entryRulePollSystem61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePollSystem68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PollSystem__Group__0_in_rulePollSystem94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePoll_in_entryRulePoll121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePoll128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__0_in_rulePoll154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_entryRuleQuestion181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuestion188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__0_in_ruleQuestion214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_entryRuleOption241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOption248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group__0_in_ruleOption274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PollSystem__Group__0__Impl_in_rule__PollSystem__Group__0308 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__PollSystem__Group__1_in_rule__PollSystem__Group__0311 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__PollSystem__Group__0__Impl339 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PollSystem__Group__1__Impl_in_rule__PollSystem__Group__1370 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_rule__PollSystem__Group__2_in_rule__PollSystem__Group__1373 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__PollSystem__Group__1__Impl401 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PollSystem__Group__2__Impl_in_rule__PollSystem__Group__2432 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_rule__PollSystem__Group__3_in_rule__PollSystem__Group__2435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__PollSystem__PollsAssignment_2_in_rule__PollSystem__Group__2__Impl462 = new BitSet(new long[]{0x0000000000004002L});
    public static final BitSet FOLLOW_rule__PollSystem__Group__3__Impl_in_rule__PollSystem__Group__3493 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__PollSystem__Group__3__Impl521 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__0__Impl_in_rule__Poll__Group__0560 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_rule__Poll__Group__1_in_rule__Poll__Group__0563 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Poll__Group__0__Impl591 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__1__Impl_in_rule__Poll__Group__1622 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_rule__Poll__Group__2_in_rule__Poll__Group__1625 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__IdAssignment_1_in_rule__Poll__Group__1__Impl652 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__2__Impl_in_rule__Poll__Group__2683 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Poll__Group__3_in_rule__Poll__Group__2686 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Poll__Group__2__Impl714 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__3__Impl_in_rule__Poll__Group__3745 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Poll__Group__4_in_rule__Poll__Group__3748 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__QuestionAssignment_3_in_rule__Poll__Group__3__Impl775 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Poll__Group__4__Impl_in_rule__Poll__Group__4805 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Poll__Group__4__Impl833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__0__Impl_in_rule__Question__Group__0874 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_rule__Question__Group__1_in_rule__Question__Group__0877 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Question__Group__0__Impl905 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__1__Impl_in_rule__Question__Group__1936 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_rule__Question__Group__2_in_rule__Question__Group__1939 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__IdAssignment_1_in_rule__Question__Group__1__Impl966 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__2__Impl_in_rule__Question__Group__2997 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Question__Group__3_in_rule__Question__Group__21000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Question__Group__2__Impl1028 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__3__Impl_in_rule__Question__Group__31059 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Question__Group__4_in_rule__Question__Group__31062 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__TextAssignment_3_in_rule__Question__Group__3__Impl1089 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__4__Impl_in_rule__Question__Group__41119 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Question__Group__5_in_rule__Question__Group__41122 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Question__Group__4__Impl1150 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__5__Impl_in_rule__Question__Group__51181 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__Question__Group__6_in_rule__Question__Group__51184 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Question__Group__5__Impl1212 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__6__Impl_in_rule__Question__Group__61243 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Question__Group__7_in_rule__Question__Group__61246 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__OptionsAssignment_6_in_rule__Question__Group__6__Impl1275 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_rule__Question__OptionsAssignment_6_in_rule__Question__Group__6__Impl1287 = new BitSet(new long[]{0x0000000000000032L});
    public static final BitSet FOLLOW_rule__Question__Group__7__Impl_in_rule__Question__Group__71320 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Question__Group__8_in_rule__Question__Group__71323 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Question__Group__7__Impl1351 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__8__Impl_in_rule__Question__Group__81382 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Question__Group__8__Impl1410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group__0__Impl_in_rule__Option__Group__01459 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_rule__Option__Group__1_in_rule__Option__Group__01462 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_0__0_in_rule__Option__Group__0__Impl1489 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group__1__Impl_in_rule__Option__Group__11520 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__TextAssignment_1_in_rule__Option__Group__1__Impl1547 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_0__0__Impl_in_rule__Option__Group_0__01581 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_rule__Option__Group_0__1_in_rule__Option__Group_0__01584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__IdAssignment_0_0_in_rule__Option__Group_0__0__Impl1611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Option__Group_0__1__Impl_in_rule__Option__Group_0__11641 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Option__Group_0__1__Impl1669 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePoll_in_rule__PollSystem__PollsAssignment_21709 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Poll__IdAssignment_11740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_rule__Poll__QuestionAssignment_31771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Question__IdAssignment_11802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Question__TextAssignment_31833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_rule__Question__OptionsAssignment_61864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Option__IdAssignment_0_01895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Option__TextAssignment_11926 = new BitSet(new long[]{0x0000000000000002L});

}