package transformation;

import com.google.common.base.Objects;
import java.util.HashMap;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.Procedures.Procedure1;
import org.junit.Assert;
import org.junit.Test;
import org.xtext.example.mydsl.MyDslformStandaloneSetupGenerated;
import org.xtext.example.mydsl.QuestDslStandaloneSetupGenerated;
import org.xtext.example.mydsl.myDslform.Formulaire;
import org.xtext.example.mydsl.questDsl.Option;
import org.xtext.example.mydsl.questDsl.Poll;
import org.xtext.example.mydsl.questDsl.PollSystem;
import org.xtext.example.mydsl.questDsl.Question;

@SuppressWarnings("all")
public class QuestionnaireDemonstration {
  public PollSystem loadPollSystem(final URI uri) {
    PollSystem _xblockexpression = null;
    {
      QuestDslStandaloneSetupGenerated _questDslStandaloneSetupGenerated = new QuestDslStandaloneSetupGenerated();
      _questDslStandaloneSetupGenerated.createInjectorAndDoEMFRegistration();
      ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
      Resource res = _resourceSetImpl.getResource(uri, true);
      EList<EObject> _contents = res.getContents();
      EObject _get = _contents.get(0);
      _xblockexpression = ((PollSystem) _get);
    }
    return _xblockexpression;
  }
  
  public void savePollSystem(final URI uri, final PollSystem pollS) {
    try {
      ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
      Resource rs = _resourceSetImpl.createResource(uri);
      EList<EObject> _contents = rs.getContents();
      _contents.add(pollS);
      HashMap<Object, Object> _hashMap = new HashMap<Object, Object>();
      rs.save(_hashMap);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  public Formulaire loadFormulaire(final URI uri) {
    Formulaire _xblockexpression = null;
    {
      MyDslformStandaloneSetupGenerated _myDslformStandaloneSetupGenerated = new MyDslformStandaloneSetupGenerated();
      _myDslformStandaloneSetupGenerated.createInjectorAndDoEMFRegistration();
      ResourceSetImpl _resourceSetImpl = new ResourceSetImpl();
      Resource res = _resourceSetImpl.getResource(uri, true);
      EList<EObject> _contents = res.getContents();
      EObject _get = _contents.get(0);
      _xblockexpression = ((Formulaire) _get);
    }
    return _xblockexpression;
  }
  
  @Test
  public void test1() {
    URI _createURI = URI.createURI("testresult.quest");
    PollSystem pollS = this.loadPollSystem(_createURI);
    URI _createURI_1 = URI.createURI("testresult2.form");
    Formulaire form = this.loadFormulaire(_createURI_1);
    Assert.assertNotNull(pollS);
    EList<Poll> _polls = pollS.getPolls();
    int _size = _polls.size();
    Assert.assertEquals(2, _size);
    URI _createURI_2 = URI.createURI("test.xmi");
    this.savePollSystem(_createURI_2, pollS);
    EList<Poll> _polls_1 = pollS.getPolls();
    final Procedure1<Poll> _function = new Procedure1<Poll>() {
      public void apply(final Poll p) {
        String _id = p.getId();
        String _plus = (_id + "_poll");
        p.setId(_plus);
      }
    };
    IterableExtensions.<Poll>forEach(_polls_1, _function);
    URI _createURI_3 = URI.createURI("testresult2.quest");
    this.savePollSystem(_createURI_3, pollS);
  }
  
  /**
   * @Test
   * def test2() {
   * 
   * var pollS = loadPollSystem(URI.createURI("file://H:/windows/bureau/runtime-EclipseXtext/test2/test.quest"))
   * 
   * val fw = new FileWriter("file://H:/windows/bureau/runtime-EclipseXtext/test2/test.html")
   * var html = toPolls(pollS.polls)
   * assertNotNull(html)
   * fw.write(html.toString)
   * fw.close
   * }
   */
  public CharSequence toPolls(final List<Poll> polls) {
    StringConcatenation _builder = new StringConcatenation();
    _builder.append("<html>");
    _builder.newLine();
    _builder.append("<body>");
    _builder.newLine();
    {
      for(final Poll p : polls) {
        {
          String _id = p.getId();
          boolean _notEquals = (!Objects.equal(_id, null));
          if (_notEquals) {
            _builder.append("<h1>");
            String _id_1 = p.getId();
            _builder.append(_id_1, "");
            _builder.append("</h1>");
            _builder.newLineIfNotEmpty();
          }
        }
        {
          EList<Question> _question = p.getQuestion();
          for(final Question q : _question) {
            _builder.append("<p>");
            _builder.newLine();
            _builder.append("<h2>");
            String _text = q.getText();
            _builder.append(_text, "");
            _builder.append("</h2>");
            _builder.newLineIfNotEmpty();
            _builder.append("<ul>");
            _builder.newLine();
            {
              EList<Option> _options = q.getOptions();
              for(final Option o : _options) {
                _builder.append("<li>");
                String _text_1 = o.getText();
                _builder.append(_text_1, "");
                _builder.append("</li>");
                _builder.newLineIfNotEmpty();
              }
            }
            _builder.append("</ul>");
            _builder.newLine();
            _builder.append("</p>");
            _builder.newLine();
          }
        }
      }
    }
    _builder.append("</body>");
    _builder.newLine();
    _builder.append("</html>");
    _builder.newLine();
    return _builder;
  }
}
