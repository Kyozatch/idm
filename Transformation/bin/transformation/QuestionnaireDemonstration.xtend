package transformation


import java.util.HashMap
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.junit.Test
import static org.junit.Assert.*
import java.util.Collections
import java.util.List
import java.io.FileWriter
import org.xtext.example.mydsl.QuestDslStandaloneSetupGenerated
import org.xtext.example.mydsl.questDsl.PollSystem
import org.xtext.example.mydsl.questDsl.Poll
import org.xtext.example.mydsl.MyDslformStandaloneSetupGenerated
import org.xtext.example.mydsl.myDslform.Formulaire

class QuestionnaireDemonstration {
	def loadPollSystem(URI uri) {
		new QuestDslStandaloneSetupGenerated().createInjectorAndDoEMFRegistration()
		var res = new ResourceSetImpl().getResource(uri, true);
		res.contents.get(0) as PollSystem
	}

	def savePollSystem(URI uri, PollSystem pollS) {
		var Resource rs = new ResourceSetImpl().createResource(uri);
		rs.getContents.add(pollS);
		rs.save(new HashMap());
	}
	
	def loadFormulaire(URI uri) {
		new MyDslformStandaloneSetupGenerated().createInjectorAndDoEMFRegistration()
		var res = new ResourceSetImpl().getResource(uri, true);
		res.contents.get(0) as Formulaire
	}

	@Test
	def test1() {

		// loading
		var pollS = loadPollSystem(URI.createURI("testresult.quest"))
		var form = loadFormulaire(URI.createURI("testresult2.form"))		
		//var m2m = new ModelToModel
		
		//var dispoll = m2m.questToUIMM(pollS, form)		
		
		assertNotNull(pollS)
		assertEquals(2, pollS.polls.size)
		savePollSystem(URI.createURI("test.xmi"), pollS)

		// MODEL MANAGEMENT (ANALYSIS, TRANSFORMATION)
		pollS.polls.forEach[p|p.id = p.id + "_poll"]

		// serializing
		savePollSystem(URI.createURI("testresult2.quest"), pollS)
		
		
	}
	
/* 
	 @Test
	def test2() {

		var pollS = loadPollSystem(URI.createURI("file://H:/windows/bureau/runtime-EclipseXtext/test2/test.quest")) 

		val fw = new FileWriter("file://H:/windows/bureau/runtime-EclipseXtext/test2/test.html")
		var html = toPolls(pollS.polls)
		assertNotNull(html)
		fw.write(html.toString)
		fw.close
	}*/

	def toPolls(List<Poll> polls) '''
<html>
<body>
�FOR p : polls�
�IF p.id != null�
<h1>�p.id�</h1>
�ENDIF�
�FOR q : p.question�
<p>
<h2>�q.text�</h2>
<ul>
�FOR o : q.options�
<li>�o.text�</li>
�ENDFOR�
</ul>
</p>
�ENDFOR�
�ENDFOR�
</body>
</html>
'''

}
