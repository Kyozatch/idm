package org.xtext.example.mydsl.serializer;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.serializer.acceptor.ISemanticSequenceAcceptor;
import org.eclipse.xtext.serializer.acceptor.SequenceFeeder;
import org.eclipse.xtext.serializer.diagnostic.ISemanticSequencerDiagnosticProvider;
import org.eclipse.xtext.serializer.diagnostic.ISerializationDiagnostic.Acceptor;
import org.eclipse.xtext.serializer.sequencer.AbstractDelegatingSemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.GenericSequencer;
import org.eclipse.xtext.serializer.sequencer.ISemanticNodeProvider.INodesForEObjectProvider;
import org.eclipse.xtext.serializer.sequencer.ISemanticSequencer;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService;
import org.eclipse.xtext.serializer.sequencer.ITransientValueService.ValueTransient;
import org.xtext.example.mydsl.myDslform.CheckBox;
import org.xtext.example.mydsl.myDslform.Choix;
import org.xtext.example.mydsl.myDslform.Contenu;
import org.xtext.example.mydsl.myDslform.Formulaire;
import org.xtext.example.mydsl.myDslform.Image;
import org.xtext.example.mydsl.myDslform.List;
import org.xtext.example.mydsl.myDslform.MyDslformPackage;
import org.xtext.example.mydsl.myDslform.Question;
import org.xtext.example.mydsl.myDslform.RadioButton;
import org.xtext.example.mydsl.myDslform.TextBox;
import org.xtext.example.mydsl.myDslform.Upload;
import org.xtext.example.mydsl.services.MyDslformGrammarAccess;

@SuppressWarnings("all")
public class MyDslformSemanticSequencer extends AbstractDelegatingSemanticSequencer {

	@Inject
	private MyDslformGrammarAccess grammarAccess;
	
	public void createSequence(EObject context, EObject semanticObject) {
		if(semanticObject.eClass().getEPackage() == MyDslformPackage.eINSTANCE) switch(semanticObject.eClass().getClassifierID()) {
			case MyDslformPackage.CHECK_BOX:
				if(context == grammarAccess.getCheckBoxRule() ||
				   context == grammarAccess.getWidgetRule()) {
					sequence_CheckBox(context, (CheckBox) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.CHOIX:
				if(context == grammarAccess.getChoixRule()) {
					sequence_Choix(context, (Choix) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.CONTENU:
				if(context == grammarAccess.getContenuRule()) {
					sequence_Contenu(context, (Contenu) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.FORMULAIRE:
				if(context == grammarAccess.getFormulaireRule()) {
					sequence_Formulaire(context, (Formulaire) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.IMAGE:
				if(context == grammarAccess.getChoixRule() ||
				   context == grammarAccess.getImageRule()) {
					sequence_Image(context, (Image) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.LIST:
				if(context == grammarAccess.getListRule() ||
				   context == grammarAccess.getWidgetRule()) {
					sequence_List(context, (List) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.QUESTION:
				if(context == grammarAccess.getQuestionRule()) {
					sequence_Question(context, (Question) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.RADIO_BUTTON:
				if(context == grammarAccess.getRadioButtonRule() ||
				   context == grammarAccess.getWidgetRule()) {
					sequence_RadioButton(context, (RadioButton) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.TEXT_BOX:
				if(context == grammarAccess.getTextBoxRule() ||
				   context == grammarAccess.getWidgetRule()) {
					sequence_TextBox(context, (TextBox) semanticObject); 
					return; 
				}
				else break;
			case MyDslformPackage.UPLOAD:
				if(context == grammarAccess.getUploadRule() ||
				   context == grammarAccess.getWidgetRule()) {
					sequence_Upload(context, (Upload) semanticObject); 
					return; 
				}
				else break;
			}
		if (errorAcceptor != null) errorAcceptor.accept(diagnosticProvider.createInvalidContextOrTypeDiagnostic(semanticObject, context));
	}
	
	/**
	 * Constraint:
	 *     (id=ID reponse+=Contenu+)
	 */
	protected void sequence_CheckBox(EObject context, CheckBox semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     text=STRING
	 */
	protected void sequence_Choix(EObject context, Choix semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.CHOIX__TEXT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.CHOIX__TEXT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getChoixAccess().getTextSTRINGTerminalRuleCall_0_0(), semanticObject.getText());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (id=ID choix=Choix)
	 */
	protected void sequence_Contenu(EObject context, Contenu semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.CONTENU__ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.CONTENU__ID));
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.CONTENU__CHOIX) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.CONTENU__CHOIX));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getContenuAccess().getIdIDTerminalRuleCall_1_0(), semanticObject.getId());
		feeder.accept(grammarAccess.getContenuAccess().getChoixChoixParserRuleCall_3_0(), semanticObject.getChoix());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (id=ID questions+=Question*)
	 */
	protected void sequence_Formulaire(EObject context, Formulaire semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     img=STRING
	 */
	protected void sequence_Image(EObject context, Image semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (id=ID elements+=Contenu+)
	 */
	protected void sequence_List(EObject context, List semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (id=ID text=STRING choix=Widget)
	 */
	protected void sequence_Question(EObject context, Question semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.QUESTION__ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.QUESTION__ID));
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.QUESTION__TEXT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.QUESTION__TEXT));
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.QUESTION__CHOIX) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.QUESTION__CHOIX));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getQuestionAccess().getIdIDTerminalRuleCall_1_0(), semanticObject.getId());
		feeder.accept(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_5_0(), semanticObject.getText());
		feeder.accept(grammarAccess.getQuestionAccess().getChoixWidgetParserRuleCall_9_0(), semanticObject.getChoix());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (id=ID reponse+=Contenu+)
	 */
	protected void sequence_RadioButton(EObject context, RadioButton semanticObject) {
		genericSequencer.createSequence(context, semanticObject);
	}
	
	
	/**
	 * Constraint:
	 *     (id=ID text=STRING)
	 */
	protected void sequence_TextBox(EObject context, TextBox semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.WIDGET__ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.WIDGET__ID));
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.TEXT_BOX__TEXT) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.TEXT_BOX__TEXT));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getTextBoxAccess().getIdIDTerminalRuleCall_1_0(), semanticObject.getId());
		feeder.accept(grammarAccess.getTextBoxAccess().getTextSTRINGTerminalRuleCall_2_0(), semanticObject.getText());
		feeder.finish();
	}
	
	
	/**
	 * Constraint:
	 *     (id=ID img=Image)
	 */
	protected void sequence_Upload(EObject context, Upload semanticObject) {
		if(errorAcceptor != null) {
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.WIDGET__ID) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.WIDGET__ID));
			if(transientValues.isValueTransient(semanticObject, MyDslformPackage.Literals.UPLOAD__IMG) == ValueTransient.YES)
				errorAcceptor.accept(diagnosticProvider.createFeatureValueMissing(semanticObject, MyDslformPackage.Literals.UPLOAD__IMG));
		}
		INodesForEObjectProvider nodes = createNodeProvider(semanticObject);
		SequenceFeeder feeder = createSequencerFeeder(semanticObject, nodes);
		feeder.accept(grammarAccess.getUploadAccess().getIdIDTerminalRuleCall_1_0(), semanticObject.getId());
		feeder.accept(grammarAccess.getUploadAccess().getImgImageParserRuleCall_3_0(), semanticObject.getImg());
		feeder.finish();
	}
}
