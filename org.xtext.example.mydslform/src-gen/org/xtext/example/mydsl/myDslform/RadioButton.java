/**
 */
package org.xtext.example.mydsl.myDslform;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Radio Button</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myDslform.RadioButton#getReponse <em>Reponse</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getRadioButton()
 * @model
 * @generated
 */
public interface RadioButton extends Widget
{
  /**
   * Returns the value of the '<em><b>Reponse</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myDslform.Contenu}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reponse</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reponse</em>' containment reference list.
   * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getRadioButton_Reponse()
   * @model containment="true"
   * @generated
   */
  EList<Contenu> getReponse();

} // RadioButton
