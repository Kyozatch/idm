/**
 */
package org.xtext.example.mydsl.myDslform;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Check Box</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myDslform.CheckBox#getReponse <em>Reponse</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getCheckBox()
 * @model
 * @generated
 */
public interface CheckBox extends Widget
{
  /**
   * Returns the value of the '<em><b>Reponse</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.myDslform.Contenu}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Reponse</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Reponse</em>' containment reference list.
   * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getCheckBox_Reponse()
   * @model containment="true"
   * @generated
   */
  EList<Contenu> getReponse();

} // CheckBox
