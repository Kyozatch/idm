/**
 */
package org.xtext.example.mydsl.myDslform.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.xtext.example.mydsl.myDslform.Image;
import org.xtext.example.mydsl.myDslform.MyDslformPackage;
import org.xtext.example.mydsl.myDslform.Upload;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Upload</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myDslform.impl.UploadImpl#getImg <em>Img</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UploadImpl extends WidgetImpl implements Upload
{
  /**
   * The cached value of the '{@link #getImg() <em>Img</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see #getImg()
   * @generated
   * @ordered
   */
  protected Image img;

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  protected UploadImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  protected EClass eStaticClass()
  {
    return MyDslformPackage.Literals.UPLOAD;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Image getImg()
  {
    return img;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public NotificationChain basicSetImg(Image newImg, NotificationChain msgs)
  {
    Image oldImg = img;
    img = newImg;
    if (eNotificationRequired())
    {
      ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, MyDslformPackage.UPLOAD__IMG, oldImg, newImg);
      if (msgs == null) msgs = notification; else msgs.add(notification);
    }
    return msgs;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public void setImg(Image newImg)
  {
    if (newImg != img)
    {
      NotificationChain msgs = null;
      if (img != null)
        msgs = ((InternalEObject)img).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - MyDslformPackage.UPLOAD__IMG, null, msgs);
      if (newImg != null)
        msgs = ((InternalEObject)newImg).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - MyDslformPackage.UPLOAD__IMG, null, msgs);
      msgs = basicSetImg(newImg, msgs);
      if (msgs != null) msgs.dispatch();
    }
    else if (eNotificationRequired())
      eNotify(new ENotificationImpl(this, Notification.SET, MyDslformPackage.UPLOAD__IMG, newImg, newImg));
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs)
  {
    switch (featureID)
    {
      case MyDslformPackage.UPLOAD__IMG:
        return basicSetImg(null, msgs);
    }
    return super.eInverseRemove(otherEnd, featureID, msgs);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public Object eGet(int featureID, boolean resolve, boolean coreType)
  {
    switch (featureID)
    {
      case MyDslformPackage.UPLOAD__IMG:
        return getImg();
    }
    return super.eGet(featureID, resolve, coreType);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eSet(int featureID, Object newValue)
  {
    switch (featureID)
    {
      case MyDslformPackage.UPLOAD__IMG:
        setImg((Image)newValue);
        return;
    }
    super.eSet(featureID, newValue);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public void eUnset(int featureID)
  {
    switch (featureID)
    {
      case MyDslformPackage.UPLOAD__IMG:
        setImg((Image)null);
        return;
    }
    super.eUnset(featureID);
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public boolean eIsSet(int featureID)
  {
    switch (featureID)
    {
      case MyDslformPackage.UPLOAD__IMG:
        return img != null;
    }
    return super.eIsSet(featureID);
  }

} //UploadImpl
