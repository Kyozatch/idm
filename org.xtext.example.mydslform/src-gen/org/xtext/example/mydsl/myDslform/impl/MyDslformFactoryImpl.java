/**
 */
package org.xtext.example.mydsl.myDslform.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import org.xtext.example.mydsl.myDslform.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MyDslformFactoryImpl extends EFactoryImpl implements MyDslformFactory
{
  /**
   * Creates the default factory implementation.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public static MyDslformFactory init()
  {
    try
    {
      MyDslformFactory theMyDslformFactory = (MyDslformFactory)EPackage.Registry.INSTANCE.getEFactory(MyDslformPackage.eNS_URI);
      if (theMyDslformFactory != null)
      {
        return theMyDslformFactory;
      }
    }
    catch (Exception exception)
    {
      EcorePlugin.INSTANCE.log(exception);
    }
    return new MyDslformFactoryImpl();
  }

  /**
   * Creates an instance of the factory.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyDslformFactoryImpl()
  {
    super();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  @Override
  public EObject create(EClass eClass)
  {
    switch (eClass.getClassifierID())
    {
      case MyDslformPackage.FORMULAIRE: return createFormulaire();
      case MyDslformPackage.QUESTION: return createQuestion();
      case MyDslformPackage.WIDGET: return createWidget();
      case MyDslformPackage.UPLOAD: return createUpload();
      case MyDslformPackage.RADIO_BUTTON: return createRadioButton();
      case MyDslformPackage.CHECK_BOX: return createCheckBox();
      case MyDslformPackage.TEXT_BOX: return createTextBox();
      case MyDslformPackage.LIST: return createList();
      case MyDslformPackage.CONTENU: return createContenu();
      case MyDslformPackage.IMAGE: return createImage();
      case MyDslformPackage.CHOIX: return createChoix();
      default:
        throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
    }
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Formulaire createFormulaire()
  {
    FormulaireImpl formulaire = new FormulaireImpl();
    return formulaire;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Question createQuestion()
  {
    QuestionImpl question = new QuestionImpl();
    return question;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Widget createWidget()
  {
    WidgetImpl widget = new WidgetImpl();
    return widget;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Upload createUpload()
  {
    UploadImpl upload = new UploadImpl();
    return upload;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public RadioButton createRadioButton()
  {
    RadioButtonImpl radioButton = new RadioButtonImpl();
    return radioButton;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public CheckBox createCheckBox()
  {
    CheckBoxImpl checkBox = new CheckBoxImpl();
    return checkBox;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public TextBox createTextBox()
  {
    TextBoxImpl textBox = new TextBoxImpl();
    return textBox;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public List createList()
  {
    ListImpl list = new ListImpl();
    return list;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Contenu createContenu()
  {
    ContenuImpl contenu = new ContenuImpl();
    return contenu;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Image createImage()
  {
    ImageImpl image = new ImageImpl();
    return image;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public Choix createChoix()
  {
    ChoixImpl choix = new ChoixImpl();
    return choix;
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  public MyDslformPackage getMyDslformPackage()
  {
    return (MyDslformPackage)getEPackage();
  }

  /**
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @deprecated
   * @generated
   */
  @Deprecated
  public static MyDslformPackage getPackage()
  {
    return MyDslformPackage.eINSTANCE;
  }

} //MyDslformFactoryImpl
