/**
 */
package org.xtext.example.mydsl.myDslform;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Upload</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myDslform.Upload#getImg <em>Img</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getUpload()
 * @model
 * @generated
 */
public interface Upload extends Widget
{
  /**
   * Returns the value of the '<em><b>Img</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Img</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Img</em>' containment reference.
   * @see #setImg(Image)
   * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getUpload_Img()
   * @model containment="true"
   * @generated
   */
  Image getImg();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myDslform.Upload#getImg <em>Img</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Img</em>' containment reference.
   * @see #getImg()
   * @generated
   */
  void setImg(Image value);

} // Upload
