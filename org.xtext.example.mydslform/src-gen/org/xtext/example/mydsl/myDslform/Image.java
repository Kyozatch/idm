/**
 */
package org.xtext.example.mydsl.myDslform;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Image</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myDslform.Image#getImg <em>Img</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getImage()
 * @model
 * @generated
 */
public interface Image extends Choix
{
  /**
   * Returns the value of the '<em><b>Img</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Img</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Img</em>' attribute.
   * @see #setImg(String)
   * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getImage_Img()
   * @model
   * @generated
   */
  String getImg();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myDslform.Image#getImg <em>Img</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Img</em>' attribute.
   * @see #getImg()
   * @generated
   */
  void setImg(String value);

} // Image
