/**
 */
package org.xtext.example.mydsl.myDslform;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Contenu</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.myDslform.Contenu#getId <em>Id</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.myDslform.Contenu#getChoix <em>Choix</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getContenu()
 * @model
 * @generated
 */
public interface Contenu extends EObject
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getContenu_Id()
   * @model
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myDslform.Contenu#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Choix</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Choix</em>' containment reference isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Choix</em>' containment reference.
   * @see #setChoix(Choix)
   * @see org.xtext.example.mydsl.myDslform.MyDslformPackage#getContenu_Choix()
   * @model containment="true"
   * @generated
   */
  Choix getChoix();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.myDslform.Contenu#getChoix <em>Choix</em>}' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Choix</em>' containment reference.
   * @see #getChoix()
   * @generated
   */
  void setChoix(Choix value);

} // Contenu
