/**
 */
package org.xtext.example.mydsl.myDslform;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see org.xtext.example.mydsl.myDslform.MyDslformFactory
 * @model kind="package"
 * @generated
 */
public interface MyDslformPackage extends EPackage
{
  /**
   * The package name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNAME = "myDslform";

  /**
   * The package namespace URI.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_URI = "http://www.xtext.org/example/mydsl/MyDslform";

  /**
   * The package namespace name.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  String eNS_PREFIX = "myDslform";

  /**
   * The singleton instance of the package.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   */
  MyDslformPackage eINSTANCE = org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl.init();

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.FormulaireImpl <em>Formulaire</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.FormulaireImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getFormulaire()
   * @generated
   */
  int FORMULAIRE = 0;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULAIRE__ID = 0;

  /**
   * The feature id for the '<em><b>Questions</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULAIRE__QUESTIONS = 1;

  /**
   * The number of structural features of the '<em>Formulaire</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int FORMULAIRE_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.QuestionImpl <em>Question</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.QuestionImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getQuestion()
   * @generated
   */
  int QUESTION = 1;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUESTION__ID = 0;

  /**
   * The feature id for the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUESTION__TEXT = 1;

  /**
   * The feature id for the '<em><b>Choix</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUESTION__CHOIX = 2;

  /**
   * The number of structural features of the '<em>Question</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int QUESTION_FEATURE_COUNT = 3;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.WidgetImpl <em>Widget</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.WidgetImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getWidget()
   * @generated
   */
  int WIDGET = 2;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET__ID = 0;

  /**
   * The number of structural features of the '<em>Widget</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int WIDGET_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.UploadImpl <em>Upload</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.UploadImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getUpload()
   * @generated
   */
  int UPLOAD = 3;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UPLOAD__ID = WIDGET__ID;

  /**
   * The feature id for the '<em><b>Img</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UPLOAD__IMG = WIDGET_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Upload</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int UPLOAD_FEATURE_COUNT = WIDGET_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.RadioButtonImpl <em>Radio Button</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.RadioButtonImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getRadioButton()
   * @generated
   */
  int RADIO_BUTTON = 4;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON__ID = WIDGET__ID;

  /**
   * The feature id for the '<em><b>Reponse</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON__REPONSE = WIDGET_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Radio Button</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int RADIO_BUTTON_FEATURE_COUNT = WIDGET_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.CheckBoxImpl <em>Check Box</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.CheckBoxImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getCheckBox()
   * @generated
   */
  int CHECK_BOX = 5;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_BOX__ID = WIDGET__ID;

  /**
   * The feature id for the '<em><b>Reponse</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_BOX__REPONSE = WIDGET_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Check Box</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHECK_BOX_FEATURE_COUNT = WIDGET_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.TextBoxImpl <em>Text Box</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.TextBoxImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getTextBox()
   * @generated
   */
  int TEXT_BOX = 6;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_BOX__ID = WIDGET__ID;

  /**
   * The feature id for the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_BOX__TEXT = WIDGET_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Text Box</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int TEXT_BOX_FEATURE_COUNT = WIDGET_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.ListImpl <em>List</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.ListImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getList()
   * @generated
   */
  int LIST = 7;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST__ID = WIDGET__ID;

  /**
   * The feature id for the '<em><b>Elements</b></em>' containment reference list.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST__ELEMENTS = WIDGET_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>List</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int LIST_FEATURE_COUNT = WIDGET_FEATURE_COUNT + 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.ContenuImpl <em>Contenu</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.ContenuImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getContenu()
   * @generated
   */
  int CONTENU = 8;

  /**
   * The feature id for the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTENU__ID = 0;

  /**
   * The feature id for the '<em><b>Choix</b></em>' containment reference.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTENU__CHOIX = 1;

  /**
   * The number of structural features of the '<em>Contenu</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CONTENU_FEATURE_COUNT = 2;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.ChoixImpl <em>Choix</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.ChoixImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getChoix()
   * @generated
   */
  int CHOIX = 10;

  /**
   * The feature id for the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHOIX__TEXT = 0;

  /**
   * The number of structural features of the '<em>Choix</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int CHOIX_FEATURE_COUNT = 1;

  /**
   * The meta object id for the '{@link org.xtext.example.mydsl.myDslform.impl.ImageImpl <em>Image</em>}' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @see org.xtext.example.mydsl.myDslform.impl.ImageImpl
   * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getImage()
   * @generated
   */
  int IMAGE = 9;

  /**
   * The feature id for the '<em><b>Text</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMAGE__TEXT = CHOIX__TEXT;

  /**
   * The feature id for the '<em><b>Img</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMAGE__IMG = CHOIX_FEATURE_COUNT + 0;

  /**
   * The number of structural features of the '<em>Image</em>' class.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @generated
   * @ordered
   */
  int IMAGE_FEATURE_COUNT = CHOIX_FEATURE_COUNT + 1;


  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.Formulaire <em>Formulaire</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Formulaire</em>'.
   * @see org.xtext.example.mydsl.myDslform.Formulaire
   * @generated
   */
  EClass getFormulaire();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDslform.Formulaire#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see org.xtext.example.mydsl.myDslform.Formulaire#getId()
   * @see #getFormulaire()
   * @generated
   */
  EAttribute getFormulaire_Id();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDslform.Formulaire#getQuestions <em>Questions</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Questions</em>'.
   * @see org.xtext.example.mydsl.myDslform.Formulaire#getQuestions()
   * @see #getFormulaire()
   * @generated
   */
  EReference getFormulaire_Questions();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.Question <em>Question</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Question</em>'.
   * @see org.xtext.example.mydsl.myDslform.Question
   * @generated
   */
  EClass getQuestion();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDslform.Question#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see org.xtext.example.mydsl.myDslform.Question#getId()
   * @see #getQuestion()
   * @generated
   */
  EAttribute getQuestion_Id();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDslform.Question#getText <em>Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Text</em>'.
   * @see org.xtext.example.mydsl.myDslform.Question#getText()
   * @see #getQuestion()
   * @generated
   */
  EAttribute getQuestion_Text();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDslform.Question#getChoix <em>Choix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Choix</em>'.
   * @see org.xtext.example.mydsl.myDslform.Question#getChoix()
   * @see #getQuestion()
   * @generated
   */
  EReference getQuestion_Choix();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.Widget <em>Widget</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Widget</em>'.
   * @see org.xtext.example.mydsl.myDslform.Widget
   * @generated
   */
  EClass getWidget();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDslform.Widget#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see org.xtext.example.mydsl.myDslform.Widget#getId()
   * @see #getWidget()
   * @generated
   */
  EAttribute getWidget_Id();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.Upload <em>Upload</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Upload</em>'.
   * @see org.xtext.example.mydsl.myDslform.Upload
   * @generated
   */
  EClass getUpload();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDslform.Upload#getImg <em>Img</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Img</em>'.
   * @see org.xtext.example.mydsl.myDslform.Upload#getImg()
   * @see #getUpload()
   * @generated
   */
  EReference getUpload_Img();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.RadioButton <em>Radio Button</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Radio Button</em>'.
   * @see org.xtext.example.mydsl.myDslform.RadioButton
   * @generated
   */
  EClass getRadioButton();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDslform.RadioButton#getReponse <em>Reponse</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Reponse</em>'.
   * @see org.xtext.example.mydsl.myDslform.RadioButton#getReponse()
   * @see #getRadioButton()
   * @generated
   */
  EReference getRadioButton_Reponse();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.CheckBox <em>Check Box</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Check Box</em>'.
   * @see org.xtext.example.mydsl.myDslform.CheckBox
   * @generated
   */
  EClass getCheckBox();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDslform.CheckBox#getReponse <em>Reponse</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Reponse</em>'.
   * @see org.xtext.example.mydsl.myDslform.CheckBox#getReponse()
   * @see #getCheckBox()
   * @generated
   */
  EReference getCheckBox_Reponse();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.TextBox <em>Text Box</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Text Box</em>'.
   * @see org.xtext.example.mydsl.myDslform.TextBox
   * @generated
   */
  EClass getTextBox();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDslform.TextBox#getText <em>Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Text</em>'.
   * @see org.xtext.example.mydsl.myDslform.TextBox#getText()
   * @see #getTextBox()
   * @generated
   */
  EAttribute getTextBox_Text();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.List <em>List</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>List</em>'.
   * @see org.xtext.example.mydsl.myDslform.List
   * @generated
   */
  EClass getList();

  /**
   * Returns the meta object for the containment reference list '{@link org.xtext.example.mydsl.myDslform.List#getElements <em>Elements</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference list '<em>Elements</em>'.
   * @see org.xtext.example.mydsl.myDslform.List#getElements()
   * @see #getList()
   * @generated
   */
  EReference getList_Elements();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.Contenu <em>Contenu</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Contenu</em>'.
   * @see org.xtext.example.mydsl.myDslform.Contenu
   * @generated
   */
  EClass getContenu();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDslform.Contenu#getId <em>Id</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Id</em>'.
   * @see org.xtext.example.mydsl.myDslform.Contenu#getId()
   * @see #getContenu()
   * @generated
   */
  EAttribute getContenu_Id();

  /**
   * Returns the meta object for the containment reference '{@link org.xtext.example.mydsl.myDslform.Contenu#getChoix <em>Choix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the containment reference '<em>Choix</em>'.
   * @see org.xtext.example.mydsl.myDslform.Contenu#getChoix()
   * @see #getContenu()
   * @generated
   */
  EReference getContenu_Choix();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.Image <em>Image</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Image</em>'.
   * @see org.xtext.example.mydsl.myDslform.Image
   * @generated
   */
  EClass getImage();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDslform.Image#getImg <em>Img</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Img</em>'.
   * @see org.xtext.example.mydsl.myDslform.Image#getImg()
   * @see #getImage()
   * @generated
   */
  EAttribute getImage_Img();

  /**
   * Returns the meta object for class '{@link org.xtext.example.mydsl.myDslform.Choix <em>Choix</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for class '<em>Choix</em>'.
   * @see org.xtext.example.mydsl.myDslform.Choix
   * @generated
   */
  EClass getChoix();

  /**
   * Returns the meta object for the attribute '{@link org.xtext.example.mydsl.myDslform.Choix#getText <em>Text</em>}'.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the meta object for the attribute '<em>Text</em>'.
   * @see org.xtext.example.mydsl.myDslform.Choix#getText()
   * @see #getChoix()
   * @generated
   */
  EAttribute getChoix_Text();

  /**
   * Returns the factory that creates the instances of the model.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @return the factory that creates the instances of the model.
   * @generated
   */
  MyDslformFactory getMyDslformFactory();

  /**
   * <!-- begin-user-doc -->
   * Defines literals for the meta objects that represent
   * <ul>
   *   <li>each class,</li>
   *   <li>each feature of each class,</li>
   *   <li>each enum,</li>
   *   <li>and each data type</li>
   * </ul>
   * <!-- end-user-doc -->
   * @generated
   */
  interface Literals
  {
    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.FormulaireImpl <em>Formulaire</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.FormulaireImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getFormulaire()
     * @generated
     */
    EClass FORMULAIRE = eINSTANCE.getFormulaire();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute FORMULAIRE__ID = eINSTANCE.getFormulaire_Id();

    /**
     * The meta object literal for the '<em><b>Questions</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference FORMULAIRE__QUESTIONS = eINSTANCE.getFormulaire_Questions();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.QuestionImpl <em>Question</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.QuestionImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getQuestion()
     * @generated
     */
    EClass QUESTION = eINSTANCE.getQuestion();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUESTION__ID = eINSTANCE.getQuestion_Id();

    /**
     * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute QUESTION__TEXT = eINSTANCE.getQuestion_Text();

    /**
     * The meta object literal for the '<em><b>Choix</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference QUESTION__CHOIX = eINSTANCE.getQuestion_Choix();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.WidgetImpl <em>Widget</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.WidgetImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getWidget()
     * @generated
     */
    EClass WIDGET = eINSTANCE.getWidget();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute WIDGET__ID = eINSTANCE.getWidget_Id();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.UploadImpl <em>Upload</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.UploadImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getUpload()
     * @generated
     */
    EClass UPLOAD = eINSTANCE.getUpload();

    /**
     * The meta object literal for the '<em><b>Img</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference UPLOAD__IMG = eINSTANCE.getUpload_Img();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.RadioButtonImpl <em>Radio Button</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.RadioButtonImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getRadioButton()
     * @generated
     */
    EClass RADIO_BUTTON = eINSTANCE.getRadioButton();

    /**
     * The meta object literal for the '<em><b>Reponse</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference RADIO_BUTTON__REPONSE = eINSTANCE.getRadioButton_Reponse();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.CheckBoxImpl <em>Check Box</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.CheckBoxImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getCheckBox()
     * @generated
     */
    EClass CHECK_BOX = eINSTANCE.getCheckBox();

    /**
     * The meta object literal for the '<em><b>Reponse</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CHECK_BOX__REPONSE = eINSTANCE.getCheckBox_Reponse();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.TextBoxImpl <em>Text Box</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.TextBoxImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getTextBox()
     * @generated
     */
    EClass TEXT_BOX = eINSTANCE.getTextBox();

    /**
     * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute TEXT_BOX__TEXT = eINSTANCE.getTextBox_Text();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.ListImpl <em>List</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.ListImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getList()
     * @generated
     */
    EClass LIST = eINSTANCE.getList();

    /**
     * The meta object literal for the '<em><b>Elements</b></em>' containment reference list feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference LIST__ELEMENTS = eINSTANCE.getList_Elements();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.ContenuImpl <em>Contenu</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.ContenuImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getContenu()
     * @generated
     */
    EClass CONTENU = eINSTANCE.getContenu();

    /**
     * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CONTENU__ID = eINSTANCE.getContenu_Id();

    /**
     * The meta object literal for the '<em><b>Choix</b></em>' containment reference feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EReference CONTENU__CHOIX = eINSTANCE.getContenu_Choix();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.ImageImpl <em>Image</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.ImageImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getImage()
     * @generated
     */
    EClass IMAGE = eINSTANCE.getImage();

    /**
     * The meta object literal for the '<em><b>Img</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute IMAGE__IMG = eINSTANCE.getImage_Img();

    /**
     * The meta object literal for the '{@link org.xtext.example.mydsl.myDslform.impl.ChoixImpl <em>Choix</em>}' class.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @see org.xtext.example.mydsl.myDslform.impl.ChoixImpl
     * @see org.xtext.example.mydsl.myDslform.impl.MyDslformPackageImpl#getChoix()
     * @generated
     */
    EClass CHOIX = eINSTANCE.getChoix();

    /**
     * The meta object literal for the '<em><b>Text</b></em>' attribute feature.
     * <!-- begin-user-doc -->
     * <!-- end-user-doc -->
     * @generated
     */
    EAttribute CHOIX__TEXT = eINSTANCE.getChoix_Text();

  }

} //MyDslformPackage
