package org.xtext.example.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.MyDslformGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslformParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Formulaire'", "'Question'", "'{'", "'Ennonce'", "'}'", "'Widget'", "'Upload'", "'RadioButton'", "'CheckBox'", "'TextBox'", "'List'", "'Contenu'", "'img'"
    };
    public static final int RULE_ID=4;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalMyDslformParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslformParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslformParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g"; }



     	private MyDslformGrammarAccess grammarAccess;
     	
        public InternalMyDslformParser(TokenStream input, MyDslformGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "Formulaire";	
       	}
       	
       	@Override
       	protected MyDslformGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRuleFormulaire"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:67:1: entryRuleFormulaire returns [EObject current=null] : iv_ruleFormulaire= ruleFormulaire EOF ;
    public final EObject entryRuleFormulaire() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleFormulaire = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:68:2: (iv_ruleFormulaire= ruleFormulaire EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:69:2: iv_ruleFormulaire= ruleFormulaire EOF
            {
             newCompositeNode(grammarAccess.getFormulaireRule()); 
            pushFollow(FOLLOW_ruleFormulaire_in_entryRuleFormulaire75);
            iv_ruleFormulaire=ruleFormulaire();

            state._fsp--;

             current =iv_ruleFormulaire; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFormulaire85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleFormulaire"


    // $ANTLR start "ruleFormulaire"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:76:1: ruleFormulaire returns [EObject current=null] : (otherlv_0= 'Formulaire' ( (lv_id_1_0= RULE_ID ) ) ( (lv_questions_2_0= ruleQuestion ) )* ) ;
    public final EObject ruleFormulaire() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        EObject lv_questions_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:79:28: ( (otherlv_0= 'Formulaire' ( (lv_id_1_0= RULE_ID ) ) ( (lv_questions_2_0= ruleQuestion ) )* ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:80:1: (otherlv_0= 'Formulaire' ( (lv_id_1_0= RULE_ID ) ) ( (lv_questions_2_0= ruleQuestion ) )* )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:80:1: (otherlv_0= 'Formulaire' ( (lv_id_1_0= RULE_ID ) ) ( (lv_questions_2_0= ruleQuestion ) )* )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:80:3: otherlv_0= 'Formulaire' ( (lv_id_1_0= RULE_ID ) ) ( (lv_questions_2_0= ruleQuestion ) )*
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_ruleFormulaire122); 

                	newLeafNode(otherlv_0, grammarAccess.getFormulaireAccess().getFormulaireKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:84:1: ( (lv_id_1_0= RULE_ID ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:85:1: (lv_id_1_0= RULE_ID )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:85:1: (lv_id_1_0= RULE_ID )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:86:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleFormulaire139); 

            			newLeafNode(lv_id_1_0, grammarAccess.getFormulaireAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getFormulaireRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:102:2: ( (lv_questions_2_0= ruleQuestion ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==12) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:103:1: (lv_questions_2_0= ruleQuestion )
            	    {
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:103:1: (lv_questions_2_0= ruleQuestion )
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:104:3: lv_questions_2_0= ruleQuestion
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getFormulaireAccess().getQuestionsQuestionParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleQuestion_in_ruleFormulaire165);
            	    lv_questions_2_0=ruleQuestion();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getFormulaireRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"questions",
            	            		lv_questions_2_0, 
            	            		"Question");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleFormulaire"


    // $ANTLR start "entryRuleQuestion"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:128:1: entryRuleQuestion returns [EObject current=null] : iv_ruleQuestion= ruleQuestion EOF ;
    public final EObject entryRuleQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuestion = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:129:2: (iv_ruleQuestion= ruleQuestion EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:130:2: iv_ruleQuestion= ruleQuestion EOF
            {
             newCompositeNode(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_ruleQuestion_in_entryRuleQuestion202);
            iv_ruleQuestion=ruleQuestion();

            state._fsp--;

             current =iv_ruleQuestion; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuestion212); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:137:1: ruleQuestion returns [EObject current=null] : (otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Ennonce' otherlv_4= '{' ( (lv_text_5_0= RULE_STRING ) ) otherlv_6= '}' otherlv_7= 'Widget' otherlv_8= '{' ( (lv_choix_9_0= ruleWidget ) ) otherlv_10= '}' otherlv_11= '}' ) ;
    public final EObject ruleQuestion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token otherlv_3=null;
        Token otherlv_4=null;
        Token lv_text_5_0=null;
        Token otherlv_6=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        Token otherlv_10=null;
        Token otherlv_11=null;
        EObject lv_choix_9_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:140:28: ( (otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Ennonce' otherlv_4= '{' ( (lv_text_5_0= RULE_STRING ) ) otherlv_6= '}' otherlv_7= 'Widget' otherlv_8= '{' ( (lv_choix_9_0= ruleWidget ) ) otherlv_10= '}' otherlv_11= '}' ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:141:1: (otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Ennonce' otherlv_4= '{' ( (lv_text_5_0= RULE_STRING ) ) otherlv_6= '}' otherlv_7= 'Widget' otherlv_8= '{' ( (lv_choix_9_0= ruleWidget ) ) otherlv_10= '}' otherlv_11= '}' )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:141:1: (otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Ennonce' otherlv_4= '{' ( (lv_text_5_0= RULE_STRING ) ) otherlv_6= '}' otherlv_7= 'Widget' otherlv_8= '{' ( (lv_choix_9_0= ruleWidget ) ) otherlv_10= '}' otherlv_11= '}' )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:141:3: otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' otherlv_3= 'Ennonce' otherlv_4= '{' ( (lv_text_5_0= RULE_STRING ) ) otherlv_6= '}' otherlv_7= 'Widget' otherlv_8= '{' ( (lv_choix_9_0= ruleWidget ) ) otherlv_10= '}' otherlv_11= '}'
            {
            otherlv_0=(Token)match(input,12,FOLLOW_12_in_ruleQuestion249); 

                	newLeafNode(otherlv_0, grammarAccess.getQuestionAccess().getQuestionKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:145:1: ( (lv_id_1_0= RULE_ID ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:146:1: (lv_id_1_0= RULE_ID )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:146:1: (lv_id_1_0= RULE_ID )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:147:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQuestion266); 

            			newLeafNode(lv_id_1_0, grammarAccess.getQuestionAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQuestionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleQuestion283); 

                	newLeafNode(otherlv_2, grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2());
                
            otherlv_3=(Token)match(input,14,FOLLOW_14_in_ruleQuestion295); 

                	newLeafNode(otherlv_3, grammarAccess.getQuestionAccess().getEnnonceKeyword_3());
                
            otherlv_4=(Token)match(input,13,FOLLOW_13_in_ruleQuestion307); 

                	newLeafNode(otherlv_4, grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_4());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:175:1: ( (lv_text_5_0= RULE_STRING ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:176:1: (lv_text_5_0= RULE_STRING )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:176:1: (lv_text_5_0= RULE_STRING )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:177:3: lv_text_5_0= RULE_STRING
            {
            lv_text_5_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleQuestion324); 

            			newLeafNode(lv_text_5_0, grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_5_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQuestionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text",
                    		lv_text_5_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_6=(Token)match(input,15,FOLLOW_15_in_ruleQuestion341); 

                	newLeafNode(otherlv_6, grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_6());
                
            otherlv_7=(Token)match(input,16,FOLLOW_16_in_ruleQuestion353); 

                	newLeafNode(otherlv_7, grammarAccess.getQuestionAccess().getWidgetKeyword_7());
                
            otherlv_8=(Token)match(input,13,FOLLOW_13_in_ruleQuestion365); 

                	newLeafNode(otherlv_8, grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_8());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:205:1: ( (lv_choix_9_0= ruleWidget ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:206:1: (lv_choix_9_0= ruleWidget )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:206:1: (lv_choix_9_0= ruleWidget )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:207:3: lv_choix_9_0= ruleWidget
            {
             
            	        newCompositeNode(grammarAccess.getQuestionAccess().getChoixWidgetParserRuleCall_9_0()); 
            	    
            pushFollow(FOLLOW_ruleWidget_in_ruleQuestion386);
            lv_choix_9_0=ruleWidget();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getQuestionRule());
            	        }
                   		set(
                   			current, 
                   			"choix",
                    		lv_choix_9_0, 
                    		"Widget");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_10=(Token)match(input,15,FOLLOW_15_in_ruleQuestion398); 

                	newLeafNode(otherlv_10, grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_10());
                
            otherlv_11=(Token)match(input,15,FOLLOW_15_in_ruleQuestion410); 

                	newLeafNode(otherlv_11, grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_11());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleWidget"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:239:1: entryRuleWidget returns [EObject current=null] : iv_ruleWidget= ruleWidget EOF ;
    public final EObject entryRuleWidget() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleWidget = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:240:2: (iv_ruleWidget= ruleWidget EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:241:2: iv_ruleWidget= ruleWidget EOF
            {
             newCompositeNode(grammarAccess.getWidgetRule()); 
            pushFollow(FOLLOW_ruleWidget_in_entryRuleWidget446);
            iv_ruleWidget=ruleWidget();

            state._fsp--;

             current =iv_ruleWidget; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidget456); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleWidget"


    // $ANTLR start "ruleWidget"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:248:1: ruleWidget returns [EObject current=null] : (this_TextBox_0= ruleTextBox | this_Upload_1= ruleUpload | this_CheckBox_2= ruleCheckBox | this_List_3= ruleList | this_RadioButton_4= ruleRadioButton ) ;
    public final EObject ruleWidget() throws RecognitionException {
        EObject current = null;

        EObject this_TextBox_0 = null;

        EObject this_Upload_1 = null;

        EObject this_CheckBox_2 = null;

        EObject this_List_3 = null;

        EObject this_RadioButton_4 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:251:28: ( (this_TextBox_0= ruleTextBox | this_Upload_1= ruleUpload | this_CheckBox_2= ruleCheckBox | this_List_3= ruleList | this_RadioButton_4= ruleRadioButton ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:252:1: (this_TextBox_0= ruleTextBox | this_Upload_1= ruleUpload | this_CheckBox_2= ruleCheckBox | this_List_3= ruleList | this_RadioButton_4= ruleRadioButton )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:252:1: (this_TextBox_0= ruleTextBox | this_Upload_1= ruleUpload | this_CheckBox_2= ruleCheckBox | this_List_3= ruleList | this_RadioButton_4= ruleRadioButton )
            int alt2=5;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt2=1;
                }
                break;
            case 17:
                {
                alt2=2;
                }
                break;
            case 19:
                {
                alt2=3;
                }
                break;
            case 21:
                {
                alt2=4;
                }
                break;
            case 18:
                {
                alt2=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }

            switch (alt2) {
                case 1 :
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:253:5: this_TextBox_0= ruleTextBox
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetAccess().getTextBoxParserRuleCall_0()); 
                        
                    pushFollow(FOLLOW_ruleTextBox_in_ruleWidget503);
                    this_TextBox_0=ruleTextBox();

                    state._fsp--;

                     
                            current = this_TextBox_0; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 2 :
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:263:5: this_Upload_1= ruleUpload
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetAccess().getUploadParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleUpload_in_ruleWidget530);
                    this_Upload_1=ruleUpload();

                    state._fsp--;

                     
                            current = this_Upload_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 3 :
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:273:5: this_CheckBox_2= ruleCheckBox
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetAccess().getCheckBoxParserRuleCall_2()); 
                        
                    pushFollow(FOLLOW_ruleCheckBox_in_ruleWidget557);
                    this_CheckBox_2=ruleCheckBox();

                    state._fsp--;

                     
                            current = this_CheckBox_2; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 4 :
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:283:5: this_List_3= ruleList
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetAccess().getListParserRuleCall_3()); 
                        
                    pushFollow(FOLLOW_ruleList_in_ruleWidget584);
                    this_List_3=ruleList();

                    state._fsp--;

                     
                            current = this_List_3; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;
                case 5 :
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:293:5: this_RadioButton_4= ruleRadioButton
                    {
                     
                            newCompositeNode(grammarAccess.getWidgetAccess().getRadioButtonParserRuleCall_4()); 
                        
                    pushFollow(FOLLOW_ruleRadioButton_in_ruleWidget611);
                    this_RadioButton_4=ruleRadioButton();

                    state._fsp--;

                     
                            current = this_RadioButton_4; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleWidget"


    // $ANTLR start "entryRuleUpload"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:309:1: entryRuleUpload returns [EObject current=null] : iv_ruleUpload= ruleUpload EOF ;
    public final EObject entryRuleUpload() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleUpload = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:310:2: (iv_ruleUpload= ruleUpload EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:311:2: iv_ruleUpload= ruleUpload EOF
            {
             newCompositeNode(grammarAccess.getUploadRule()); 
            pushFollow(FOLLOW_ruleUpload_in_entryRuleUpload646);
            iv_ruleUpload=ruleUpload();

            state._fsp--;

             current =iv_ruleUpload; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUpload656); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleUpload"


    // $ANTLR start "ruleUpload"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:318:1: ruleUpload returns [EObject current=null] : (otherlv_0= 'Upload' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_img_3_0= ruleImage ) ) otherlv_4= '}' ) ;
    public final EObject ruleUpload() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_img_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:321:28: ( (otherlv_0= 'Upload' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_img_3_0= ruleImage ) ) otherlv_4= '}' ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:322:1: (otherlv_0= 'Upload' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_img_3_0= ruleImage ) ) otherlv_4= '}' )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:322:1: (otherlv_0= 'Upload' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_img_3_0= ruleImage ) ) otherlv_4= '}' )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:322:3: otherlv_0= 'Upload' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_img_3_0= ruleImage ) ) otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,17,FOLLOW_17_in_ruleUpload693); 

                	newLeafNode(otherlv_0, grammarAccess.getUploadAccess().getUploadKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:326:1: ( (lv_id_1_0= RULE_ID ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:327:1: (lv_id_1_0= RULE_ID )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:327:1: (lv_id_1_0= RULE_ID )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:328:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleUpload710); 

            			newLeafNode(lv_id_1_0, grammarAccess.getUploadAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getUploadRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleUpload727); 

                	newLeafNode(otherlv_2, grammarAccess.getUploadAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:348:1: ( (lv_img_3_0= ruleImage ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:349:1: (lv_img_3_0= ruleImage )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:349:1: (lv_img_3_0= ruleImage )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:350:3: lv_img_3_0= ruleImage
            {
             
            	        newCompositeNode(grammarAccess.getUploadAccess().getImgImageParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleImage_in_ruleUpload748);
            lv_img_3_0=ruleImage();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getUploadRule());
            	        }
                   		set(
                   			current, 
                   			"img",
                    		lv_img_3_0, 
                    		"Image");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_15_in_ruleUpload760); 

                	newLeafNode(otherlv_4, grammarAccess.getUploadAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleUpload"


    // $ANTLR start "entryRuleRadioButton"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:378:1: entryRuleRadioButton returns [EObject current=null] : iv_ruleRadioButton= ruleRadioButton EOF ;
    public final EObject entryRuleRadioButton() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleRadioButton = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:379:2: (iv_ruleRadioButton= ruleRadioButton EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:380:2: iv_ruleRadioButton= ruleRadioButton EOF
            {
             newCompositeNode(grammarAccess.getRadioButtonRule()); 
            pushFollow(FOLLOW_ruleRadioButton_in_entryRuleRadioButton796);
            iv_ruleRadioButton=ruleRadioButton();

            state._fsp--;

             current =iv_ruleRadioButton; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadioButton806); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleRadioButton"


    // $ANTLR start "ruleRadioButton"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:387:1: ruleRadioButton returns [EObject current=null] : (otherlv_0= 'RadioButton' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+ ) ;
    public final EObject ruleRadioButton() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        EObject lv_reponse_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:390:28: ( (otherlv_0= 'RadioButton' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+ ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:391:1: (otherlv_0= 'RadioButton' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+ )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:391:1: (otherlv_0= 'RadioButton' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+ )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:391:3: otherlv_0= 'RadioButton' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+
            {
            otherlv_0=(Token)match(input,18,FOLLOW_18_in_ruleRadioButton843); 

                	newLeafNode(otherlv_0, grammarAccess.getRadioButtonAccess().getRadioButtonKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:395:1: ( (lv_id_1_0= RULE_ID ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:396:1: (lv_id_1_0= RULE_ID )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:396:1: (lv_id_1_0= RULE_ID )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:397:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleRadioButton860); 

            			newLeafNode(lv_id_1_0, grammarAccess.getRadioButtonAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getRadioButtonRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:413:2: ( (lv_reponse_2_0= ruleContenu ) )+
            int cnt3=0;
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==22) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:414:1: (lv_reponse_2_0= ruleContenu )
            	    {
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:414:1: (lv_reponse_2_0= ruleContenu )
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:415:3: lv_reponse_2_0= ruleContenu
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getRadioButtonAccess().getReponseContenuParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleContenu_in_ruleRadioButton886);
            	    lv_reponse_2_0=ruleContenu();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getRadioButtonRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"reponse",
            	            		lv_reponse_2_0, 
            	            		"Contenu");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt3 >= 1 ) break loop3;
                        EarlyExitException eee =
                            new EarlyExitException(3, input);
                        throw eee;
                }
                cnt3++;
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleRadioButton"


    // $ANTLR start "entryRuleCheckBox"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:439:1: entryRuleCheckBox returns [EObject current=null] : iv_ruleCheckBox= ruleCheckBox EOF ;
    public final EObject entryRuleCheckBox() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleCheckBox = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:440:2: (iv_ruleCheckBox= ruleCheckBox EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:441:2: iv_ruleCheckBox= ruleCheckBox EOF
            {
             newCompositeNode(grammarAccess.getCheckBoxRule()); 
            pushFollow(FOLLOW_ruleCheckBox_in_entryRuleCheckBox923);
            iv_ruleCheckBox=ruleCheckBox();

            state._fsp--;

             current =iv_ruleCheckBox; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckBox933); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleCheckBox"


    // $ANTLR start "ruleCheckBox"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:448:1: ruleCheckBox returns [EObject current=null] : (otherlv_0= 'CheckBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+ ) ;
    public final EObject ruleCheckBox() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        EObject lv_reponse_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:451:28: ( (otherlv_0= 'CheckBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+ ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:452:1: (otherlv_0= 'CheckBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+ )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:452:1: (otherlv_0= 'CheckBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+ )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:452:3: otherlv_0= 'CheckBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_reponse_2_0= ruleContenu ) )+
            {
            otherlv_0=(Token)match(input,19,FOLLOW_19_in_ruleCheckBox970); 

                	newLeafNode(otherlv_0, grammarAccess.getCheckBoxAccess().getCheckBoxKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:456:1: ( (lv_id_1_0= RULE_ID ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:457:1: (lv_id_1_0= RULE_ID )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:457:1: (lv_id_1_0= RULE_ID )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:458:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleCheckBox987); 

            			newLeafNode(lv_id_1_0, grammarAccess.getCheckBoxAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getCheckBoxRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:474:2: ( (lv_reponse_2_0= ruleContenu ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==22) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:475:1: (lv_reponse_2_0= ruleContenu )
            	    {
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:475:1: (lv_reponse_2_0= ruleContenu )
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:476:3: lv_reponse_2_0= ruleContenu
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getCheckBoxAccess().getReponseContenuParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleContenu_in_ruleCheckBox1013);
            	    lv_reponse_2_0=ruleContenu();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getCheckBoxRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"reponse",
            	            		lv_reponse_2_0, 
            	            		"Contenu");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleCheckBox"


    // $ANTLR start "entryRuleTextBox"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:500:1: entryRuleTextBox returns [EObject current=null] : iv_ruleTextBox= ruleTextBox EOF ;
    public final EObject entryRuleTextBox() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleTextBox = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:501:2: (iv_ruleTextBox= ruleTextBox EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:502:2: iv_ruleTextBox= ruleTextBox EOF
            {
             newCompositeNode(grammarAccess.getTextBoxRule()); 
            pushFollow(FOLLOW_ruleTextBox_in_entryRuleTextBox1050);
            iv_ruleTextBox=ruleTextBox();

            state._fsp--;

             current =iv_ruleTextBox; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTextBox1060); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleTextBox"


    // $ANTLR start "ruleTextBox"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:509:1: ruleTextBox returns [EObject current=null] : (otherlv_0= 'TextBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_text_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleTextBox() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token lv_text_2_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:512:28: ( (otherlv_0= 'TextBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_text_2_0= RULE_STRING ) ) ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:513:1: (otherlv_0= 'TextBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_text_2_0= RULE_STRING ) ) )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:513:1: (otherlv_0= 'TextBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_text_2_0= RULE_STRING ) ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:513:3: otherlv_0= 'TextBox' ( (lv_id_1_0= RULE_ID ) ) ( (lv_text_2_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,20,FOLLOW_20_in_ruleTextBox1097); 

                	newLeafNode(otherlv_0, grammarAccess.getTextBoxAccess().getTextBoxKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:517:1: ( (lv_id_1_0= RULE_ID ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:518:1: (lv_id_1_0= RULE_ID )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:518:1: (lv_id_1_0= RULE_ID )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:519:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleTextBox1114); 

            			newLeafNode(lv_id_1_0, grammarAccess.getTextBoxAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTextBoxRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:535:2: ( (lv_text_2_0= RULE_STRING ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:536:1: (lv_text_2_0= RULE_STRING )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:536:1: (lv_text_2_0= RULE_STRING )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:537:3: lv_text_2_0= RULE_STRING
            {
            lv_text_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleTextBox1136); 

            			newLeafNode(lv_text_2_0, grammarAccess.getTextBoxAccess().getTextSTRINGTerminalRuleCall_2_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getTextBoxRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text",
                    		lv_text_2_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleTextBox"


    // $ANTLR start "entryRuleList"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:561:1: entryRuleList returns [EObject current=null] : iv_ruleList= ruleList EOF ;
    public final EObject entryRuleList() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleList = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:562:2: (iv_ruleList= ruleList EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:563:2: iv_ruleList= ruleList EOF
            {
             newCompositeNode(grammarAccess.getListRule()); 
            pushFollow(FOLLOW_ruleList_in_entryRuleList1177);
            iv_ruleList=ruleList();

            state._fsp--;

             current =iv_ruleList; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleList1187); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleList"


    // $ANTLR start "ruleList"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:570:1: ruleList returns [EObject current=null] : (otherlv_0= 'List' ( (lv_id_1_0= RULE_ID ) ) ( (lv_elements_2_0= ruleContenu ) )+ ) ;
    public final EObject ruleList() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        EObject lv_elements_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:573:28: ( (otherlv_0= 'List' ( (lv_id_1_0= RULE_ID ) ) ( (lv_elements_2_0= ruleContenu ) )+ ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:574:1: (otherlv_0= 'List' ( (lv_id_1_0= RULE_ID ) ) ( (lv_elements_2_0= ruleContenu ) )+ )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:574:1: (otherlv_0= 'List' ( (lv_id_1_0= RULE_ID ) ) ( (lv_elements_2_0= ruleContenu ) )+ )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:574:3: otherlv_0= 'List' ( (lv_id_1_0= RULE_ID ) ) ( (lv_elements_2_0= ruleContenu ) )+
            {
            otherlv_0=(Token)match(input,21,FOLLOW_21_in_ruleList1224); 

                	newLeafNode(otherlv_0, grammarAccess.getListAccess().getListKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:578:1: ( (lv_id_1_0= RULE_ID ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:579:1: (lv_id_1_0= RULE_ID )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:579:1: (lv_id_1_0= RULE_ID )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:580:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleList1241); 

            			newLeafNode(lv_id_1_0, grammarAccess.getListAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getListRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:596:2: ( (lv_elements_2_0= ruleContenu ) )+
            int cnt5=0;
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==22) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:597:1: (lv_elements_2_0= ruleContenu )
            	    {
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:597:1: (lv_elements_2_0= ruleContenu )
            	    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:598:3: lv_elements_2_0= ruleContenu
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getListAccess().getElementsContenuParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleContenu_in_ruleList1267);
            	    lv_elements_2_0=ruleContenu();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getListRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"elements",
            	            		lv_elements_2_0, 
            	            		"Contenu");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt5 >= 1 ) break loop5;
                        EarlyExitException eee =
                            new EarlyExitException(5, input);
                        throw eee;
                }
                cnt5++;
            } while (true);


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleList"


    // $ANTLR start "entryRuleContenu"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:622:1: entryRuleContenu returns [EObject current=null] : iv_ruleContenu= ruleContenu EOF ;
    public final EObject entryRuleContenu() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleContenu = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:623:2: (iv_ruleContenu= ruleContenu EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:624:2: iv_ruleContenu= ruleContenu EOF
            {
             newCompositeNode(grammarAccess.getContenuRule()); 
            pushFollow(FOLLOW_ruleContenu_in_entryRuleContenu1304);
            iv_ruleContenu=ruleContenu();

            state._fsp--;

             current =iv_ruleContenu; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleContenu1314); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleContenu"


    // $ANTLR start "ruleContenu"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:631:1: ruleContenu returns [EObject current=null] : (otherlv_0= 'Contenu' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_choix_3_0= ruleChoix ) ) otherlv_4= '}' ) ;
    public final EObject ruleContenu() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_choix_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:634:28: ( (otherlv_0= 'Contenu' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_choix_3_0= ruleChoix ) ) otherlv_4= '}' ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:635:1: (otherlv_0= 'Contenu' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_choix_3_0= ruleChoix ) ) otherlv_4= '}' )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:635:1: (otherlv_0= 'Contenu' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_choix_3_0= ruleChoix ) ) otherlv_4= '}' )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:635:3: otherlv_0= 'Contenu' ( (lv_id_1_0= RULE_ID ) ) otherlv_2= '{' ( (lv_choix_3_0= ruleChoix ) ) otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,22,FOLLOW_22_in_ruleContenu1351); 

                	newLeafNode(otherlv_0, grammarAccess.getContenuAccess().getContenuKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:639:1: ( (lv_id_1_0= RULE_ID ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:640:1: (lv_id_1_0= RULE_ID )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:640:1: (lv_id_1_0= RULE_ID )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:641:3: lv_id_1_0= RULE_ID
            {
            lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleContenu1368); 

            			newLeafNode(lv_id_1_0, grammarAccess.getContenuAccess().getIdIDTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getContenuRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"id",
                    		lv_id_1_0, 
                    		"ID");
            	    

            }


            }

            otherlv_2=(Token)match(input,13,FOLLOW_13_in_ruleContenu1385); 

                	newLeafNode(otherlv_2, grammarAccess.getContenuAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:661:1: ( (lv_choix_3_0= ruleChoix ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:662:1: (lv_choix_3_0= ruleChoix )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:662:1: (lv_choix_3_0= ruleChoix )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:663:3: lv_choix_3_0= ruleChoix
            {
             
            	        newCompositeNode(grammarAccess.getContenuAccess().getChoixChoixParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleChoix_in_ruleContenu1406);
            lv_choix_3_0=ruleChoix();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getContenuRule());
            	        }
                   		set(
                   			current, 
                   			"choix",
                    		lv_choix_3_0, 
                    		"Choix");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,15,FOLLOW_15_in_ruleContenu1418); 

                	newLeafNode(otherlv_4, grammarAccess.getContenuAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleContenu"


    // $ANTLR start "entryRuleImage"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:691:1: entryRuleImage returns [EObject current=null] : iv_ruleImage= ruleImage EOF ;
    public final EObject entryRuleImage() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleImage = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:692:2: (iv_ruleImage= ruleImage EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:693:2: iv_ruleImage= ruleImage EOF
            {
             newCompositeNode(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_ruleImage_in_entryRuleImage1454);
            iv_ruleImage=ruleImage();

            state._fsp--;

             current =iv_ruleImage; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImage1464); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:700:1: ruleImage returns [EObject current=null] : (otherlv_0= 'img' ( (lv_img_1_0= RULE_STRING ) ) ) ;
    public final EObject ruleImage() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_img_1_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:703:28: ( (otherlv_0= 'img' ( (lv_img_1_0= RULE_STRING ) ) ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:704:1: (otherlv_0= 'img' ( (lv_img_1_0= RULE_STRING ) ) )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:704:1: (otherlv_0= 'img' ( (lv_img_1_0= RULE_STRING ) ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:704:3: otherlv_0= 'img' ( (lv_img_1_0= RULE_STRING ) )
            {
            otherlv_0=(Token)match(input,23,FOLLOW_23_in_ruleImage1501); 

                	newLeafNode(otherlv_0, grammarAccess.getImageAccess().getImgKeyword_0());
                
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:708:1: ( (lv_img_1_0= RULE_STRING ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:709:1: (lv_img_1_0= RULE_STRING )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:709:1: (lv_img_1_0= RULE_STRING )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:710:3: lv_img_1_0= RULE_STRING
            {
            lv_img_1_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleImage1518); 

            			newLeafNode(lv_img_1_0, grammarAccess.getImageAccess().getImgSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getImageRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"img",
                    		lv_img_1_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleChoix"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:734:1: entryRuleChoix returns [EObject current=null] : iv_ruleChoix= ruleChoix EOF ;
    public final EObject entryRuleChoix() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleChoix = null;


        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:735:2: (iv_ruleChoix= ruleChoix EOF )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:736:2: iv_ruleChoix= ruleChoix EOF
            {
             newCompositeNode(grammarAccess.getChoixRule()); 
            pushFollow(FOLLOW_ruleChoix_in_entryRuleChoix1559);
            iv_ruleChoix=ruleChoix();

            state._fsp--;

             current =iv_ruleChoix; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleChoix1569); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleChoix"


    // $ANTLR start "ruleChoix"
    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:743:1: ruleChoix returns [EObject current=null] : ( ( (lv_text_0_0= RULE_STRING ) ) | this_Image_1= ruleImage ) ;
    public final EObject ruleChoix() throws RecognitionException {
        EObject current = null;

        Token lv_text_0_0=null;
        EObject this_Image_1 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:746:28: ( ( ( (lv_text_0_0= RULE_STRING ) ) | this_Image_1= ruleImage ) )
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:747:1: ( ( (lv_text_0_0= RULE_STRING ) ) | this_Image_1= ruleImage )
            {
            // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:747:1: ( ( (lv_text_0_0= RULE_STRING ) ) | this_Image_1= ruleImage )
            int alt6=2;
            int LA6_0 = input.LA(1);

            if ( (LA6_0==RULE_STRING) ) {
                alt6=1;
            }
            else if ( (LA6_0==23) ) {
                alt6=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 6, 0, input);

                throw nvae;
            }
            switch (alt6) {
                case 1 :
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:747:2: ( (lv_text_0_0= RULE_STRING ) )
                    {
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:747:2: ( (lv_text_0_0= RULE_STRING ) )
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:748:1: (lv_text_0_0= RULE_STRING )
                    {
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:748:1: (lv_text_0_0= RULE_STRING )
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:749:3: lv_text_0_0= RULE_STRING
                    {
                    lv_text_0_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleChoix1611); 

                    			newLeafNode(lv_text_0_0, grammarAccess.getChoixAccess().getTextSTRINGTerminalRuleCall_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getChoixRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"text",
                            		lv_text_0_0, 
                            		"STRING");
                    	    

                    }


                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.mydslform/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalMyDslform.g:767:5: this_Image_1= ruleImage
                    {
                     
                            newCompositeNode(grammarAccess.getChoixAccess().getImageParserRuleCall_1()); 
                        
                    pushFollow(FOLLOW_ruleImage_in_ruleChoix1644);
                    this_Image_1=ruleImage();

                    state._fsp--;

                     
                            current = this_Image_1; 
                            afterParserOrEnumRuleCall();
                        

                    }
                    break;

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleChoix"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleFormulaire_in_entryRuleFormulaire75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFormulaire85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_ruleFormulaire122 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleFormulaire139 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_ruleQuestion_in_ruleFormulaire165 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_ruleQuestion_in_entryRuleQuestion202 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuestion212 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_ruleQuestion249 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQuestion266 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleQuestion283 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_14_in_ruleQuestion295 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleQuestion307 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleQuestion324 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleQuestion341 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleQuestion353 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleQuestion365 = new BitSet(new long[]{0x00000000003E0000L});
    public static final BitSet FOLLOW_ruleWidget_in_ruleQuestion386 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleQuestion398 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleQuestion410 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidget_in_entryRuleWidget446 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidget456 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTextBox_in_ruleWidget503 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUpload_in_ruleWidget530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckBox_in_ruleWidget557 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleList_in_ruleWidget584 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButton_in_ruleWidget611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUpload_in_entryRuleUpload646 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUpload656 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_ruleUpload693 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleUpload710 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleUpload727 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_ruleImage_in_ruleUpload748 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleUpload760 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButton_in_entryRuleRadioButton796 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadioButton806 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_ruleRadioButton843 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleRadioButton860 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_ruleContenu_in_ruleRadioButton886 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_ruleCheckBox_in_entryRuleCheckBox923 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckBox933 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_ruleCheckBox970 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleCheckBox987 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_ruleContenu_in_ruleCheckBox1013 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_ruleTextBox_in_entryRuleTextBox1050 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTextBox1060 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_ruleTextBox1097 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleTextBox1114 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleTextBox1136 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleList_in_entryRuleList1177 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleList1187 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_ruleList1224 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleList1241 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_ruleContenu_in_ruleList1267 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_ruleContenu_in_entryRuleContenu1304 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleContenu1314 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_ruleContenu1351 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleContenu1368 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleContenu1385 = new BitSet(new long[]{0x0000000000800020L});
    public static final BitSet FOLLOW_ruleChoix_in_ruleContenu1406 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_15_in_ruleContenu1418 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_entryRuleImage1454 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImage1464 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_ruleImage1501 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleImage1518 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChoix_in_entryRuleChoix1559 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleChoix1569 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleChoix1611 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_ruleChoix1644 = new BitSet(new long[]{0x0000000000000002L});

}