package org.xtext.example.mydsl.ui.contentassist.antlr.internal; 

import java.io.InputStream;
import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.AbstractInternalContentAssistParser;
import org.eclipse.xtext.ui.editor.contentassist.antlr.internal.DFA;
import org.xtext.example.mydsl.services.MyDslformGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalMyDslformParser extends AbstractInternalContentAssistParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'Formulaire'", "'Question'", "'{'", "'Ennonce'", "'}'", "'Widget'", "'Upload'", "'RadioButton'", "'CheckBox'", "'TextBox'", "'List'", "'Contenu'", "'img'"
    };
    public static final int RULE_ID=4;
    public static final int T__23=23;
    public static final int T__22=22;
    public static final int RULE_ANY_OTHER=10;
    public static final int T__21=21;
    public static final int T__20=20;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;
    public static final int T__19=19;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__18=18;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;

    // delegates
    // delegators


        public InternalMyDslformParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalMyDslformParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalMyDslformParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g"; }


     
     	private MyDslformGrammarAccess grammarAccess;
     	
        public void setGrammarAccess(MyDslformGrammarAccess grammarAccess) {
        	this.grammarAccess = grammarAccess;
        }
        
        @Override
        protected Grammar getGrammar() {
        	return grammarAccess.getGrammar();
        }
        
        @Override
        protected String getValueForTokenName(String tokenName) {
        	return tokenName;
        }




    // $ANTLR start "entryRuleFormulaire"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:60:1: entryRuleFormulaire : ruleFormulaire EOF ;
    public final void entryRuleFormulaire() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:61:1: ( ruleFormulaire EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:62:1: ruleFormulaire EOF
            {
             before(grammarAccess.getFormulaireRule()); 
            pushFollow(FOLLOW_ruleFormulaire_in_entryRuleFormulaire61);
            ruleFormulaire();

            state._fsp--;

             after(grammarAccess.getFormulaireRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleFormulaire68); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleFormulaire"


    // $ANTLR start "ruleFormulaire"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:69:1: ruleFormulaire : ( ( rule__Formulaire__Group__0 ) ) ;
    public final void ruleFormulaire() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:73:2: ( ( ( rule__Formulaire__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:74:1: ( ( rule__Formulaire__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:74:1: ( ( rule__Formulaire__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:75:1: ( rule__Formulaire__Group__0 )
            {
             before(grammarAccess.getFormulaireAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:76:1: ( rule__Formulaire__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:76:2: rule__Formulaire__Group__0
            {
            pushFollow(FOLLOW_rule__Formulaire__Group__0_in_ruleFormulaire94);
            rule__Formulaire__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getFormulaireAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleFormulaire"


    // $ANTLR start "entryRuleQuestion"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:88:1: entryRuleQuestion : ruleQuestion EOF ;
    public final void entryRuleQuestion() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:89:1: ( ruleQuestion EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:90:1: ruleQuestion EOF
            {
             before(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_ruleQuestion_in_entryRuleQuestion121);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getQuestionRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuestion128); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:97:1: ruleQuestion : ( ( rule__Question__Group__0 ) ) ;
    public final void ruleQuestion() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:101:2: ( ( ( rule__Question__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:102:1: ( ( rule__Question__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:102:1: ( ( rule__Question__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:103:1: ( rule__Question__Group__0 )
            {
             before(grammarAccess.getQuestionAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:104:1: ( rule__Question__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:104:2: rule__Question__Group__0
            {
            pushFollow(FOLLOW_rule__Question__Group__0_in_ruleQuestion154);
            rule__Question__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleWidget"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:116:1: entryRuleWidget : ruleWidget EOF ;
    public final void entryRuleWidget() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:117:1: ( ruleWidget EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:118:1: ruleWidget EOF
            {
             before(grammarAccess.getWidgetRule()); 
            pushFollow(FOLLOW_ruleWidget_in_entryRuleWidget181);
            ruleWidget();

            state._fsp--;

             after(grammarAccess.getWidgetRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleWidget188); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleWidget"


    // $ANTLR start "ruleWidget"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:125:1: ruleWidget : ( ( rule__Widget__Alternatives ) ) ;
    public final void ruleWidget() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:129:2: ( ( ( rule__Widget__Alternatives ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:130:1: ( ( rule__Widget__Alternatives ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:130:1: ( ( rule__Widget__Alternatives ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:131:1: ( rule__Widget__Alternatives )
            {
             before(grammarAccess.getWidgetAccess().getAlternatives()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:132:1: ( rule__Widget__Alternatives )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:132:2: rule__Widget__Alternatives
            {
            pushFollow(FOLLOW_rule__Widget__Alternatives_in_ruleWidget214);
            rule__Widget__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getWidgetAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleWidget"


    // $ANTLR start "entryRuleUpload"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:144:1: entryRuleUpload : ruleUpload EOF ;
    public final void entryRuleUpload() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:145:1: ( ruleUpload EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:146:1: ruleUpload EOF
            {
             before(grammarAccess.getUploadRule()); 
            pushFollow(FOLLOW_ruleUpload_in_entryRuleUpload241);
            ruleUpload();

            state._fsp--;

             after(grammarAccess.getUploadRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleUpload248); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleUpload"


    // $ANTLR start "ruleUpload"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:153:1: ruleUpload : ( ( rule__Upload__Group__0 ) ) ;
    public final void ruleUpload() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:157:2: ( ( ( rule__Upload__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:158:1: ( ( rule__Upload__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:158:1: ( ( rule__Upload__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:159:1: ( rule__Upload__Group__0 )
            {
             before(grammarAccess.getUploadAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:160:1: ( rule__Upload__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:160:2: rule__Upload__Group__0
            {
            pushFollow(FOLLOW_rule__Upload__Group__0_in_ruleUpload274);
            rule__Upload__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getUploadAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleUpload"


    // $ANTLR start "entryRuleRadioButton"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:172:1: entryRuleRadioButton : ruleRadioButton EOF ;
    public final void entryRuleRadioButton() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:173:1: ( ruleRadioButton EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:174:1: ruleRadioButton EOF
            {
             before(grammarAccess.getRadioButtonRule()); 
            pushFollow(FOLLOW_ruleRadioButton_in_entryRuleRadioButton301);
            ruleRadioButton();

            state._fsp--;

             after(grammarAccess.getRadioButtonRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleRadioButton308); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleRadioButton"


    // $ANTLR start "ruleRadioButton"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:181:1: ruleRadioButton : ( ( rule__RadioButton__Group__0 ) ) ;
    public final void ruleRadioButton() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:185:2: ( ( ( rule__RadioButton__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:186:1: ( ( rule__RadioButton__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:186:1: ( ( rule__RadioButton__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:187:1: ( rule__RadioButton__Group__0 )
            {
             before(grammarAccess.getRadioButtonAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:188:1: ( rule__RadioButton__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:188:2: rule__RadioButton__Group__0
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__0_in_ruleRadioButton334);
            rule__RadioButton__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getRadioButtonAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleRadioButton"


    // $ANTLR start "entryRuleCheckBox"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:200:1: entryRuleCheckBox : ruleCheckBox EOF ;
    public final void entryRuleCheckBox() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:201:1: ( ruleCheckBox EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:202:1: ruleCheckBox EOF
            {
             before(grammarAccess.getCheckBoxRule()); 
            pushFollow(FOLLOW_ruleCheckBox_in_entryRuleCheckBox361);
            ruleCheckBox();

            state._fsp--;

             after(grammarAccess.getCheckBoxRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleCheckBox368); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleCheckBox"


    // $ANTLR start "ruleCheckBox"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:209:1: ruleCheckBox : ( ( rule__CheckBox__Group__0 ) ) ;
    public final void ruleCheckBox() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:213:2: ( ( ( rule__CheckBox__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:214:1: ( ( rule__CheckBox__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:214:1: ( ( rule__CheckBox__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:215:1: ( rule__CheckBox__Group__0 )
            {
             before(grammarAccess.getCheckBoxAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:216:1: ( rule__CheckBox__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:216:2: rule__CheckBox__Group__0
            {
            pushFollow(FOLLOW_rule__CheckBox__Group__0_in_ruleCheckBox394);
            rule__CheckBox__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getCheckBoxAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleCheckBox"


    // $ANTLR start "entryRuleTextBox"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:228:1: entryRuleTextBox : ruleTextBox EOF ;
    public final void entryRuleTextBox() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:229:1: ( ruleTextBox EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:230:1: ruleTextBox EOF
            {
             before(grammarAccess.getTextBoxRule()); 
            pushFollow(FOLLOW_ruleTextBox_in_entryRuleTextBox421);
            ruleTextBox();

            state._fsp--;

             after(grammarAccess.getTextBoxRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleTextBox428); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleTextBox"


    // $ANTLR start "ruleTextBox"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:237:1: ruleTextBox : ( ( rule__TextBox__Group__0 ) ) ;
    public final void ruleTextBox() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:241:2: ( ( ( rule__TextBox__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:242:1: ( ( rule__TextBox__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:242:1: ( ( rule__TextBox__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:243:1: ( rule__TextBox__Group__0 )
            {
             before(grammarAccess.getTextBoxAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:244:1: ( rule__TextBox__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:244:2: rule__TextBox__Group__0
            {
            pushFollow(FOLLOW_rule__TextBox__Group__0_in_ruleTextBox454);
            rule__TextBox__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getTextBoxAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleTextBox"


    // $ANTLR start "entryRuleList"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:256:1: entryRuleList : ruleList EOF ;
    public final void entryRuleList() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:257:1: ( ruleList EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:258:1: ruleList EOF
            {
             before(grammarAccess.getListRule()); 
            pushFollow(FOLLOW_ruleList_in_entryRuleList481);
            ruleList();

            state._fsp--;

             after(grammarAccess.getListRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleList488); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleList"


    // $ANTLR start "ruleList"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:265:1: ruleList : ( ( rule__List__Group__0 ) ) ;
    public final void ruleList() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:269:2: ( ( ( rule__List__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:270:1: ( ( rule__List__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:270:1: ( ( rule__List__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:271:1: ( rule__List__Group__0 )
            {
             before(grammarAccess.getListAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:272:1: ( rule__List__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:272:2: rule__List__Group__0
            {
            pushFollow(FOLLOW_rule__List__Group__0_in_ruleList514);
            rule__List__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getListAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleList"


    // $ANTLR start "entryRuleContenu"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:284:1: entryRuleContenu : ruleContenu EOF ;
    public final void entryRuleContenu() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:285:1: ( ruleContenu EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:286:1: ruleContenu EOF
            {
             before(grammarAccess.getContenuRule()); 
            pushFollow(FOLLOW_ruleContenu_in_entryRuleContenu541);
            ruleContenu();

            state._fsp--;

             after(grammarAccess.getContenuRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleContenu548); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleContenu"


    // $ANTLR start "ruleContenu"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:293:1: ruleContenu : ( ( rule__Contenu__Group__0 ) ) ;
    public final void ruleContenu() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:297:2: ( ( ( rule__Contenu__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:298:1: ( ( rule__Contenu__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:298:1: ( ( rule__Contenu__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:299:1: ( rule__Contenu__Group__0 )
            {
             before(grammarAccess.getContenuAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:300:1: ( rule__Contenu__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:300:2: rule__Contenu__Group__0
            {
            pushFollow(FOLLOW_rule__Contenu__Group__0_in_ruleContenu574);
            rule__Contenu__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getContenuAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleContenu"


    // $ANTLR start "entryRuleImage"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:312:1: entryRuleImage : ruleImage EOF ;
    public final void entryRuleImage() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:313:1: ( ruleImage EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:314:1: ruleImage EOF
            {
             before(grammarAccess.getImageRule()); 
            pushFollow(FOLLOW_ruleImage_in_entryRuleImage601);
            ruleImage();

            state._fsp--;

             after(grammarAccess.getImageRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleImage608); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleImage"


    // $ANTLR start "ruleImage"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:321:1: ruleImage : ( ( rule__Image__Group__0 ) ) ;
    public final void ruleImage() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:325:2: ( ( ( rule__Image__Group__0 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:326:1: ( ( rule__Image__Group__0 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:326:1: ( ( rule__Image__Group__0 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:327:1: ( rule__Image__Group__0 )
            {
             before(grammarAccess.getImageAccess().getGroup()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:328:1: ( rule__Image__Group__0 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:328:2: rule__Image__Group__0
            {
            pushFollow(FOLLOW_rule__Image__Group__0_in_ruleImage634);
            rule__Image__Group__0();

            state._fsp--;


            }

             after(grammarAccess.getImageAccess().getGroup()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleImage"


    // $ANTLR start "entryRuleChoix"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:340:1: entryRuleChoix : ruleChoix EOF ;
    public final void entryRuleChoix() throws RecognitionException {
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:341:1: ( ruleChoix EOF )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:342:1: ruleChoix EOF
            {
             before(grammarAccess.getChoixRule()); 
            pushFollow(FOLLOW_ruleChoix_in_entryRuleChoix661);
            ruleChoix();

            state._fsp--;

             after(grammarAccess.getChoixRule()); 
            match(input,EOF,FOLLOW_EOF_in_entryRuleChoix668); 

            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {
        }
        return ;
    }
    // $ANTLR end "entryRuleChoix"


    // $ANTLR start "ruleChoix"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:349:1: ruleChoix : ( ( rule__Choix__Alternatives ) ) ;
    public final void ruleChoix() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:353:2: ( ( ( rule__Choix__Alternatives ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:354:1: ( ( rule__Choix__Alternatives ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:354:1: ( ( rule__Choix__Alternatives ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:355:1: ( rule__Choix__Alternatives )
            {
             before(grammarAccess.getChoixAccess().getAlternatives()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:356:1: ( rule__Choix__Alternatives )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:356:2: rule__Choix__Alternatives
            {
            pushFollow(FOLLOW_rule__Choix__Alternatives_in_ruleChoix694);
            rule__Choix__Alternatives();

            state._fsp--;


            }

             after(grammarAccess.getChoixAccess().getAlternatives()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "ruleChoix"


    // $ANTLR start "rule__Widget__Alternatives"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:368:1: rule__Widget__Alternatives : ( ( ruleTextBox ) | ( ruleUpload ) | ( ruleCheckBox ) | ( ruleList ) | ( ruleRadioButton ) );
    public final void rule__Widget__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:372:1: ( ( ruleTextBox ) | ( ruleUpload ) | ( ruleCheckBox ) | ( ruleList ) | ( ruleRadioButton ) )
            int alt1=5;
            switch ( input.LA(1) ) {
            case 20:
                {
                alt1=1;
                }
                break;
            case 17:
                {
                alt1=2;
                }
                break;
            case 19:
                {
                alt1=3;
                }
                break;
            case 21:
                {
                alt1=4;
                }
                break;
            case 18:
                {
                alt1=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 1, 0, input);

                throw nvae;
            }

            switch (alt1) {
                case 1 :
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:373:1: ( ruleTextBox )
                    {
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:373:1: ( ruleTextBox )
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:374:1: ruleTextBox
                    {
                     before(grammarAccess.getWidgetAccess().getTextBoxParserRuleCall_0()); 
                    pushFollow(FOLLOW_ruleTextBox_in_rule__Widget__Alternatives730);
                    ruleTextBox();

                    state._fsp--;

                     after(grammarAccess.getWidgetAccess().getTextBoxParserRuleCall_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:379:6: ( ruleUpload )
                    {
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:379:6: ( ruleUpload )
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:380:1: ruleUpload
                    {
                     before(grammarAccess.getWidgetAccess().getUploadParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleUpload_in_rule__Widget__Alternatives747);
                    ruleUpload();

                    state._fsp--;

                     after(grammarAccess.getWidgetAccess().getUploadParserRuleCall_1()); 

                    }


                    }
                    break;
                case 3 :
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:385:6: ( ruleCheckBox )
                    {
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:385:6: ( ruleCheckBox )
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:386:1: ruleCheckBox
                    {
                     before(grammarAccess.getWidgetAccess().getCheckBoxParserRuleCall_2()); 
                    pushFollow(FOLLOW_ruleCheckBox_in_rule__Widget__Alternatives764);
                    ruleCheckBox();

                    state._fsp--;

                     after(grammarAccess.getWidgetAccess().getCheckBoxParserRuleCall_2()); 

                    }


                    }
                    break;
                case 4 :
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:391:6: ( ruleList )
                    {
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:391:6: ( ruleList )
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:392:1: ruleList
                    {
                     before(grammarAccess.getWidgetAccess().getListParserRuleCall_3()); 
                    pushFollow(FOLLOW_ruleList_in_rule__Widget__Alternatives781);
                    ruleList();

                    state._fsp--;

                     after(grammarAccess.getWidgetAccess().getListParserRuleCall_3()); 

                    }


                    }
                    break;
                case 5 :
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:397:6: ( ruleRadioButton )
                    {
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:397:6: ( ruleRadioButton )
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:398:1: ruleRadioButton
                    {
                     before(grammarAccess.getWidgetAccess().getRadioButtonParserRuleCall_4()); 
                    pushFollow(FOLLOW_ruleRadioButton_in_rule__Widget__Alternatives798);
                    ruleRadioButton();

                    state._fsp--;

                     after(grammarAccess.getWidgetAccess().getRadioButtonParserRuleCall_4()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Widget__Alternatives"


    // $ANTLR start "rule__Choix__Alternatives"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:408:1: rule__Choix__Alternatives : ( ( ( rule__Choix__TextAssignment_0 ) ) | ( ruleImage ) );
    public final void rule__Choix__Alternatives() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:412:1: ( ( ( rule__Choix__TextAssignment_0 ) ) | ( ruleImage ) )
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_STRING) ) {
                alt2=1;
            }
            else if ( (LA2_0==23) ) {
                alt2=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 2, 0, input);

                throw nvae;
            }
            switch (alt2) {
                case 1 :
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:413:1: ( ( rule__Choix__TextAssignment_0 ) )
                    {
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:413:1: ( ( rule__Choix__TextAssignment_0 ) )
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:414:1: ( rule__Choix__TextAssignment_0 )
                    {
                     before(grammarAccess.getChoixAccess().getTextAssignment_0()); 
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:415:1: ( rule__Choix__TextAssignment_0 )
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:415:2: rule__Choix__TextAssignment_0
                    {
                    pushFollow(FOLLOW_rule__Choix__TextAssignment_0_in_rule__Choix__Alternatives830);
                    rule__Choix__TextAssignment_0();

                    state._fsp--;


                    }

                     after(grammarAccess.getChoixAccess().getTextAssignment_0()); 

                    }


                    }
                    break;
                case 2 :
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:419:6: ( ruleImage )
                    {
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:419:6: ( ruleImage )
                    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:420:1: ruleImage
                    {
                     before(grammarAccess.getChoixAccess().getImageParserRuleCall_1()); 
                    pushFollow(FOLLOW_ruleImage_in_rule__Choix__Alternatives848);
                    ruleImage();

                    state._fsp--;

                     after(grammarAccess.getChoixAccess().getImageParserRuleCall_1()); 

                    }


                    }
                    break;

            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Choix__Alternatives"


    // $ANTLR start "rule__Formulaire__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:432:1: rule__Formulaire__Group__0 : rule__Formulaire__Group__0__Impl rule__Formulaire__Group__1 ;
    public final void rule__Formulaire__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:436:1: ( rule__Formulaire__Group__0__Impl rule__Formulaire__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:437:2: rule__Formulaire__Group__0__Impl rule__Formulaire__Group__1
            {
            pushFollow(FOLLOW_rule__Formulaire__Group__0__Impl_in_rule__Formulaire__Group__0878);
            rule__Formulaire__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Formulaire__Group__1_in_rule__Formulaire__Group__0881);
            rule__Formulaire__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formulaire__Group__0"


    // $ANTLR start "rule__Formulaire__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:444:1: rule__Formulaire__Group__0__Impl : ( 'Formulaire' ) ;
    public final void rule__Formulaire__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:448:1: ( ( 'Formulaire' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:449:1: ( 'Formulaire' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:449:1: ( 'Formulaire' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:450:1: 'Formulaire'
            {
             before(grammarAccess.getFormulaireAccess().getFormulaireKeyword_0()); 
            match(input,11,FOLLOW_11_in_rule__Formulaire__Group__0__Impl909); 
             after(grammarAccess.getFormulaireAccess().getFormulaireKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formulaire__Group__0__Impl"


    // $ANTLR start "rule__Formulaire__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:463:1: rule__Formulaire__Group__1 : rule__Formulaire__Group__1__Impl rule__Formulaire__Group__2 ;
    public final void rule__Formulaire__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:467:1: ( rule__Formulaire__Group__1__Impl rule__Formulaire__Group__2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:468:2: rule__Formulaire__Group__1__Impl rule__Formulaire__Group__2
            {
            pushFollow(FOLLOW_rule__Formulaire__Group__1__Impl_in_rule__Formulaire__Group__1940);
            rule__Formulaire__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Formulaire__Group__2_in_rule__Formulaire__Group__1943);
            rule__Formulaire__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formulaire__Group__1"


    // $ANTLR start "rule__Formulaire__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:475:1: rule__Formulaire__Group__1__Impl : ( ( rule__Formulaire__IdAssignment_1 ) ) ;
    public final void rule__Formulaire__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:479:1: ( ( ( rule__Formulaire__IdAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:480:1: ( ( rule__Formulaire__IdAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:480:1: ( ( rule__Formulaire__IdAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:481:1: ( rule__Formulaire__IdAssignment_1 )
            {
             before(grammarAccess.getFormulaireAccess().getIdAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:482:1: ( rule__Formulaire__IdAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:482:2: rule__Formulaire__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__Formulaire__IdAssignment_1_in_rule__Formulaire__Group__1__Impl970);
            rule__Formulaire__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getFormulaireAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formulaire__Group__1__Impl"


    // $ANTLR start "rule__Formulaire__Group__2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:492:1: rule__Formulaire__Group__2 : rule__Formulaire__Group__2__Impl ;
    public final void rule__Formulaire__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:496:1: ( rule__Formulaire__Group__2__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:497:2: rule__Formulaire__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__Formulaire__Group__2__Impl_in_rule__Formulaire__Group__21000);
            rule__Formulaire__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formulaire__Group__2"


    // $ANTLR start "rule__Formulaire__Group__2__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:503:1: rule__Formulaire__Group__2__Impl : ( ( rule__Formulaire__QuestionsAssignment_2 )* ) ;
    public final void rule__Formulaire__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:507:1: ( ( ( rule__Formulaire__QuestionsAssignment_2 )* ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:508:1: ( ( rule__Formulaire__QuestionsAssignment_2 )* )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:508:1: ( ( rule__Formulaire__QuestionsAssignment_2 )* )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:509:1: ( rule__Formulaire__QuestionsAssignment_2 )*
            {
             before(grammarAccess.getFormulaireAccess().getQuestionsAssignment_2()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:510:1: ( rule__Formulaire__QuestionsAssignment_2 )*
            loop3:
            do {
                int alt3=2;
                int LA3_0 = input.LA(1);

                if ( (LA3_0==12) ) {
                    alt3=1;
                }


                switch (alt3) {
            	case 1 :
            	    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:510:2: rule__Formulaire__QuestionsAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__Formulaire__QuestionsAssignment_2_in_rule__Formulaire__Group__2__Impl1027);
            	    rule__Formulaire__QuestionsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop3;
                }
            } while (true);

             after(grammarAccess.getFormulaireAccess().getQuestionsAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formulaire__Group__2__Impl"


    // $ANTLR start "rule__Question__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:526:1: rule__Question__Group__0 : rule__Question__Group__0__Impl rule__Question__Group__1 ;
    public final void rule__Question__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:530:1: ( rule__Question__Group__0__Impl rule__Question__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:531:2: rule__Question__Group__0__Impl rule__Question__Group__1
            {
            pushFollow(FOLLOW_rule__Question__Group__0__Impl_in_rule__Question__Group__01064);
            rule__Question__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__1_in_rule__Question__Group__01067);
            rule__Question__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__0"


    // $ANTLR start "rule__Question__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:538:1: rule__Question__Group__0__Impl : ( 'Question' ) ;
    public final void rule__Question__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:542:1: ( ( 'Question' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:543:1: ( 'Question' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:543:1: ( 'Question' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:544:1: 'Question'
            {
             before(grammarAccess.getQuestionAccess().getQuestionKeyword_0()); 
            match(input,12,FOLLOW_12_in_rule__Question__Group__0__Impl1095); 
             after(grammarAccess.getQuestionAccess().getQuestionKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__0__Impl"


    // $ANTLR start "rule__Question__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:557:1: rule__Question__Group__1 : rule__Question__Group__1__Impl rule__Question__Group__2 ;
    public final void rule__Question__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:561:1: ( rule__Question__Group__1__Impl rule__Question__Group__2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:562:2: rule__Question__Group__1__Impl rule__Question__Group__2
            {
            pushFollow(FOLLOW_rule__Question__Group__1__Impl_in_rule__Question__Group__11126);
            rule__Question__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__2_in_rule__Question__Group__11129);
            rule__Question__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__1"


    // $ANTLR start "rule__Question__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:569:1: rule__Question__Group__1__Impl : ( ( rule__Question__IdAssignment_1 ) ) ;
    public final void rule__Question__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:573:1: ( ( ( rule__Question__IdAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:574:1: ( ( rule__Question__IdAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:574:1: ( ( rule__Question__IdAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:575:1: ( rule__Question__IdAssignment_1 )
            {
             before(grammarAccess.getQuestionAccess().getIdAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:576:1: ( rule__Question__IdAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:576:2: rule__Question__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__Question__IdAssignment_1_in_rule__Question__Group__1__Impl1156);
            rule__Question__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__1__Impl"


    // $ANTLR start "rule__Question__Group__2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:586:1: rule__Question__Group__2 : rule__Question__Group__2__Impl rule__Question__Group__3 ;
    public final void rule__Question__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:590:1: ( rule__Question__Group__2__Impl rule__Question__Group__3 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:591:2: rule__Question__Group__2__Impl rule__Question__Group__3
            {
            pushFollow(FOLLOW_rule__Question__Group__2__Impl_in_rule__Question__Group__21186);
            rule__Question__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__3_in_rule__Question__Group__21189);
            rule__Question__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__2"


    // $ANTLR start "rule__Question__Group__2__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:598:1: rule__Question__Group__2__Impl : ( '{' ) ;
    public final void rule__Question__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:602:1: ( ( '{' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:603:1: ( '{' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:603:1: ( '{' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:604:1: '{'
            {
             before(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,13,FOLLOW_13_in_rule__Question__Group__2__Impl1217); 
             after(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__2__Impl"


    // $ANTLR start "rule__Question__Group__3"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:617:1: rule__Question__Group__3 : rule__Question__Group__3__Impl rule__Question__Group__4 ;
    public final void rule__Question__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:621:1: ( rule__Question__Group__3__Impl rule__Question__Group__4 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:622:2: rule__Question__Group__3__Impl rule__Question__Group__4
            {
            pushFollow(FOLLOW_rule__Question__Group__3__Impl_in_rule__Question__Group__31248);
            rule__Question__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__4_in_rule__Question__Group__31251);
            rule__Question__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__3"


    // $ANTLR start "rule__Question__Group__3__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:629:1: rule__Question__Group__3__Impl : ( 'Ennonce' ) ;
    public final void rule__Question__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:633:1: ( ( 'Ennonce' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:634:1: ( 'Ennonce' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:634:1: ( 'Ennonce' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:635:1: 'Ennonce'
            {
             before(grammarAccess.getQuestionAccess().getEnnonceKeyword_3()); 
            match(input,14,FOLLOW_14_in_rule__Question__Group__3__Impl1279); 
             after(grammarAccess.getQuestionAccess().getEnnonceKeyword_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__3__Impl"


    // $ANTLR start "rule__Question__Group__4"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:648:1: rule__Question__Group__4 : rule__Question__Group__4__Impl rule__Question__Group__5 ;
    public final void rule__Question__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:652:1: ( rule__Question__Group__4__Impl rule__Question__Group__5 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:653:2: rule__Question__Group__4__Impl rule__Question__Group__5
            {
            pushFollow(FOLLOW_rule__Question__Group__4__Impl_in_rule__Question__Group__41310);
            rule__Question__Group__4__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__5_in_rule__Question__Group__41313);
            rule__Question__Group__5();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__4"


    // $ANTLR start "rule__Question__Group__4__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:660:1: rule__Question__Group__4__Impl : ( '{' ) ;
    public final void rule__Question__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:664:1: ( ( '{' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:665:1: ( '{' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:665:1: ( '{' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:666:1: '{'
            {
             before(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_4()); 
            match(input,13,FOLLOW_13_in_rule__Question__Group__4__Impl1341); 
             after(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__4__Impl"


    // $ANTLR start "rule__Question__Group__5"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:679:1: rule__Question__Group__5 : rule__Question__Group__5__Impl rule__Question__Group__6 ;
    public final void rule__Question__Group__5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:683:1: ( rule__Question__Group__5__Impl rule__Question__Group__6 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:684:2: rule__Question__Group__5__Impl rule__Question__Group__6
            {
            pushFollow(FOLLOW_rule__Question__Group__5__Impl_in_rule__Question__Group__51372);
            rule__Question__Group__5__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__6_in_rule__Question__Group__51375);
            rule__Question__Group__6();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__5"


    // $ANTLR start "rule__Question__Group__5__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:691:1: rule__Question__Group__5__Impl : ( ( rule__Question__TextAssignment_5 ) ) ;
    public final void rule__Question__Group__5__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:695:1: ( ( ( rule__Question__TextAssignment_5 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:696:1: ( ( rule__Question__TextAssignment_5 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:696:1: ( ( rule__Question__TextAssignment_5 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:697:1: ( rule__Question__TextAssignment_5 )
            {
             before(grammarAccess.getQuestionAccess().getTextAssignment_5()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:698:1: ( rule__Question__TextAssignment_5 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:698:2: rule__Question__TextAssignment_5
            {
            pushFollow(FOLLOW_rule__Question__TextAssignment_5_in_rule__Question__Group__5__Impl1402);
            rule__Question__TextAssignment_5();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getTextAssignment_5()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__5__Impl"


    // $ANTLR start "rule__Question__Group__6"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:708:1: rule__Question__Group__6 : rule__Question__Group__6__Impl rule__Question__Group__7 ;
    public final void rule__Question__Group__6() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:712:1: ( rule__Question__Group__6__Impl rule__Question__Group__7 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:713:2: rule__Question__Group__6__Impl rule__Question__Group__7
            {
            pushFollow(FOLLOW_rule__Question__Group__6__Impl_in_rule__Question__Group__61432);
            rule__Question__Group__6__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__7_in_rule__Question__Group__61435);
            rule__Question__Group__7();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__6"


    // $ANTLR start "rule__Question__Group__6__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:720:1: rule__Question__Group__6__Impl : ( '}' ) ;
    public final void rule__Question__Group__6__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:724:1: ( ( '}' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:725:1: ( '}' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:725:1: ( '}' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:726:1: '}'
            {
             before(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_6()); 
            match(input,15,FOLLOW_15_in_rule__Question__Group__6__Impl1463); 
             after(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_6()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__6__Impl"


    // $ANTLR start "rule__Question__Group__7"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:739:1: rule__Question__Group__7 : rule__Question__Group__7__Impl rule__Question__Group__8 ;
    public final void rule__Question__Group__7() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:743:1: ( rule__Question__Group__7__Impl rule__Question__Group__8 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:744:2: rule__Question__Group__7__Impl rule__Question__Group__8
            {
            pushFollow(FOLLOW_rule__Question__Group__7__Impl_in_rule__Question__Group__71494);
            rule__Question__Group__7__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__8_in_rule__Question__Group__71497);
            rule__Question__Group__8();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__7"


    // $ANTLR start "rule__Question__Group__7__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:751:1: rule__Question__Group__7__Impl : ( 'Widget' ) ;
    public final void rule__Question__Group__7__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:755:1: ( ( 'Widget' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:756:1: ( 'Widget' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:756:1: ( 'Widget' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:757:1: 'Widget'
            {
             before(grammarAccess.getQuestionAccess().getWidgetKeyword_7()); 
            match(input,16,FOLLOW_16_in_rule__Question__Group__7__Impl1525); 
             after(grammarAccess.getQuestionAccess().getWidgetKeyword_7()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__7__Impl"


    // $ANTLR start "rule__Question__Group__8"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:770:1: rule__Question__Group__8 : rule__Question__Group__8__Impl rule__Question__Group__9 ;
    public final void rule__Question__Group__8() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:774:1: ( rule__Question__Group__8__Impl rule__Question__Group__9 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:775:2: rule__Question__Group__8__Impl rule__Question__Group__9
            {
            pushFollow(FOLLOW_rule__Question__Group__8__Impl_in_rule__Question__Group__81556);
            rule__Question__Group__8__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__9_in_rule__Question__Group__81559);
            rule__Question__Group__9();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__8"


    // $ANTLR start "rule__Question__Group__8__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:782:1: rule__Question__Group__8__Impl : ( '{' ) ;
    public final void rule__Question__Group__8__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:786:1: ( ( '{' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:787:1: ( '{' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:787:1: ( '{' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:788:1: '{'
            {
             before(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_8()); 
            match(input,13,FOLLOW_13_in_rule__Question__Group__8__Impl1587); 
             after(grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_8()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__8__Impl"


    // $ANTLR start "rule__Question__Group__9"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:801:1: rule__Question__Group__9 : rule__Question__Group__9__Impl rule__Question__Group__10 ;
    public final void rule__Question__Group__9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:805:1: ( rule__Question__Group__9__Impl rule__Question__Group__10 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:806:2: rule__Question__Group__9__Impl rule__Question__Group__10
            {
            pushFollow(FOLLOW_rule__Question__Group__9__Impl_in_rule__Question__Group__91618);
            rule__Question__Group__9__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__10_in_rule__Question__Group__91621);
            rule__Question__Group__10();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__9"


    // $ANTLR start "rule__Question__Group__9__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:813:1: rule__Question__Group__9__Impl : ( ( rule__Question__ChoixAssignment_9 ) ) ;
    public final void rule__Question__Group__9__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:817:1: ( ( ( rule__Question__ChoixAssignment_9 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:818:1: ( ( rule__Question__ChoixAssignment_9 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:818:1: ( ( rule__Question__ChoixAssignment_9 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:819:1: ( rule__Question__ChoixAssignment_9 )
            {
             before(grammarAccess.getQuestionAccess().getChoixAssignment_9()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:820:1: ( rule__Question__ChoixAssignment_9 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:820:2: rule__Question__ChoixAssignment_9
            {
            pushFollow(FOLLOW_rule__Question__ChoixAssignment_9_in_rule__Question__Group__9__Impl1648);
            rule__Question__ChoixAssignment_9();

            state._fsp--;


            }

             after(grammarAccess.getQuestionAccess().getChoixAssignment_9()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__9__Impl"


    // $ANTLR start "rule__Question__Group__10"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:830:1: rule__Question__Group__10 : rule__Question__Group__10__Impl rule__Question__Group__11 ;
    public final void rule__Question__Group__10() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:834:1: ( rule__Question__Group__10__Impl rule__Question__Group__11 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:835:2: rule__Question__Group__10__Impl rule__Question__Group__11
            {
            pushFollow(FOLLOW_rule__Question__Group__10__Impl_in_rule__Question__Group__101678);
            rule__Question__Group__10__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Question__Group__11_in_rule__Question__Group__101681);
            rule__Question__Group__11();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__10"


    // $ANTLR start "rule__Question__Group__10__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:842:1: rule__Question__Group__10__Impl : ( '}' ) ;
    public final void rule__Question__Group__10__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:846:1: ( ( '}' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:847:1: ( '}' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:847:1: ( '}' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:848:1: '}'
            {
             before(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_10()); 
            match(input,15,FOLLOW_15_in_rule__Question__Group__10__Impl1709); 
             after(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_10()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__10__Impl"


    // $ANTLR start "rule__Question__Group__11"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:861:1: rule__Question__Group__11 : rule__Question__Group__11__Impl ;
    public final void rule__Question__Group__11() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:865:1: ( rule__Question__Group__11__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:866:2: rule__Question__Group__11__Impl
            {
            pushFollow(FOLLOW_rule__Question__Group__11__Impl_in_rule__Question__Group__111740);
            rule__Question__Group__11__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__11"


    // $ANTLR start "rule__Question__Group__11__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:872:1: rule__Question__Group__11__Impl : ( '}' ) ;
    public final void rule__Question__Group__11__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:876:1: ( ( '}' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:877:1: ( '}' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:877:1: ( '}' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:878:1: '}'
            {
             before(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_11()); 
            match(input,15,FOLLOW_15_in_rule__Question__Group__11__Impl1768); 
             after(grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_11()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__Group__11__Impl"


    // $ANTLR start "rule__Upload__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:915:1: rule__Upload__Group__0 : rule__Upload__Group__0__Impl rule__Upload__Group__1 ;
    public final void rule__Upload__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:919:1: ( rule__Upload__Group__0__Impl rule__Upload__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:920:2: rule__Upload__Group__0__Impl rule__Upload__Group__1
            {
            pushFollow(FOLLOW_rule__Upload__Group__0__Impl_in_rule__Upload__Group__01823);
            rule__Upload__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Upload__Group__1_in_rule__Upload__Group__01826);
            rule__Upload__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__0"


    // $ANTLR start "rule__Upload__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:927:1: rule__Upload__Group__0__Impl : ( 'Upload' ) ;
    public final void rule__Upload__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:931:1: ( ( 'Upload' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:932:1: ( 'Upload' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:932:1: ( 'Upload' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:933:1: 'Upload'
            {
             before(grammarAccess.getUploadAccess().getUploadKeyword_0()); 
            match(input,17,FOLLOW_17_in_rule__Upload__Group__0__Impl1854); 
             after(grammarAccess.getUploadAccess().getUploadKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__0__Impl"


    // $ANTLR start "rule__Upload__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:946:1: rule__Upload__Group__1 : rule__Upload__Group__1__Impl rule__Upload__Group__2 ;
    public final void rule__Upload__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:950:1: ( rule__Upload__Group__1__Impl rule__Upload__Group__2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:951:2: rule__Upload__Group__1__Impl rule__Upload__Group__2
            {
            pushFollow(FOLLOW_rule__Upload__Group__1__Impl_in_rule__Upload__Group__11885);
            rule__Upload__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Upload__Group__2_in_rule__Upload__Group__11888);
            rule__Upload__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__1"


    // $ANTLR start "rule__Upload__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:958:1: rule__Upload__Group__1__Impl : ( ( rule__Upload__IdAssignment_1 ) ) ;
    public final void rule__Upload__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:962:1: ( ( ( rule__Upload__IdAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:963:1: ( ( rule__Upload__IdAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:963:1: ( ( rule__Upload__IdAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:964:1: ( rule__Upload__IdAssignment_1 )
            {
             before(grammarAccess.getUploadAccess().getIdAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:965:1: ( rule__Upload__IdAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:965:2: rule__Upload__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__Upload__IdAssignment_1_in_rule__Upload__Group__1__Impl1915);
            rule__Upload__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getUploadAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__1__Impl"


    // $ANTLR start "rule__Upload__Group__2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:975:1: rule__Upload__Group__2 : rule__Upload__Group__2__Impl rule__Upload__Group__3 ;
    public final void rule__Upload__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:979:1: ( rule__Upload__Group__2__Impl rule__Upload__Group__3 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:980:2: rule__Upload__Group__2__Impl rule__Upload__Group__3
            {
            pushFollow(FOLLOW_rule__Upload__Group__2__Impl_in_rule__Upload__Group__21945);
            rule__Upload__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Upload__Group__3_in_rule__Upload__Group__21948);
            rule__Upload__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__2"


    // $ANTLR start "rule__Upload__Group__2__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:987:1: rule__Upload__Group__2__Impl : ( '{' ) ;
    public final void rule__Upload__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:991:1: ( ( '{' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:992:1: ( '{' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:992:1: ( '{' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:993:1: '{'
            {
             before(grammarAccess.getUploadAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,13,FOLLOW_13_in_rule__Upload__Group__2__Impl1976); 
             after(grammarAccess.getUploadAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__2__Impl"


    // $ANTLR start "rule__Upload__Group__3"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1006:1: rule__Upload__Group__3 : rule__Upload__Group__3__Impl rule__Upload__Group__4 ;
    public final void rule__Upload__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1010:1: ( rule__Upload__Group__3__Impl rule__Upload__Group__4 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1011:2: rule__Upload__Group__3__Impl rule__Upload__Group__4
            {
            pushFollow(FOLLOW_rule__Upload__Group__3__Impl_in_rule__Upload__Group__32007);
            rule__Upload__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Upload__Group__4_in_rule__Upload__Group__32010);
            rule__Upload__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__3"


    // $ANTLR start "rule__Upload__Group__3__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1018:1: rule__Upload__Group__3__Impl : ( ( rule__Upload__ImgAssignment_3 ) ) ;
    public final void rule__Upload__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1022:1: ( ( ( rule__Upload__ImgAssignment_3 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1023:1: ( ( rule__Upload__ImgAssignment_3 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1023:1: ( ( rule__Upload__ImgAssignment_3 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1024:1: ( rule__Upload__ImgAssignment_3 )
            {
             before(grammarAccess.getUploadAccess().getImgAssignment_3()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1025:1: ( rule__Upload__ImgAssignment_3 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1025:2: rule__Upload__ImgAssignment_3
            {
            pushFollow(FOLLOW_rule__Upload__ImgAssignment_3_in_rule__Upload__Group__3__Impl2037);
            rule__Upload__ImgAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getUploadAccess().getImgAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__3__Impl"


    // $ANTLR start "rule__Upload__Group__4"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1035:1: rule__Upload__Group__4 : rule__Upload__Group__4__Impl ;
    public final void rule__Upload__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1039:1: ( rule__Upload__Group__4__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1040:2: rule__Upload__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Upload__Group__4__Impl_in_rule__Upload__Group__42067);
            rule__Upload__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__4"


    // $ANTLR start "rule__Upload__Group__4__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1046:1: rule__Upload__Group__4__Impl : ( '}' ) ;
    public final void rule__Upload__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1050:1: ( ( '}' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1051:1: ( '}' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1051:1: ( '}' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1052:1: '}'
            {
             before(grammarAccess.getUploadAccess().getRightCurlyBracketKeyword_4()); 
            match(input,15,FOLLOW_15_in_rule__Upload__Group__4__Impl2095); 
             after(grammarAccess.getUploadAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__Group__4__Impl"


    // $ANTLR start "rule__RadioButton__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1075:1: rule__RadioButton__Group__0 : rule__RadioButton__Group__0__Impl rule__RadioButton__Group__1 ;
    public final void rule__RadioButton__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1079:1: ( rule__RadioButton__Group__0__Impl rule__RadioButton__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1080:2: rule__RadioButton__Group__0__Impl rule__RadioButton__Group__1
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__0__Impl_in_rule__RadioButton__Group__02136);
            rule__RadioButton__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButton__Group__1_in_rule__RadioButton__Group__02139);
            rule__RadioButton__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__0"


    // $ANTLR start "rule__RadioButton__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1087:1: rule__RadioButton__Group__0__Impl : ( 'RadioButton' ) ;
    public final void rule__RadioButton__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1091:1: ( ( 'RadioButton' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1092:1: ( 'RadioButton' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1092:1: ( 'RadioButton' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1093:1: 'RadioButton'
            {
             before(grammarAccess.getRadioButtonAccess().getRadioButtonKeyword_0()); 
            match(input,18,FOLLOW_18_in_rule__RadioButton__Group__0__Impl2167); 
             after(grammarAccess.getRadioButtonAccess().getRadioButtonKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__0__Impl"


    // $ANTLR start "rule__RadioButton__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1106:1: rule__RadioButton__Group__1 : rule__RadioButton__Group__1__Impl rule__RadioButton__Group__2 ;
    public final void rule__RadioButton__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1110:1: ( rule__RadioButton__Group__1__Impl rule__RadioButton__Group__2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1111:2: rule__RadioButton__Group__1__Impl rule__RadioButton__Group__2
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__1__Impl_in_rule__RadioButton__Group__12198);
            rule__RadioButton__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__RadioButton__Group__2_in_rule__RadioButton__Group__12201);
            rule__RadioButton__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__1"


    // $ANTLR start "rule__RadioButton__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1118:1: rule__RadioButton__Group__1__Impl : ( ( rule__RadioButton__IdAssignment_1 ) ) ;
    public final void rule__RadioButton__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1122:1: ( ( ( rule__RadioButton__IdAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1123:1: ( ( rule__RadioButton__IdAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1123:1: ( ( rule__RadioButton__IdAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1124:1: ( rule__RadioButton__IdAssignment_1 )
            {
             before(grammarAccess.getRadioButtonAccess().getIdAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1125:1: ( rule__RadioButton__IdAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1125:2: rule__RadioButton__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__RadioButton__IdAssignment_1_in_rule__RadioButton__Group__1__Impl2228);
            rule__RadioButton__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getRadioButtonAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__1__Impl"


    // $ANTLR start "rule__RadioButton__Group__2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1135:1: rule__RadioButton__Group__2 : rule__RadioButton__Group__2__Impl ;
    public final void rule__RadioButton__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1139:1: ( rule__RadioButton__Group__2__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1140:2: rule__RadioButton__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__RadioButton__Group__2__Impl_in_rule__RadioButton__Group__22258);
            rule__RadioButton__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__2"


    // $ANTLR start "rule__RadioButton__Group__2__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1146:1: rule__RadioButton__Group__2__Impl : ( ( ( rule__RadioButton__ReponseAssignment_2 ) ) ( ( rule__RadioButton__ReponseAssignment_2 )* ) ) ;
    public final void rule__RadioButton__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1150:1: ( ( ( ( rule__RadioButton__ReponseAssignment_2 ) ) ( ( rule__RadioButton__ReponseAssignment_2 )* ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1151:1: ( ( ( rule__RadioButton__ReponseAssignment_2 ) ) ( ( rule__RadioButton__ReponseAssignment_2 )* ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1151:1: ( ( ( rule__RadioButton__ReponseAssignment_2 ) ) ( ( rule__RadioButton__ReponseAssignment_2 )* ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1152:1: ( ( rule__RadioButton__ReponseAssignment_2 ) ) ( ( rule__RadioButton__ReponseAssignment_2 )* )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1152:1: ( ( rule__RadioButton__ReponseAssignment_2 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1153:1: ( rule__RadioButton__ReponseAssignment_2 )
            {
             before(grammarAccess.getRadioButtonAccess().getReponseAssignment_2()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1154:1: ( rule__RadioButton__ReponseAssignment_2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1154:2: rule__RadioButton__ReponseAssignment_2
            {
            pushFollow(FOLLOW_rule__RadioButton__ReponseAssignment_2_in_rule__RadioButton__Group__2__Impl2287);
            rule__RadioButton__ReponseAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getRadioButtonAccess().getReponseAssignment_2()); 

            }

            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1157:1: ( ( rule__RadioButton__ReponseAssignment_2 )* )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1158:1: ( rule__RadioButton__ReponseAssignment_2 )*
            {
             before(grammarAccess.getRadioButtonAccess().getReponseAssignment_2()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1159:1: ( rule__RadioButton__ReponseAssignment_2 )*
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( (LA4_0==22) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1159:2: rule__RadioButton__ReponseAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__RadioButton__ReponseAssignment_2_in_rule__RadioButton__Group__2__Impl2299);
            	    rule__RadioButton__ReponseAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop4;
                }
            } while (true);

             after(grammarAccess.getRadioButtonAccess().getReponseAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__Group__2__Impl"


    // $ANTLR start "rule__CheckBox__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1176:1: rule__CheckBox__Group__0 : rule__CheckBox__Group__0__Impl rule__CheckBox__Group__1 ;
    public final void rule__CheckBox__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1180:1: ( rule__CheckBox__Group__0__Impl rule__CheckBox__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1181:2: rule__CheckBox__Group__0__Impl rule__CheckBox__Group__1
            {
            pushFollow(FOLLOW_rule__CheckBox__Group__0__Impl_in_rule__CheckBox__Group__02338);
            rule__CheckBox__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CheckBox__Group__1_in_rule__CheckBox__Group__02341);
            rule__CheckBox__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckBox__Group__0"


    // $ANTLR start "rule__CheckBox__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1188:1: rule__CheckBox__Group__0__Impl : ( 'CheckBox' ) ;
    public final void rule__CheckBox__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1192:1: ( ( 'CheckBox' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1193:1: ( 'CheckBox' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1193:1: ( 'CheckBox' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1194:1: 'CheckBox'
            {
             before(grammarAccess.getCheckBoxAccess().getCheckBoxKeyword_0()); 
            match(input,19,FOLLOW_19_in_rule__CheckBox__Group__0__Impl2369); 
             after(grammarAccess.getCheckBoxAccess().getCheckBoxKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckBox__Group__0__Impl"


    // $ANTLR start "rule__CheckBox__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1207:1: rule__CheckBox__Group__1 : rule__CheckBox__Group__1__Impl rule__CheckBox__Group__2 ;
    public final void rule__CheckBox__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1211:1: ( rule__CheckBox__Group__1__Impl rule__CheckBox__Group__2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1212:2: rule__CheckBox__Group__1__Impl rule__CheckBox__Group__2
            {
            pushFollow(FOLLOW_rule__CheckBox__Group__1__Impl_in_rule__CheckBox__Group__12400);
            rule__CheckBox__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__CheckBox__Group__2_in_rule__CheckBox__Group__12403);
            rule__CheckBox__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckBox__Group__1"


    // $ANTLR start "rule__CheckBox__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1219:1: rule__CheckBox__Group__1__Impl : ( ( rule__CheckBox__IdAssignment_1 ) ) ;
    public final void rule__CheckBox__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1223:1: ( ( ( rule__CheckBox__IdAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1224:1: ( ( rule__CheckBox__IdAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1224:1: ( ( rule__CheckBox__IdAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1225:1: ( rule__CheckBox__IdAssignment_1 )
            {
             before(grammarAccess.getCheckBoxAccess().getIdAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1226:1: ( rule__CheckBox__IdAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1226:2: rule__CheckBox__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__CheckBox__IdAssignment_1_in_rule__CheckBox__Group__1__Impl2430);
            rule__CheckBox__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getCheckBoxAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckBox__Group__1__Impl"


    // $ANTLR start "rule__CheckBox__Group__2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1236:1: rule__CheckBox__Group__2 : rule__CheckBox__Group__2__Impl ;
    public final void rule__CheckBox__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1240:1: ( rule__CheckBox__Group__2__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1241:2: rule__CheckBox__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__CheckBox__Group__2__Impl_in_rule__CheckBox__Group__22460);
            rule__CheckBox__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckBox__Group__2"


    // $ANTLR start "rule__CheckBox__Group__2__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1247:1: rule__CheckBox__Group__2__Impl : ( ( ( rule__CheckBox__ReponseAssignment_2 ) ) ( ( rule__CheckBox__ReponseAssignment_2 )* ) ) ;
    public final void rule__CheckBox__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1251:1: ( ( ( ( rule__CheckBox__ReponseAssignment_2 ) ) ( ( rule__CheckBox__ReponseAssignment_2 )* ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1252:1: ( ( ( rule__CheckBox__ReponseAssignment_2 ) ) ( ( rule__CheckBox__ReponseAssignment_2 )* ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1252:1: ( ( ( rule__CheckBox__ReponseAssignment_2 ) ) ( ( rule__CheckBox__ReponseAssignment_2 )* ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1253:1: ( ( rule__CheckBox__ReponseAssignment_2 ) ) ( ( rule__CheckBox__ReponseAssignment_2 )* )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1253:1: ( ( rule__CheckBox__ReponseAssignment_2 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1254:1: ( rule__CheckBox__ReponseAssignment_2 )
            {
             before(grammarAccess.getCheckBoxAccess().getReponseAssignment_2()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1255:1: ( rule__CheckBox__ReponseAssignment_2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1255:2: rule__CheckBox__ReponseAssignment_2
            {
            pushFollow(FOLLOW_rule__CheckBox__ReponseAssignment_2_in_rule__CheckBox__Group__2__Impl2489);
            rule__CheckBox__ReponseAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getCheckBoxAccess().getReponseAssignment_2()); 

            }

            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1258:1: ( ( rule__CheckBox__ReponseAssignment_2 )* )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1259:1: ( rule__CheckBox__ReponseAssignment_2 )*
            {
             before(grammarAccess.getCheckBoxAccess().getReponseAssignment_2()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1260:1: ( rule__CheckBox__ReponseAssignment_2 )*
            loop5:
            do {
                int alt5=2;
                int LA5_0 = input.LA(1);

                if ( (LA5_0==22) ) {
                    alt5=1;
                }


                switch (alt5) {
            	case 1 :
            	    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1260:2: rule__CheckBox__ReponseAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__CheckBox__ReponseAssignment_2_in_rule__CheckBox__Group__2__Impl2501);
            	    rule__CheckBox__ReponseAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop5;
                }
            } while (true);

             after(grammarAccess.getCheckBoxAccess().getReponseAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckBox__Group__2__Impl"


    // $ANTLR start "rule__TextBox__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1277:1: rule__TextBox__Group__0 : rule__TextBox__Group__0__Impl rule__TextBox__Group__1 ;
    public final void rule__TextBox__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1281:1: ( rule__TextBox__Group__0__Impl rule__TextBox__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1282:2: rule__TextBox__Group__0__Impl rule__TextBox__Group__1
            {
            pushFollow(FOLLOW_rule__TextBox__Group__0__Impl_in_rule__TextBox__Group__02540);
            rule__TextBox__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TextBox__Group__1_in_rule__TextBox__Group__02543);
            rule__TextBox__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TextBox__Group__0"


    // $ANTLR start "rule__TextBox__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1289:1: rule__TextBox__Group__0__Impl : ( 'TextBox' ) ;
    public final void rule__TextBox__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1293:1: ( ( 'TextBox' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1294:1: ( 'TextBox' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1294:1: ( 'TextBox' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1295:1: 'TextBox'
            {
             before(grammarAccess.getTextBoxAccess().getTextBoxKeyword_0()); 
            match(input,20,FOLLOW_20_in_rule__TextBox__Group__0__Impl2571); 
             after(grammarAccess.getTextBoxAccess().getTextBoxKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TextBox__Group__0__Impl"


    // $ANTLR start "rule__TextBox__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1308:1: rule__TextBox__Group__1 : rule__TextBox__Group__1__Impl rule__TextBox__Group__2 ;
    public final void rule__TextBox__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1312:1: ( rule__TextBox__Group__1__Impl rule__TextBox__Group__2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1313:2: rule__TextBox__Group__1__Impl rule__TextBox__Group__2
            {
            pushFollow(FOLLOW_rule__TextBox__Group__1__Impl_in_rule__TextBox__Group__12602);
            rule__TextBox__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__TextBox__Group__2_in_rule__TextBox__Group__12605);
            rule__TextBox__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TextBox__Group__1"


    // $ANTLR start "rule__TextBox__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1320:1: rule__TextBox__Group__1__Impl : ( ( rule__TextBox__IdAssignment_1 ) ) ;
    public final void rule__TextBox__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1324:1: ( ( ( rule__TextBox__IdAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1325:1: ( ( rule__TextBox__IdAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1325:1: ( ( rule__TextBox__IdAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1326:1: ( rule__TextBox__IdAssignment_1 )
            {
             before(grammarAccess.getTextBoxAccess().getIdAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1327:1: ( rule__TextBox__IdAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1327:2: rule__TextBox__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__TextBox__IdAssignment_1_in_rule__TextBox__Group__1__Impl2632);
            rule__TextBox__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getTextBoxAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TextBox__Group__1__Impl"


    // $ANTLR start "rule__TextBox__Group__2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1337:1: rule__TextBox__Group__2 : rule__TextBox__Group__2__Impl ;
    public final void rule__TextBox__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1341:1: ( rule__TextBox__Group__2__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1342:2: rule__TextBox__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__TextBox__Group__2__Impl_in_rule__TextBox__Group__22662);
            rule__TextBox__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TextBox__Group__2"


    // $ANTLR start "rule__TextBox__Group__2__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1348:1: rule__TextBox__Group__2__Impl : ( ( rule__TextBox__TextAssignment_2 ) ) ;
    public final void rule__TextBox__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1352:1: ( ( ( rule__TextBox__TextAssignment_2 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1353:1: ( ( rule__TextBox__TextAssignment_2 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1353:1: ( ( rule__TextBox__TextAssignment_2 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1354:1: ( rule__TextBox__TextAssignment_2 )
            {
             before(grammarAccess.getTextBoxAccess().getTextAssignment_2()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1355:1: ( rule__TextBox__TextAssignment_2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1355:2: rule__TextBox__TextAssignment_2
            {
            pushFollow(FOLLOW_rule__TextBox__TextAssignment_2_in_rule__TextBox__Group__2__Impl2689);
            rule__TextBox__TextAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getTextBoxAccess().getTextAssignment_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TextBox__Group__2__Impl"


    // $ANTLR start "rule__List__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1371:1: rule__List__Group__0 : rule__List__Group__0__Impl rule__List__Group__1 ;
    public final void rule__List__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1375:1: ( rule__List__Group__0__Impl rule__List__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1376:2: rule__List__Group__0__Impl rule__List__Group__1
            {
            pushFollow(FOLLOW_rule__List__Group__0__Impl_in_rule__List__Group__02725);
            rule__List__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__List__Group__1_in_rule__List__Group__02728);
            rule__List__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__0"


    // $ANTLR start "rule__List__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1383:1: rule__List__Group__0__Impl : ( 'List' ) ;
    public final void rule__List__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1387:1: ( ( 'List' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1388:1: ( 'List' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1388:1: ( 'List' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1389:1: 'List'
            {
             before(grammarAccess.getListAccess().getListKeyword_0()); 
            match(input,21,FOLLOW_21_in_rule__List__Group__0__Impl2756); 
             after(grammarAccess.getListAccess().getListKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__0__Impl"


    // $ANTLR start "rule__List__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1402:1: rule__List__Group__1 : rule__List__Group__1__Impl rule__List__Group__2 ;
    public final void rule__List__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1406:1: ( rule__List__Group__1__Impl rule__List__Group__2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1407:2: rule__List__Group__1__Impl rule__List__Group__2
            {
            pushFollow(FOLLOW_rule__List__Group__1__Impl_in_rule__List__Group__12787);
            rule__List__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__List__Group__2_in_rule__List__Group__12790);
            rule__List__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__1"


    // $ANTLR start "rule__List__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1414:1: rule__List__Group__1__Impl : ( ( rule__List__IdAssignment_1 ) ) ;
    public final void rule__List__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1418:1: ( ( ( rule__List__IdAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1419:1: ( ( rule__List__IdAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1419:1: ( ( rule__List__IdAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1420:1: ( rule__List__IdAssignment_1 )
            {
             before(grammarAccess.getListAccess().getIdAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1421:1: ( rule__List__IdAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1421:2: rule__List__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__List__IdAssignment_1_in_rule__List__Group__1__Impl2817);
            rule__List__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getListAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__1__Impl"


    // $ANTLR start "rule__List__Group__2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1431:1: rule__List__Group__2 : rule__List__Group__2__Impl ;
    public final void rule__List__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1435:1: ( rule__List__Group__2__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1436:2: rule__List__Group__2__Impl
            {
            pushFollow(FOLLOW_rule__List__Group__2__Impl_in_rule__List__Group__22847);
            rule__List__Group__2__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__2"


    // $ANTLR start "rule__List__Group__2__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1442:1: rule__List__Group__2__Impl : ( ( ( rule__List__ElementsAssignment_2 ) ) ( ( rule__List__ElementsAssignment_2 )* ) ) ;
    public final void rule__List__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1446:1: ( ( ( ( rule__List__ElementsAssignment_2 ) ) ( ( rule__List__ElementsAssignment_2 )* ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1447:1: ( ( ( rule__List__ElementsAssignment_2 ) ) ( ( rule__List__ElementsAssignment_2 )* ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1447:1: ( ( ( rule__List__ElementsAssignment_2 ) ) ( ( rule__List__ElementsAssignment_2 )* ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1448:1: ( ( rule__List__ElementsAssignment_2 ) ) ( ( rule__List__ElementsAssignment_2 )* )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1448:1: ( ( rule__List__ElementsAssignment_2 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1449:1: ( rule__List__ElementsAssignment_2 )
            {
             before(grammarAccess.getListAccess().getElementsAssignment_2()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1450:1: ( rule__List__ElementsAssignment_2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1450:2: rule__List__ElementsAssignment_2
            {
            pushFollow(FOLLOW_rule__List__ElementsAssignment_2_in_rule__List__Group__2__Impl2876);
            rule__List__ElementsAssignment_2();

            state._fsp--;


            }

             after(grammarAccess.getListAccess().getElementsAssignment_2()); 

            }

            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1453:1: ( ( rule__List__ElementsAssignment_2 )* )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1454:1: ( rule__List__ElementsAssignment_2 )*
            {
             before(grammarAccess.getListAccess().getElementsAssignment_2()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1455:1: ( rule__List__ElementsAssignment_2 )*
            loop6:
            do {
                int alt6=2;
                int LA6_0 = input.LA(1);

                if ( (LA6_0==22) ) {
                    alt6=1;
                }


                switch (alt6) {
            	case 1 :
            	    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1455:2: rule__List__ElementsAssignment_2
            	    {
            	    pushFollow(FOLLOW_rule__List__ElementsAssignment_2_in_rule__List__Group__2__Impl2888);
            	    rule__List__ElementsAssignment_2();

            	    state._fsp--;


            	    }
            	    break;

            	default :
            	    break loop6;
                }
            } while (true);

             after(grammarAccess.getListAccess().getElementsAssignment_2()); 

            }


            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__Group__2__Impl"


    // $ANTLR start "rule__Contenu__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1472:1: rule__Contenu__Group__0 : rule__Contenu__Group__0__Impl rule__Contenu__Group__1 ;
    public final void rule__Contenu__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1476:1: ( rule__Contenu__Group__0__Impl rule__Contenu__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1477:2: rule__Contenu__Group__0__Impl rule__Contenu__Group__1
            {
            pushFollow(FOLLOW_rule__Contenu__Group__0__Impl_in_rule__Contenu__Group__02927);
            rule__Contenu__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Contenu__Group__1_in_rule__Contenu__Group__02930);
            rule__Contenu__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__0"


    // $ANTLR start "rule__Contenu__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1484:1: rule__Contenu__Group__0__Impl : ( 'Contenu' ) ;
    public final void rule__Contenu__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1488:1: ( ( 'Contenu' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1489:1: ( 'Contenu' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1489:1: ( 'Contenu' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1490:1: 'Contenu'
            {
             before(grammarAccess.getContenuAccess().getContenuKeyword_0()); 
            match(input,22,FOLLOW_22_in_rule__Contenu__Group__0__Impl2958); 
             after(grammarAccess.getContenuAccess().getContenuKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__0__Impl"


    // $ANTLR start "rule__Contenu__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1503:1: rule__Contenu__Group__1 : rule__Contenu__Group__1__Impl rule__Contenu__Group__2 ;
    public final void rule__Contenu__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1507:1: ( rule__Contenu__Group__1__Impl rule__Contenu__Group__2 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1508:2: rule__Contenu__Group__1__Impl rule__Contenu__Group__2
            {
            pushFollow(FOLLOW_rule__Contenu__Group__1__Impl_in_rule__Contenu__Group__12989);
            rule__Contenu__Group__1__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Contenu__Group__2_in_rule__Contenu__Group__12992);
            rule__Contenu__Group__2();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__1"


    // $ANTLR start "rule__Contenu__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1515:1: rule__Contenu__Group__1__Impl : ( ( rule__Contenu__IdAssignment_1 ) ) ;
    public final void rule__Contenu__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1519:1: ( ( ( rule__Contenu__IdAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1520:1: ( ( rule__Contenu__IdAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1520:1: ( ( rule__Contenu__IdAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1521:1: ( rule__Contenu__IdAssignment_1 )
            {
             before(grammarAccess.getContenuAccess().getIdAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1522:1: ( rule__Contenu__IdAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1522:2: rule__Contenu__IdAssignment_1
            {
            pushFollow(FOLLOW_rule__Contenu__IdAssignment_1_in_rule__Contenu__Group__1__Impl3019);
            rule__Contenu__IdAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getContenuAccess().getIdAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__1__Impl"


    // $ANTLR start "rule__Contenu__Group__2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1532:1: rule__Contenu__Group__2 : rule__Contenu__Group__2__Impl rule__Contenu__Group__3 ;
    public final void rule__Contenu__Group__2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1536:1: ( rule__Contenu__Group__2__Impl rule__Contenu__Group__3 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1537:2: rule__Contenu__Group__2__Impl rule__Contenu__Group__3
            {
            pushFollow(FOLLOW_rule__Contenu__Group__2__Impl_in_rule__Contenu__Group__23049);
            rule__Contenu__Group__2__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Contenu__Group__3_in_rule__Contenu__Group__23052);
            rule__Contenu__Group__3();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__2"


    // $ANTLR start "rule__Contenu__Group__2__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1544:1: rule__Contenu__Group__2__Impl : ( '{' ) ;
    public final void rule__Contenu__Group__2__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1548:1: ( ( '{' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1549:1: ( '{' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1549:1: ( '{' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1550:1: '{'
            {
             before(grammarAccess.getContenuAccess().getLeftCurlyBracketKeyword_2()); 
            match(input,13,FOLLOW_13_in_rule__Contenu__Group__2__Impl3080); 
             after(grammarAccess.getContenuAccess().getLeftCurlyBracketKeyword_2()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__2__Impl"


    // $ANTLR start "rule__Contenu__Group__3"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1563:1: rule__Contenu__Group__3 : rule__Contenu__Group__3__Impl rule__Contenu__Group__4 ;
    public final void rule__Contenu__Group__3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1567:1: ( rule__Contenu__Group__3__Impl rule__Contenu__Group__4 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1568:2: rule__Contenu__Group__3__Impl rule__Contenu__Group__4
            {
            pushFollow(FOLLOW_rule__Contenu__Group__3__Impl_in_rule__Contenu__Group__33111);
            rule__Contenu__Group__3__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Contenu__Group__4_in_rule__Contenu__Group__33114);
            rule__Contenu__Group__4();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__3"


    // $ANTLR start "rule__Contenu__Group__3__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1575:1: rule__Contenu__Group__3__Impl : ( ( rule__Contenu__ChoixAssignment_3 ) ) ;
    public final void rule__Contenu__Group__3__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1579:1: ( ( ( rule__Contenu__ChoixAssignment_3 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1580:1: ( ( rule__Contenu__ChoixAssignment_3 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1580:1: ( ( rule__Contenu__ChoixAssignment_3 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1581:1: ( rule__Contenu__ChoixAssignment_3 )
            {
             before(grammarAccess.getContenuAccess().getChoixAssignment_3()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1582:1: ( rule__Contenu__ChoixAssignment_3 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1582:2: rule__Contenu__ChoixAssignment_3
            {
            pushFollow(FOLLOW_rule__Contenu__ChoixAssignment_3_in_rule__Contenu__Group__3__Impl3141);
            rule__Contenu__ChoixAssignment_3();

            state._fsp--;


            }

             after(grammarAccess.getContenuAccess().getChoixAssignment_3()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__3__Impl"


    // $ANTLR start "rule__Contenu__Group__4"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1592:1: rule__Contenu__Group__4 : rule__Contenu__Group__4__Impl ;
    public final void rule__Contenu__Group__4() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1596:1: ( rule__Contenu__Group__4__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1597:2: rule__Contenu__Group__4__Impl
            {
            pushFollow(FOLLOW_rule__Contenu__Group__4__Impl_in_rule__Contenu__Group__43171);
            rule__Contenu__Group__4__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__4"


    // $ANTLR start "rule__Contenu__Group__4__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1603:1: rule__Contenu__Group__4__Impl : ( '}' ) ;
    public final void rule__Contenu__Group__4__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1607:1: ( ( '}' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1608:1: ( '}' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1608:1: ( '}' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1609:1: '}'
            {
             before(grammarAccess.getContenuAccess().getRightCurlyBracketKeyword_4()); 
            match(input,15,FOLLOW_15_in_rule__Contenu__Group__4__Impl3199); 
             after(grammarAccess.getContenuAccess().getRightCurlyBracketKeyword_4()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__Group__4__Impl"


    // $ANTLR start "rule__Image__Group__0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1632:1: rule__Image__Group__0 : rule__Image__Group__0__Impl rule__Image__Group__1 ;
    public final void rule__Image__Group__0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1636:1: ( rule__Image__Group__0__Impl rule__Image__Group__1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1637:2: rule__Image__Group__0__Impl rule__Image__Group__1
            {
            pushFollow(FOLLOW_rule__Image__Group__0__Impl_in_rule__Image__Group__03240);
            rule__Image__Group__0__Impl();

            state._fsp--;

            pushFollow(FOLLOW_rule__Image__Group__1_in_rule__Image__Group__03243);
            rule__Image__Group__1();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__0"


    // $ANTLR start "rule__Image__Group__0__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1644:1: rule__Image__Group__0__Impl : ( 'img' ) ;
    public final void rule__Image__Group__0__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1648:1: ( ( 'img' ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1649:1: ( 'img' )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1649:1: ( 'img' )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1650:1: 'img'
            {
             before(grammarAccess.getImageAccess().getImgKeyword_0()); 
            match(input,23,FOLLOW_23_in_rule__Image__Group__0__Impl3271); 
             after(grammarAccess.getImageAccess().getImgKeyword_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__0__Impl"


    // $ANTLR start "rule__Image__Group__1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1663:1: rule__Image__Group__1 : rule__Image__Group__1__Impl ;
    public final void rule__Image__Group__1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1667:1: ( rule__Image__Group__1__Impl )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1668:2: rule__Image__Group__1__Impl
            {
            pushFollow(FOLLOW_rule__Image__Group__1__Impl_in_rule__Image__Group__13302);
            rule__Image__Group__1__Impl();

            state._fsp--;


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__1"


    // $ANTLR start "rule__Image__Group__1__Impl"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1674:1: rule__Image__Group__1__Impl : ( ( rule__Image__ImgAssignment_1 ) ) ;
    public final void rule__Image__Group__1__Impl() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1678:1: ( ( ( rule__Image__ImgAssignment_1 ) ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1679:1: ( ( rule__Image__ImgAssignment_1 ) )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1679:1: ( ( rule__Image__ImgAssignment_1 ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1680:1: ( rule__Image__ImgAssignment_1 )
            {
             before(grammarAccess.getImageAccess().getImgAssignment_1()); 
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1681:1: ( rule__Image__ImgAssignment_1 )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1681:2: rule__Image__ImgAssignment_1
            {
            pushFollow(FOLLOW_rule__Image__ImgAssignment_1_in_rule__Image__Group__1__Impl3329);
            rule__Image__ImgAssignment_1();

            state._fsp--;


            }

             after(grammarAccess.getImageAccess().getImgAssignment_1()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__Group__1__Impl"


    // $ANTLR start "rule__Formulaire__IdAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1696:1: rule__Formulaire__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Formulaire__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1700:1: ( ( RULE_ID ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1701:1: ( RULE_ID )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1701:1: ( RULE_ID )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1702:1: RULE_ID
            {
             before(grammarAccess.getFormulaireAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Formulaire__IdAssignment_13368); 
             after(grammarAccess.getFormulaireAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formulaire__IdAssignment_1"


    // $ANTLR start "rule__Formulaire__QuestionsAssignment_2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1711:1: rule__Formulaire__QuestionsAssignment_2 : ( ruleQuestion ) ;
    public final void rule__Formulaire__QuestionsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1715:1: ( ( ruleQuestion ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1716:1: ( ruleQuestion )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1716:1: ( ruleQuestion )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1717:1: ruleQuestion
            {
             before(grammarAccess.getFormulaireAccess().getQuestionsQuestionParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleQuestion_in_rule__Formulaire__QuestionsAssignment_23399);
            ruleQuestion();

            state._fsp--;

             after(grammarAccess.getFormulaireAccess().getQuestionsQuestionParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Formulaire__QuestionsAssignment_2"


    // $ANTLR start "rule__Question__IdAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1726:1: rule__Question__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Question__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1730:1: ( ( RULE_ID ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1731:1: ( RULE_ID )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1731:1: ( RULE_ID )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1732:1: RULE_ID
            {
             before(grammarAccess.getQuestionAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Question__IdAssignment_13430); 
             after(grammarAccess.getQuestionAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__IdAssignment_1"


    // $ANTLR start "rule__Question__TextAssignment_5"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1741:1: rule__Question__TextAssignment_5 : ( RULE_STRING ) ;
    public final void rule__Question__TextAssignment_5() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1745:1: ( ( RULE_STRING ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1746:1: ( RULE_STRING )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1746:1: ( RULE_STRING )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1747:1: RULE_STRING
            {
             before(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_5_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Question__TextAssignment_53461); 
             after(grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_5_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__TextAssignment_5"


    // $ANTLR start "rule__Question__ChoixAssignment_9"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1756:1: rule__Question__ChoixAssignment_9 : ( ruleWidget ) ;
    public final void rule__Question__ChoixAssignment_9() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1760:1: ( ( ruleWidget ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1761:1: ( ruleWidget )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1761:1: ( ruleWidget )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1762:1: ruleWidget
            {
             before(grammarAccess.getQuestionAccess().getChoixWidgetParserRuleCall_9_0()); 
            pushFollow(FOLLOW_ruleWidget_in_rule__Question__ChoixAssignment_93492);
            ruleWidget();

            state._fsp--;

             after(grammarAccess.getQuestionAccess().getChoixWidgetParserRuleCall_9_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Question__ChoixAssignment_9"


    // $ANTLR start "rule__Upload__IdAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1771:1: rule__Upload__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Upload__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1775:1: ( ( RULE_ID ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1776:1: ( RULE_ID )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1776:1: ( RULE_ID )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1777:1: RULE_ID
            {
             before(grammarAccess.getUploadAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Upload__IdAssignment_13523); 
             after(grammarAccess.getUploadAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__IdAssignment_1"


    // $ANTLR start "rule__Upload__ImgAssignment_3"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1786:1: rule__Upload__ImgAssignment_3 : ( ruleImage ) ;
    public final void rule__Upload__ImgAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1790:1: ( ( ruleImage ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1791:1: ( ruleImage )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1791:1: ( ruleImage )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1792:1: ruleImage
            {
             before(grammarAccess.getUploadAccess().getImgImageParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleImage_in_rule__Upload__ImgAssignment_33554);
            ruleImage();

            state._fsp--;

             after(grammarAccess.getUploadAccess().getImgImageParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Upload__ImgAssignment_3"


    // $ANTLR start "rule__RadioButton__IdAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1801:1: rule__RadioButton__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__RadioButton__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1805:1: ( ( RULE_ID ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1806:1: ( RULE_ID )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1806:1: ( RULE_ID )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1807:1: RULE_ID
            {
             before(grammarAccess.getRadioButtonAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__RadioButton__IdAssignment_13585); 
             after(grammarAccess.getRadioButtonAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__IdAssignment_1"


    // $ANTLR start "rule__RadioButton__ReponseAssignment_2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1816:1: rule__RadioButton__ReponseAssignment_2 : ( ruleContenu ) ;
    public final void rule__RadioButton__ReponseAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1820:1: ( ( ruleContenu ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1821:1: ( ruleContenu )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1821:1: ( ruleContenu )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1822:1: ruleContenu
            {
             before(grammarAccess.getRadioButtonAccess().getReponseContenuParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleContenu_in_rule__RadioButton__ReponseAssignment_23616);
            ruleContenu();

            state._fsp--;

             after(grammarAccess.getRadioButtonAccess().getReponseContenuParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__RadioButton__ReponseAssignment_2"


    // $ANTLR start "rule__CheckBox__IdAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1831:1: rule__CheckBox__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__CheckBox__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1835:1: ( ( RULE_ID ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1836:1: ( RULE_ID )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1836:1: ( RULE_ID )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1837:1: RULE_ID
            {
             before(grammarAccess.getCheckBoxAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__CheckBox__IdAssignment_13647); 
             after(grammarAccess.getCheckBoxAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckBox__IdAssignment_1"


    // $ANTLR start "rule__CheckBox__ReponseAssignment_2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1846:1: rule__CheckBox__ReponseAssignment_2 : ( ruleContenu ) ;
    public final void rule__CheckBox__ReponseAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1850:1: ( ( ruleContenu ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1851:1: ( ruleContenu )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1851:1: ( ruleContenu )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1852:1: ruleContenu
            {
             before(grammarAccess.getCheckBoxAccess().getReponseContenuParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleContenu_in_rule__CheckBox__ReponseAssignment_23678);
            ruleContenu();

            state._fsp--;

             after(grammarAccess.getCheckBoxAccess().getReponseContenuParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__CheckBox__ReponseAssignment_2"


    // $ANTLR start "rule__TextBox__IdAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1861:1: rule__TextBox__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__TextBox__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1865:1: ( ( RULE_ID ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1866:1: ( RULE_ID )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1866:1: ( RULE_ID )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1867:1: RULE_ID
            {
             before(grammarAccess.getTextBoxAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__TextBox__IdAssignment_13709); 
             after(grammarAccess.getTextBoxAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TextBox__IdAssignment_1"


    // $ANTLR start "rule__TextBox__TextAssignment_2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1876:1: rule__TextBox__TextAssignment_2 : ( RULE_STRING ) ;
    public final void rule__TextBox__TextAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1880:1: ( ( RULE_STRING ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1881:1: ( RULE_STRING )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1881:1: ( RULE_STRING )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1882:1: RULE_STRING
            {
             before(grammarAccess.getTextBoxAccess().getTextSTRINGTerminalRuleCall_2_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__TextBox__TextAssignment_23740); 
             after(grammarAccess.getTextBoxAccess().getTextSTRINGTerminalRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__TextBox__TextAssignment_2"


    // $ANTLR start "rule__List__IdAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1891:1: rule__List__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__List__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1895:1: ( ( RULE_ID ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1896:1: ( RULE_ID )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1896:1: ( RULE_ID )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1897:1: RULE_ID
            {
             before(grammarAccess.getListAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__List__IdAssignment_13771); 
             after(grammarAccess.getListAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__IdAssignment_1"


    // $ANTLR start "rule__List__ElementsAssignment_2"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1906:1: rule__List__ElementsAssignment_2 : ( ruleContenu ) ;
    public final void rule__List__ElementsAssignment_2() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1910:1: ( ( ruleContenu ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1911:1: ( ruleContenu )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1911:1: ( ruleContenu )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1912:1: ruleContenu
            {
             before(grammarAccess.getListAccess().getElementsContenuParserRuleCall_2_0()); 
            pushFollow(FOLLOW_ruleContenu_in_rule__List__ElementsAssignment_23802);
            ruleContenu();

            state._fsp--;

             after(grammarAccess.getListAccess().getElementsContenuParserRuleCall_2_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__List__ElementsAssignment_2"


    // $ANTLR start "rule__Contenu__IdAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1921:1: rule__Contenu__IdAssignment_1 : ( RULE_ID ) ;
    public final void rule__Contenu__IdAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1925:1: ( ( RULE_ID ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1926:1: ( RULE_ID )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1926:1: ( RULE_ID )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1927:1: RULE_ID
            {
             before(grammarAccess.getContenuAccess().getIdIDTerminalRuleCall_1_0()); 
            match(input,RULE_ID,FOLLOW_RULE_ID_in_rule__Contenu__IdAssignment_13833); 
             after(grammarAccess.getContenuAccess().getIdIDTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__IdAssignment_1"


    // $ANTLR start "rule__Contenu__ChoixAssignment_3"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1936:1: rule__Contenu__ChoixAssignment_3 : ( ruleChoix ) ;
    public final void rule__Contenu__ChoixAssignment_3() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1940:1: ( ( ruleChoix ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1941:1: ( ruleChoix )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1941:1: ( ruleChoix )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1942:1: ruleChoix
            {
             before(grammarAccess.getContenuAccess().getChoixChoixParserRuleCall_3_0()); 
            pushFollow(FOLLOW_ruleChoix_in_rule__Contenu__ChoixAssignment_33864);
            ruleChoix();

            state._fsp--;

             after(grammarAccess.getContenuAccess().getChoixChoixParserRuleCall_3_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Contenu__ChoixAssignment_3"


    // $ANTLR start "rule__Image__ImgAssignment_1"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1951:1: rule__Image__ImgAssignment_1 : ( RULE_STRING ) ;
    public final void rule__Image__ImgAssignment_1() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1955:1: ( ( RULE_STRING ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1956:1: ( RULE_STRING )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1956:1: ( RULE_STRING )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1957:1: RULE_STRING
            {
             before(grammarAccess.getImageAccess().getImgSTRINGTerminalRuleCall_1_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Image__ImgAssignment_13895); 
             after(grammarAccess.getImageAccess().getImgSTRINGTerminalRuleCall_1_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Image__ImgAssignment_1"


    // $ANTLR start "rule__Choix__TextAssignment_0"
    // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1966:1: rule__Choix__TextAssignment_0 : ( RULE_STRING ) ;
    public final void rule__Choix__TextAssignment_0() throws RecognitionException {

        		int stackSize = keepStackSize();
            
        try {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1970:1: ( ( RULE_STRING ) )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1971:1: ( RULE_STRING )
            {
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1971:1: ( RULE_STRING )
            // ../org.xtext.example.mydslform.ui/src-gen/org/xtext/example/mydsl/ui/contentassist/antlr/internal/InternalMyDslform.g:1972:1: RULE_STRING
            {
             before(grammarAccess.getChoixAccess().getTextSTRINGTerminalRuleCall_0_0()); 
            match(input,RULE_STRING,FOLLOW_RULE_STRING_in_rule__Choix__TextAssignment_03926); 
             after(grammarAccess.getChoixAccess().getTextSTRINGTerminalRuleCall_0_0()); 

            }


            }

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
        }
        finally {

            	restoreStackSize(stackSize);

        }
        return ;
    }
    // $ANTLR end "rule__Choix__TextAssignment_0"

    // Delegated rules


 

    public static final BitSet FOLLOW_ruleFormulaire_in_entryRuleFormulaire61 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleFormulaire68 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Formulaire__Group__0_in_ruleFormulaire94 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_entryRuleQuestion121 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuestion128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__0_in_ruleQuestion154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidget_in_entryRuleWidget181 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleWidget188 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Widget__Alternatives_in_ruleWidget214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUpload_in_entryRuleUpload241 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleUpload248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Upload__Group__0_in_ruleUpload274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButton_in_entryRuleRadioButton301 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleRadioButton308 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__0_in_ruleRadioButton334 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckBox_in_entryRuleCheckBox361 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleCheckBox368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckBox__Group__0_in_ruleCheckBox394 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTextBox_in_entryRuleTextBox421 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleTextBox428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TextBox__Group__0_in_ruleTextBox454 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleList_in_entryRuleList481 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleList488 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__List__Group__0_in_ruleList514 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContenu_in_entryRuleContenu541 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleContenu548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Contenu__Group__0_in_ruleContenu574 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_entryRuleImage601 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleImage608 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__0_in_ruleImage634 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChoix_in_entryRuleChoix661 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleChoix668 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Choix__Alternatives_in_ruleChoix694 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleTextBox_in_rule__Widget__Alternatives730 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleUpload_in_rule__Widget__Alternatives747 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleCheckBox_in_rule__Widget__Alternatives764 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleList_in_rule__Widget__Alternatives781 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleRadioButton_in_rule__Widget__Alternatives798 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Choix__TextAssignment_0_in_rule__Choix__Alternatives830 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_rule__Choix__Alternatives848 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Formulaire__Group__0__Impl_in_rule__Formulaire__Group__0878 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Formulaire__Group__1_in_rule__Formulaire__Group__0881 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rule__Formulaire__Group__0__Impl909 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Formulaire__Group__1__Impl_in_rule__Formulaire__Group__1940 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_rule__Formulaire__Group__2_in_rule__Formulaire__Group__1943 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Formulaire__IdAssignment_1_in_rule__Formulaire__Group__1__Impl970 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Formulaire__Group__2__Impl_in_rule__Formulaire__Group__21000 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Formulaire__QuestionsAssignment_2_in_rule__Formulaire__Group__2__Impl1027 = new BitSet(new long[]{0x0000000000001002L});
    public static final BitSet FOLLOW_rule__Question__Group__0__Impl_in_rule__Question__Group__01064 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Question__Group__1_in_rule__Question__Group__01067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_12_in_rule__Question__Group__0__Impl1095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__1__Impl_in_rule__Question__Group__11126 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Question__Group__2_in_rule__Question__Group__11129 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__IdAssignment_1_in_rule__Question__Group__1__Impl1156 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__2__Impl_in_rule__Question__Group__21186 = new BitSet(new long[]{0x0000000000004000L});
    public static final BitSet FOLLOW_rule__Question__Group__3_in_rule__Question__Group__21189 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Question__Group__2__Impl1217 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__3__Impl_in_rule__Question__Group__31248 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Question__Group__4_in_rule__Question__Group__31251 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rule__Question__Group__3__Impl1279 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__4__Impl_in_rule__Question__Group__41310 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Question__Group__5_in_rule__Question__Group__41313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Question__Group__4__Impl1341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__5__Impl_in_rule__Question__Group__51372 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Question__Group__6_in_rule__Question__Group__51375 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__TextAssignment_5_in_rule__Question__Group__5__Impl1402 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__6__Impl_in_rule__Question__Group__61432 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_rule__Question__Group__7_in_rule__Question__Group__61435 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Question__Group__6__Impl1463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__7__Impl_in_rule__Question__Group__71494 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Question__Group__8_in_rule__Question__Group__71497 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_16_in_rule__Question__Group__7__Impl1525 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__8__Impl_in_rule__Question__Group__81556 = new BitSet(new long[]{0x00000000003E0000L});
    public static final BitSet FOLLOW_rule__Question__Group__9_in_rule__Question__Group__81559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Question__Group__8__Impl1587 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__9__Impl_in_rule__Question__Group__91618 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Question__Group__10_in_rule__Question__Group__91621 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__ChoixAssignment_9_in_rule__Question__Group__9__Impl1648 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__10__Impl_in_rule__Question__Group__101678 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Question__Group__11_in_rule__Question__Group__101681 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Question__Group__10__Impl1709 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Question__Group__11__Impl_in_rule__Question__Group__111740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Question__Group__11__Impl1768 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Upload__Group__0__Impl_in_rule__Upload__Group__01823 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Upload__Group__1_in_rule__Upload__Group__01826 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_17_in_rule__Upload__Group__0__Impl1854 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Upload__Group__1__Impl_in_rule__Upload__Group__11885 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Upload__Group__2_in_rule__Upload__Group__11888 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Upload__IdAssignment_1_in_rule__Upload__Group__1__Impl1915 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Upload__Group__2__Impl_in_rule__Upload__Group__21945 = new BitSet(new long[]{0x0000000000800000L});
    public static final BitSet FOLLOW_rule__Upload__Group__3_in_rule__Upload__Group__21948 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Upload__Group__2__Impl1976 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Upload__Group__3__Impl_in_rule__Upload__Group__32007 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Upload__Group__4_in_rule__Upload__Group__32010 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Upload__ImgAssignment_3_in_rule__Upload__Group__3__Impl2037 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Upload__Group__4__Impl_in_rule__Upload__Group__42067 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Upload__Group__4__Impl2095 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__0__Impl_in_rule__RadioButton__Group__02136 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__1_in_rule__RadioButton__Group__02139 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_18_in_rule__RadioButton__Group__0__Impl2167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__1__Impl_in_rule__RadioButton__Group__12198 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__2_in_rule__RadioButton__Group__12201 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__IdAssignment_1_in_rule__RadioButton__Group__1__Impl2228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__Group__2__Impl_in_rule__RadioButton__Group__22258 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__RadioButton__ReponseAssignment_2_in_rule__RadioButton__Group__2__Impl2287 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__RadioButton__ReponseAssignment_2_in_rule__RadioButton__Group__2__Impl2299 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__CheckBox__Group__0__Impl_in_rule__CheckBox__Group__02338 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__CheckBox__Group__1_in_rule__CheckBox__Group__02341 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_19_in_rule__CheckBox__Group__0__Impl2369 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckBox__Group__1__Impl_in_rule__CheckBox__Group__12400 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__CheckBox__Group__2_in_rule__CheckBox__Group__12403 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckBox__IdAssignment_1_in_rule__CheckBox__Group__1__Impl2430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckBox__Group__2__Impl_in_rule__CheckBox__Group__22460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__CheckBox__ReponseAssignment_2_in_rule__CheckBox__Group__2__Impl2489 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__CheckBox__ReponseAssignment_2_in_rule__CheckBox__Group__2__Impl2501 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__TextBox__Group__0__Impl_in_rule__TextBox__Group__02540 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__TextBox__Group__1_in_rule__TextBox__Group__02543 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_20_in_rule__TextBox__Group__0__Impl2571 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TextBox__Group__1__Impl_in_rule__TextBox__Group__12602 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__TextBox__Group__2_in_rule__TextBox__Group__12605 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TextBox__IdAssignment_1_in_rule__TextBox__Group__1__Impl2632 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TextBox__Group__2__Impl_in_rule__TextBox__Group__22662 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__TextBox__TextAssignment_2_in_rule__TextBox__Group__2__Impl2689 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__List__Group__0__Impl_in_rule__List__Group__02725 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__List__Group__1_in_rule__List__Group__02728 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_21_in_rule__List__Group__0__Impl2756 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__List__Group__1__Impl_in_rule__List__Group__12787 = new BitSet(new long[]{0x0000000000400000L});
    public static final BitSet FOLLOW_rule__List__Group__2_in_rule__List__Group__12790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__List__IdAssignment_1_in_rule__List__Group__1__Impl2817 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__List__Group__2__Impl_in_rule__List__Group__22847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__List__ElementsAssignment_2_in_rule__List__Group__2__Impl2876 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__List__ElementsAssignment_2_in_rule__List__Group__2__Impl2888 = new BitSet(new long[]{0x0000000000400002L});
    public static final BitSet FOLLOW_rule__Contenu__Group__0__Impl_in_rule__Contenu__Group__02927 = new BitSet(new long[]{0x0000000000000010L});
    public static final BitSet FOLLOW_rule__Contenu__Group__1_in_rule__Contenu__Group__02930 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_22_in_rule__Contenu__Group__0__Impl2958 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Contenu__Group__1__Impl_in_rule__Contenu__Group__12989 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_rule__Contenu__Group__2_in_rule__Contenu__Group__12992 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Contenu__IdAssignment_1_in_rule__Contenu__Group__1__Impl3019 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Contenu__Group__2__Impl_in_rule__Contenu__Group__23049 = new BitSet(new long[]{0x0000000000800020L});
    public static final BitSet FOLLOW_rule__Contenu__Group__3_in_rule__Contenu__Group__23052 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_13_in_rule__Contenu__Group__2__Impl3080 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Contenu__Group__3__Impl_in_rule__Contenu__Group__33111 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_rule__Contenu__Group__4_in_rule__Contenu__Group__33114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Contenu__ChoixAssignment_3_in_rule__Contenu__Group__3__Impl3141 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Contenu__Group__4__Impl_in_rule__Contenu__Group__43171 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_rule__Contenu__Group__4__Impl3199 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__0__Impl_in_rule__Image__Group__03240 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_rule__Image__Group__1_in_rule__Image__Group__03243 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_23_in_rule__Image__Group__0__Impl3271 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__Group__1__Impl_in_rule__Image__Group__13302 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rule__Image__ImgAssignment_1_in_rule__Image__Group__1__Impl3329 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Formulaire__IdAssignment_13368 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_rule__Formulaire__QuestionsAssignment_23399 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Question__IdAssignment_13430 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Question__TextAssignment_53461 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleWidget_in_rule__Question__ChoixAssignment_93492 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Upload__IdAssignment_13523 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleImage_in_rule__Upload__ImgAssignment_33554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__RadioButton__IdAssignment_13585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContenu_in_rule__RadioButton__ReponseAssignment_23616 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__CheckBox__IdAssignment_13647 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContenu_in_rule__CheckBox__ReponseAssignment_23678 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__TextBox__IdAssignment_13709 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__TextBox__TextAssignment_23740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__List__IdAssignment_13771 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleContenu_in_rule__List__ElementsAssignment_23802 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_rule__Contenu__IdAssignment_13833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleChoix_in_rule__Contenu__ChoixAssignment_33864 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Image__ImgAssignment_13895 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_STRING_in_rule__Choix__TextAssignment_03926 = new BitSet(new long[]{0x0000000000000002L});

}