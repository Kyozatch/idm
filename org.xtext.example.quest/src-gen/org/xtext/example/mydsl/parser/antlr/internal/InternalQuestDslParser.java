package org.xtext.example.mydsl.parser.antlr.internal; 

import org.eclipse.xtext.*;
import org.eclipse.xtext.parser.*;
import org.eclipse.xtext.parser.impl.*;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.parser.antlr.AbstractInternalAntlrParser;
import org.eclipse.xtext.parser.antlr.XtextTokenStream;
import org.eclipse.xtext.parser.antlr.XtextTokenStream.HiddenTokens;
import org.eclipse.xtext.parser.antlr.AntlrDatatypeRuleToken;
import org.xtext.example.mydsl.services.QuestDslGrammarAccess;



import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class InternalQuestDslParser extends AbstractInternalAntlrParser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "RULE_ID", "RULE_STRING", "RULE_INT", "RULE_ML_COMMENT", "RULE_SL_COMMENT", "RULE_WS", "RULE_ANY_OTHER", "'PollSystem'", "'{'", "'}'", "'Poll'", "'Question'", "'options'", "'=>'"
    };
    public static final int RULE_ID=4;
    public static final int RULE_STRING=5;
    public static final int T__16=16;
    public static final int T__15=15;
    public static final int T__17=17;
    public static final int T__12=12;
    public static final int T__11=11;
    public static final int T__14=14;
    public static final int T__13=13;
    public static final int RULE_ANY_OTHER=10;
    public static final int RULE_INT=6;
    public static final int RULE_WS=9;
    public static final int RULE_SL_COMMENT=8;
    public static final int EOF=-1;
    public static final int RULE_ML_COMMENT=7;

    // delegates
    // delegators


        public InternalQuestDslParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public InternalQuestDslParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        

    public String[] getTokenNames() { return InternalQuestDslParser.tokenNames; }
    public String getGrammarFileName() { return "../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g"; }



     	private QuestDslGrammarAccess grammarAccess;
     	
        public InternalQuestDslParser(TokenStream input, QuestDslGrammarAccess grammarAccess) {
            this(input);
            this.grammarAccess = grammarAccess;
            registerRules(grammarAccess.getGrammar());
        }
        
        @Override
        protected String getFirstRuleName() {
        	return "PollSystem";	
       	}
       	
       	@Override
       	protected QuestDslGrammarAccess getGrammarAccess() {
       		return grammarAccess;
       	}



    // $ANTLR start "entryRulePollSystem"
    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:67:1: entryRulePollSystem returns [EObject current=null] : iv_rulePollSystem= rulePollSystem EOF ;
    public final EObject entryRulePollSystem() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePollSystem = null;


        try {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:68:2: (iv_rulePollSystem= rulePollSystem EOF )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:69:2: iv_rulePollSystem= rulePollSystem EOF
            {
             newCompositeNode(grammarAccess.getPollSystemRule()); 
            pushFollow(FOLLOW_rulePollSystem_in_entryRulePollSystem75);
            iv_rulePollSystem=rulePollSystem();

            state._fsp--;

             current =iv_rulePollSystem; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePollSystem85); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePollSystem"


    // $ANTLR start "rulePollSystem"
    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:76:1: rulePollSystem returns [EObject current=null] : (otherlv_0= 'PollSystem' otherlv_1= '{' ( (lv_polls_2_0= rulePoll ) )* otherlv_3= '}' ) ;
    public final EObject rulePollSystem() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token otherlv_1=null;
        Token otherlv_3=null;
        EObject lv_polls_2_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:79:28: ( (otherlv_0= 'PollSystem' otherlv_1= '{' ( (lv_polls_2_0= rulePoll ) )* otherlv_3= '}' ) )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:80:1: (otherlv_0= 'PollSystem' otherlv_1= '{' ( (lv_polls_2_0= rulePoll ) )* otherlv_3= '}' )
            {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:80:1: (otherlv_0= 'PollSystem' otherlv_1= '{' ( (lv_polls_2_0= rulePoll ) )* otherlv_3= '}' )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:80:3: otherlv_0= 'PollSystem' otherlv_1= '{' ( (lv_polls_2_0= rulePoll ) )* otherlv_3= '}'
            {
            otherlv_0=(Token)match(input,11,FOLLOW_11_in_rulePollSystem122); 

                	newLeafNode(otherlv_0, grammarAccess.getPollSystemAccess().getPollSystemKeyword_0());
                
            otherlv_1=(Token)match(input,12,FOLLOW_12_in_rulePollSystem134); 

                	newLeafNode(otherlv_1, grammarAccess.getPollSystemAccess().getLeftCurlyBracketKeyword_1());
                
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:88:1: ( (lv_polls_2_0= rulePoll ) )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==14) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:89:1: (lv_polls_2_0= rulePoll )
            	    {
            	    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:89:1: (lv_polls_2_0= rulePoll )
            	    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:90:3: lv_polls_2_0= rulePoll
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getPollSystemAccess().getPollsPollParserRuleCall_2_0()); 
            	    	    
            	    pushFollow(FOLLOW_rulePoll_in_rulePollSystem155);
            	    lv_polls_2_0=rulePoll();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getPollSystemRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"polls",
            	            		lv_polls_2_0, 
            	            		"Poll");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            otherlv_3=(Token)match(input,13,FOLLOW_13_in_rulePollSystem168); 

                	newLeafNode(otherlv_3, grammarAccess.getPollSystemAccess().getRightCurlyBracketKeyword_3());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePollSystem"


    // $ANTLR start "entryRulePoll"
    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:118:1: entryRulePoll returns [EObject current=null] : iv_rulePoll= rulePoll EOF ;
    public final EObject entryRulePoll() throws RecognitionException {
        EObject current = null;

        EObject iv_rulePoll = null;


        try {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:119:2: (iv_rulePoll= rulePoll EOF )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:120:2: iv_rulePoll= rulePoll EOF
            {
             newCompositeNode(grammarAccess.getPollRule()); 
            pushFollow(FOLLOW_rulePoll_in_entryRulePoll204);
            iv_rulePoll=rulePoll();

            state._fsp--;

             current =iv_rulePoll; 
            match(input,EOF,FOLLOW_EOF_in_entryRulePoll214); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRulePoll"


    // $ANTLR start "rulePoll"
    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:127:1: rulePoll returns [EObject current=null] : (otherlv_0= 'Poll' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_question_3_0= ruleQuestion ) ) otherlv_4= '}' ) ;
    public final EObject rulePoll() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token otherlv_4=null;
        EObject lv_question_3_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:130:28: ( (otherlv_0= 'Poll' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_question_3_0= ruleQuestion ) ) otherlv_4= '}' ) )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:131:1: (otherlv_0= 'Poll' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_question_3_0= ruleQuestion ) ) otherlv_4= '}' )
            {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:131:1: (otherlv_0= 'Poll' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_question_3_0= ruleQuestion ) ) otherlv_4= '}' )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:131:3: otherlv_0= 'Poll' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_question_3_0= ruleQuestion ) ) otherlv_4= '}'
            {
            otherlv_0=(Token)match(input,14,FOLLOW_14_in_rulePoll251); 

                	newLeafNode(otherlv_0, grammarAccess.getPollAccess().getPollKeyword_0());
                
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:135:1: ( (lv_id_1_0= RULE_ID ) )?
            int alt2=2;
            int LA2_0 = input.LA(1);

            if ( (LA2_0==RULE_ID) ) {
                alt2=1;
            }
            switch (alt2) {
                case 1 :
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:136:1: (lv_id_1_0= RULE_ID )
                    {
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:136:1: (lv_id_1_0= RULE_ID )
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:137:3: lv_id_1_0= RULE_ID
                    {
                    lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_rulePoll268); 

                    			newLeafNode(lv_id_1_0, grammarAccess.getPollAccess().getIdIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getPollRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"id",
                            		lv_id_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_rulePoll286); 

                	newLeafNode(otherlv_2, grammarAccess.getPollAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:157:1: ( (lv_question_3_0= ruleQuestion ) )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:158:1: (lv_question_3_0= ruleQuestion )
            {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:158:1: (lv_question_3_0= ruleQuestion )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:159:3: lv_question_3_0= ruleQuestion
            {
             
            	        newCompositeNode(grammarAccess.getPollAccess().getQuestionQuestionParserRuleCall_3_0()); 
            	    
            pushFollow(FOLLOW_ruleQuestion_in_rulePoll307);
            lv_question_3_0=ruleQuestion();

            state._fsp--;


            	        if (current==null) {
            	            current = createModelElementForParent(grammarAccess.getPollRule());
            	        }
                   		add(
                   			current, 
                   			"question",
                    		lv_question_3_0, 
                    		"Question");
            	        afterParserOrEnumRuleCall();
            	    

            }


            }

            otherlv_4=(Token)match(input,13,FOLLOW_13_in_rulePoll319); 

                	newLeafNode(otherlv_4, grammarAccess.getPollAccess().getRightCurlyBracketKeyword_4());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "rulePoll"


    // $ANTLR start "entryRuleQuestion"
    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:187:1: entryRuleQuestion returns [EObject current=null] : iv_ruleQuestion= ruleQuestion EOF ;
    public final EObject entryRuleQuestion() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleQuestion = null;


        try {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:188:2: (iv_ruleQuestion= ruleQuestion EOF )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:189:2: iv_ruleQuestion= ruleQuestion EOF
            {
             newCompositeNode(grammarAccess.getQuestionRule()); 
            pushFollow(FOLLOW_ruleQuestion_in_entryRuleQuestion355);
            iv_ruleQuestion=ruleQuestion();

            state._fsp--;

             current =iv_ruleQuestion; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleQuestion365); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleQuestion"


    // $ANTLR start "ruleQuestion"
    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:196:1: ruleQuestion returns [EObject current=null] : (otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) otherlv_4= 'options' otherlv_5= '{' ( (lv_options_6_0= ruleOption ) )+ otherlv_7= '}' otherlv_8= '}' ) ;
    public final EObject ruleQuestion() throws RecognitionException {
        EObject current = null;

        Token otherlv_0=null;
        Token lv_id_1_0=null;
        Token otherlv_2=null;
        Token lv_text_3_0=null;
        Token otherlv_4=null;
        Token otherlv_5=null;
        Token otherlv_7=null;
        Token otherlv_8=null;
        EObject lv_options_6_0 = null;


         enterRule(); 
            
        try {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:199:28: ( (otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) otherlv_4= 'options' otherlv_5= '{' ( (lv_options_6_0= ruleOption ) )+ otherlv_7= '}' otherlv_8= '}' ) )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:200:1: (otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) otherlv_4= 'options' otherlv_5= '{' ( (lv_options_6_0= ruleOption ) )+ otherlv_7= '}' otherlv_8= '}' )
            {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:200:1: (otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) otherlv_4= 'options' otherlv_5= '{' ( (lv_options_6_0= ruleOption ) )+ otherlv_7= '}' otherlv_8= '}' )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:200:3: otherlv_0= 'Question' ( (lv_id_1_0= RULE_ID ) )? otherlv_2= '{' ( (lv_text_3_0= RULE_STRING ) ) otherlv_4= 'options' otherlv_5= '{' ( (lv_options_6_0= ruleOption ) )+ otherlv_7= '}' otherlv_8= '}'
            {
            otherlv_0=(Token)match(input,15,FOLLOW_15_in_ruleQuestion402); 

                	newLeafNode(otherlv_0, grammarAccess.getQuestionAccess().getQuestionKeyword_0());
                
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:204:1: ( (lv_id_1_0= RULE_ID ) )?
            int alt3=2;
            int LA3_0 = input.LA(1);

            if ( (LA3_0==RULE_ID) ) {
                alt3=1;
            }
            switch (alt3) {
                case 1 :
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:205:1: (lv_id_1_0= RULE_ID )
                    {
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:205:1: (lv_id_1_0= RULE_ID )
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:206:3: lv_id_1_0= RULE_ID
                    {
                    lv_id_1_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleQuestion419); 

                    			newLeafNode(lv_id_1_0, grammarAccess.getQuestionAccess().getIdIDTerminalRuleCall_1_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getQuestionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"id",
                            		lv_id_1_0, 
                            		"ID");
                    	    

                    }


                    }
                    break;

            }

            otherlv_2=(Token)match(input,12,FOLLOW_12_in_ruleQuestion437); 

                	newLeafNode(otherlv_2, grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_2());
                
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:226:1: ( (lv_text_3_0= RULE_STRING ) )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:227:1: (lv_text_3_0= RULE_STRING )
            {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:227:1: (lv_text_3_0= RULE_STRING )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:228:3: lv_text_3_0= RULE_STRING
            {
            lv_text_3_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleQuestion454); 

            			newLeafNode(lv_text_3_0, grammarAccess.getQuestionAccess().getTextSTRINGTerminalRuleCall_3_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getQuestionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text",
                    		lv_text_3_0, 
                    		"STRING");
            	    

            }


            }

            otherlv_4=(Token)match(input,16,FOLLOW_16_in_ruleQuestion471); 

                	newLeafNode(otherlv_4, grammarAccess.getQuestionAccess().getOptionsKeyword_4());
                
            otherlv_5=(Token)match(input,12,FOLLOW_12_in_ruleQuestion483); 

                	newLeafNode(otherlv_5, grammarAccess.getQuestionAccess().getLeftCurlyBracketKeyword_5());
                
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:252:1: ( (lv_options_6_0= ruleOption ) )+
            int cnt4=0;
            loop4:
            do {
                int alt4=2;
                int LA4_0 = input.LA(1);

                if ( ((LA4_0>=RULE_ID && LA4_0<=RULE_STRING)) ) {
                    alt4=1;
                }


                switch (alt4) {
            	case 1 :
            	    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:253:1: (lv_options_6_0= ruleOption )
            	    {
            	    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:253:1: (lv_options_6_0= ruleOption )
            	    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:254:3: lv_options_6_0= ruleOption
            	    {
            	     
            	    	        newCompositeNode(grammarAccess.getQuestionAccess().getOptionsOptionParserRuleCall_6_0()); 
            	    	    
            	    pushFollow(FOLLOW_ruleOption_in_ruleQuestion504);
            	    lv_options_6_0=ruleOption();

            	    state._fsp--;


            	    	        if (current==null) {
            	    	            current = createModelElementForParent(grammarAccess.getQuestionRule());
            	    	        }
            	           		add(
            	           			current, 
            	           			"options",
            	            		lv_options_6_0, 
            	            		"Option");
            	    	        afterParserOrEnumRuleCall();
            	    	    

            	    }


            	    }
            	    break;

            	default :
            	    if ( cnt4 >= 1 ) break loop4;
                        EarlyExitException eee =
                            new EarlyExitException(4, input);
                        throw eee;
                }
                cnt4++;
            } while (true);

            otherlv_7=(Token)match(input,13,FOLLOW_13_in_ruleQuestion517); 

                	newLeafNode(otherlv_7, grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_7());
                
            otherlv_8=(Token)match(input,13,FOLLOW_13_in_ruleQuestion529); 

                	newLeafNode(otherlv_8, grammarAccess.getQuestionAccess().getRightCurlyBracketKeyword_8());
                

            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleQuestion"


    // $ANTLR start "entryRuleOption"
    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:286:1: entryRuleOption returns [EObject current=null] : iv_ruleOption= ruleOption EOF ;
    public final EObject entryRuleOption() throws RecognitionException {
        EObject current = null;

        EObject iv_ruleOption = null;


        try {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:287:2: (iv_ruleOption= ruleOption EOF )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:288:2: iv_ruleOption= ruleOption EOF
            {
             newCompositeNode(grammarAccess.getOptionRule()); 
            pushFollow(FOLLOW_ruleOption_in_entryRuleOption565);
            iv_ruleOption=ruleOption();

            state._fsp--;

             current =iv_ruleOption; 
            match(input,EOF,FOLLOW_EOF_in_entryRuleOption575); 

            }

        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "entryRuleOption"


    // $ANTLR start "ruleOption"
    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:295:1: ruleOption returns [EObject current=null] : ( ( ( (lv_id_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) ) ;
    public final EObject ruleOption() throws RecognitionException {
        EObject current = null;

        Token lv_id_0_0=null;
        Token otherlv_1=null;
        Token lv_text_2_0=null;

         enterRule(); 
            
        try {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:298:28: ( ( ( ( (lv_id_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) ) )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:299:1: ( ( ( (lv_id_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) )
            {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:299:1: ( ( ( (lv_id_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) ) )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:299:2: ( ( (lv_id_0_0= RULE_ID ) ) otherlv_1= '=>' )? ( (lv_text_2_0= RULE_STRING ) )
            {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:299:2: ( ( (lv_id_0_0= RULE_ID ) ) otherlv_1= '=>' )?
            int alt5=2;
            int LA5_0 = input.LA(1);

            if ( (LA5_0==RULE_ID) ) {
                alt5=1;
            }
            switch (alt5) {
                case 1 :
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:299:3: ( (lv_id_0_0= RULE_ID ) ) otherlv_1= '=>'
                    {
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:299:3: ( (lv_id_0_0= RULE_ID ) )
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:300:1: (lv_id_0_0= RULE_ID )
                    {
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:300:1: (lv_id_0_0= RULE_ID )
                    // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:301:3: lv_id_0_0= RULE_ID
                    {
                    lv_id_0_0=(Token)match(input,RULE_ID,FOLLOW_RULE_ID_in_ruleOption618); 

                    			newLeafNode(lv_id_0_0, grammarAccess.getOptionAccess().getIdIDTerminalRuleCall_0_0_0()); 
                    		

                    	        if (current==null) {
                    	            current = createModelElement(grammarAccess.getOptionRule());
                    	        }
                           		setWithLastConsumed(
                           			current, 
                           			"id",
                            		lv_id_0_0, 
                            		"ID");
                    	    

                    }


                    }

                    otherlv_1=(Token)match(input,17,FOLLOW_17_in_ruleOption635); 

                        	newLeafNode(otherlv_1, grammarAccess.getOptionAccess().getEqualsSignGreaterThanSignKeyword_0_1());
                        

                    }
                    break;

            }

            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:321:3: ( (lv_text_2_0= RULE_STRING ) )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:322:1: (lv_text_2_0= RULE_STRING )
            {
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:322:1: (lv_text_2_0= RULE_STRING )
            // ../org.xtext.example.quest/src-gen/org/xtext/example/mydsl/parser/antlr/internal/InternalQuestDsl.g:323:3: lv_text_2_0= RULE_STRING
            {
            lv_text_2_0=(Token)match(input,RULE_STRING,FOLLOW_RULE_STRING_in_ruleOption654); 

            			newLeafNode(lv_text_2_0, grammarAccess.getOptionAccess().getTextSTRINGTerminalRuleCall_1_0()); 
            		

            	        if (current==null) {
            	            current = createModelElement(grammarAccess.getOptionRule());
            	        }
                   		setWithLastConsumed(
                   			current, 
                   			"text",
                    		lv_text_2_0, 
                    		"STRING");
            	    

            }


            }


            }


            }

             leaveRule(); 
        }
         
            catch (RecognitionException re) { 
                recover(input,re); 
                appendSkippedTokens();
            } 
        finally {
        }
        return current;
    }
    // $ANTLR end "ruleOption"

    // Delegated rules


 

    public static final BitSet FOLLOW_rulePollSystem_in_entryRulePollSystem75 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePollSystem85 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_11_in_rulePollSystem122 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_rulePollSystem134 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_rulePoll_in_rulePollSystem155 = new BitSet(new long[]{0x0000000000006000L});
    public static final BitSet FOLLOW_13_in_rulePollSystem168 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_rulePoll_in_entryRulePoll204 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRulePoll214 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_14_in_rulePoll251 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_RULE_ID_in_rulePoll268 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_rulePoll286 = new BitSet(new long[]{0x0000000000008000L});
    public static final BitSet FOLLOW_ruleQuestion_in_rulePoll307 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_rulePoll319 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleQuestion_in_entryRuleQuestion355 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleQuestion365 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_15_in_ruleQuestion402 = new BitSet(new long[]{0x0000000000001010L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleQuestion419 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleQuestion437 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleQuestion454 = new BitSet(new long[]{0x0000000000010000L});
    public static final BitSet FOLLOW_16_in_ruleQuestion471 = new BitSet(new long[]{0x0000000000001000L});
    public static final BitSet FOLLOW_12_in_ruleQuestion483 = new BitSet(new long[]{0x0000000000000030L});
    public static final BitSet FOLLOW_ruleOption_in_ruleQuestion504 = new BitSet(new long[]{0x0000000000002030L});
    public static final BitSet FOLLOW_13_in_ruleQuestion517 = new BitSet(new long[]{0x0000000000002000L});
    public static final BitSet FOLLOW_13_in_ruleQuestion529 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleOption_in_entryRuleOption565 = new BitSet(new long[]{0x0000000000000000L});
    public static final BitSet FOLLOW_EOF_in_entryRuleOption575 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_RULE_ID_in_ruleOption618 = new BitSet(new long[]{0x0000000000020000L});
    public static final BitSet FOLLOW_17_in_ruleOption635 = new BitSet(new long[]{0x0000000000000020L});
    public static final BitSet FOLLOW_RULE_STRING_in_ruleOption654 = new BitSet(new long[]{0x0000000000000002L});

}