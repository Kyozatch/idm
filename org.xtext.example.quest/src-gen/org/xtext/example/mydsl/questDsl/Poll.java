/**
 */
package org.xtext.example.mydsl.questDsl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Poll</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link org.xtext.example.mydsl.questDsl.Poll#getId <em>Id</em>}</li>
 *   <li>{@link org.xtext.example.mydsl.questDsl.Poll#getQuestion <em>Question</em>}</li>
 * </ul>
 * </p>
 *
 * @see org.xtext.example.mydsl.questDsl.QuestDslPackage#getPoll()
 * @model
 * @generated
 */
public interface Poll extends EObject
{
  /**
   * Returns the value of the '<em><b>Id</b></em>' attribute.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Id</em>' attribute isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Id</em>' attribute.
   * @see #setId(String)
   * @see org.xtext.example.mydsl.questDsl.QuestDslPackage#getPoll_Id()
   * @model
   * @generated
   */
  String getId();

  /**
   * Sets the value of the '{@link org.xtext.example.mydsl.questDsl.Poll#getId <em>Id</em>}' attribute.
   * <!-- begin-user-doc -->
   * <!-- end-user-doc -->
   * @param value the new value of the '<em>Id</em>' attribute.
   * @see #getId()
   * @generated
   */
  void setId(String value);

  /**
   * Returns the value of the '<em><b>Question</b></em>' containment reference list.
   * The list contents are of type {@link org.xtext.example.mydsl.questDsl.Question}.
   * <!-- begin-user-doc -->
   * <p>
   * If the meaning of the '<em>Question</em>' containment reference list isn't clear,
   * there really should be more of a description here...
   * </p>
   * <!-- end-user-doc -->
   * @return the value of the '<em>Question</em>' containment reference list.
   * @see org.xtext.example.mydsl.questDsl.QuestDslPackage#getPoll_Question()
   * @model containment="true"
   * @generated
   */
  EList<Question> getQuestion();

} // Poll
